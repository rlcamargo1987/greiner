﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GreinerAssistec.Dominio.Modelo.Administradores;
using GreinerAssistec.Dominio.Modelo.Entidades;
using System.Threading.Tasks;
using System.Linq;

namespace GreinerAssistec.Applicacion.Negocio.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public async Task CreateUserTest()
        {
            try
            {
                var manager = AppUsuarioManager.Create();
                Usuario u = new Usuario();
                u.IdUsuario = Guid.NewGuid();
                u.Correo = "rodro_185lc@hotmail.com";
                u.Contrasena = "Contraseña";
                u.Genero = false;//NHombre
                var r = await manager.CreateAsync(u);
                Assert.AreEqual(manager.Usuarios.Where(x => x.Correo == "rodro_185lc@hotmail.com").Any(), true);
            }
            catch (Exception ex)
            {

            }

        }
    }
}
