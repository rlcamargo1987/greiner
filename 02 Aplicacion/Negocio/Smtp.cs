﻿using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Aplicacion.Negocio
{

    public class TestClass
    {
        public void Method01Test()
        {
            ISmtpSender smtpSender = new SmtpSender();

            var notification = new Notification
            {
                To = "hector.flores@webpoint.mx",
                Subject = "Test",
                Html = false,
                Body = "Este es un correo de prueba",
                From = "noreply@mail.com"
            };

            var smtpConfiguration = new SmtpConfiguration
            {
                Host = "smtp.gmail.com",
                Password = "Q1w2e3r$",
                //Password = "kmarguin1234",
                Port = 587,
                Username = "noresponderwp@gmail.com"
                //Username = "rodro182lc@gmail.com"
            };

            smtpSender.Send(notification, smtpConfiguration);
        }
    }


}
