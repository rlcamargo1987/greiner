﻿
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;

namespace GreinerAssistec.Aplicacion.Negocio
{
    public class EmpaqueManagement
    {
        public static EmpaqueManagement Instance()
        {
            return new EmpaqueManagement();
        }

        public EmpaqueManagement()
        {
        }

        public Response ObtenerTodosEmpaques()
        {
            try
            {
                var lista = EmpaqueRepository.Instance().ObtenerTodosEmpaques();
                return new Response() { Result = ResultType.OK, Records = lista };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Empaques/ObtenerTodosEmpaques: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }

        public async Task<Response> CreaEmpaque(Empaque Empaque)
        {
            return await EmpaqueRepository.Instance().GuardaEmpaque(Empaque);
        }

        public async Task<Response> EliminarEmpaque(Guid IdEmpaque)
        {
            try
            {
                var ins = EmpaqueRepository.Instance().ObtenerEmpaquePorId(IdEmpaque);
                var r = await EmpaqueRepository.Instance().Eliminar(IdEmpaque);
                return new Response() { Result = ResultType.OK, Record = r };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Empaques/ObtenerTodosEmpaques: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }
    }
}
