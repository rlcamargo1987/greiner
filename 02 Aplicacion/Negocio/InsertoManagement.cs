﻿
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;

namespace GreinerAssistec.Aplicacion.Negocio
{
    public class InsertoManagement
    {
        public static InsertoManagement Instance()
        {
            return new InsertoManagement();
        }

        public InsertoManagement()
        {
        }

        public Response ObtenerTodosInsertos()
        {
            try
            {
                var lista = InsertoRepository.Instance().ObtenerTodosInsertos();
                return new Response() { Result = ResultType.OK, Records = lista };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Insertos/ObtenerTodosInsertos: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }

        public async Task<Response> CreaInserto(Inserto inserto, HttpPostedFile archivo)
        {
            #region Profile Image
            string name = "", relPath = "";
            if (archivo != null && inserto != null)
            {
                name = Guid.NewGuid().ToString() + Path.GetExtension(archivo.FileName);
                // Validate the uploaded image(optional)
                var pathBase = "/Uploads/Inserto/Archivos/";
                relPath = pathBase + name;
                // Get the complete file path
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + pathBase), name);

                // Save the uploaded file to "UploadedFiles" folder
                if (!string.IsNullOrEmpty(inserto.Instruccion) && File.Exists(HttpContext.Current.Server.MapPath("~") + inserto.Instruccion))
                {
                    File.Delete(HttpContext.Current.Server.MapPath("~") + inserto.Instruccion);
                }

                if (!Directory.Exists(HttpContext.Current.Server.MapPath("~" + pathBase)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~" + pathBase));
                }

                archivo.SaveAs(fileSavePath);
                inserto.Instruccion = relPath;
            }
            #endregion
            return await InsertoRepository.Instance().GuardaInserto(inserto);
        }

        public async Task<Response> EliminarInserto(Guid IdInserto)
        {
            try
            {

                var ins = InsertoRepository.Instance().ObtenerInsertoPorId(IdInserto);

                // Save the uploaded file to "UploadedFiles" folder
                if (File.Exists(HttpContext.Current.Server.MapPath("~") + ins.Instruccion))
                {
                    File.Delete(HttpContext.Current.Server.MapPath("~") + ins.Instruccion);
                }

                var r = await InsertoRepository.Instance().Eliminar(IdInserto);
                return new Response() { Result = ResultType.OK, Record = r };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Insertos/ObtenerTodosInsertos: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }
    }
}
