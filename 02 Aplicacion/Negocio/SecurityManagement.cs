﻿using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.Threading.Tasks;

namespace GreinerAssistec.Aplicacion.Negocio
{
    public class SecurityManagement
    {
        public static SecurityManagement Instance()
        {
            return new SecurityManagement();
        }

        public SecurityManagement()
        {
        }

        public Usuario VerificaCredenciales(string correo, string contrasena)
        {
            return UserRepository.Instance().VerificaCredenciales(correo, contrasena);
        }

        public async Task<bool> CreaUsuario(UserDto usuario)
        {
            var entity = UserService.Instance().ToEntity(usuario);
            return await UserRepository.Instance().GuardaUsuario(entity);
        }

        public bool RecuperaContrasena(string correo)
        {
            var cont = UserRepository.Instance().ObtenContrasena(correo);

            var notification = new Notification
            {
                To = correo,
                Subject = "Correo de recuperación de correo. Greiner Assistec.",
                Html = false,
                Body = string.Format("Su contraseña es : {0}", cont),
                From = "noreply@mail.com"
            };
            try
            {
                NotificacionService.Instance().EnviarCorreo(notification);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> CambiaContrasena(ChangePasswordModel model)
        {
            var usuario = UserRepository.Instance().ObtenUsuario(model.IdUsuario);
            usuario.Contrasena = model.password;
            await UserRepository.Instance().GuardaUsuario(usuario);
            return true;
        }
    }
}
