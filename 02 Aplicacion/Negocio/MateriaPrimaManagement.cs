﻿
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.Threading.Tasks;

namespace GreinerAssistec.Aplicacion.Negocio
{
    public class MateriaPrimaManagement
    {
        public static MateriaPrimaManagement Instance()
        {
            return new MateriaPrimaManagement();
        }

        public MateriaPrimaManagement()
        {
        }

        public MateriaPrima ObtenMateriaPrima(string idMateriaPrima)
        {
            return new MateriaPrima();// MateriaPrimaRepository.Instance().ObtenMateriaPrima(idMateriaPrima);
        }

        public object ObtenMateriaPrima(Guid idMateriaPrima)
        {
            try
            {
                return new MateriaPrima();// MateriaPrimaRepository.Instance().ObtenMateriaPrima(idMateriaPrima);
            }
            catch
            {
                return new Response() { Result = ResultType.Error, Message = "MateriaPrimas/Listado: Ocurrio un error interno. Contactar con Soporte." };
            }
        }

        public Response ObtenerTodosMateriaPrimas()
        {
            try
            {
                var lista = MateriaPrimaRepository.Instance().ObtenerTodosMateriaPrimas();
                return new Response() { Result = ResultType.OK, Records = lista };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "MateriaPrimas/ObtenerTodosMateriaPrimas: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }


        public async Task<Response> CreaMateriaPrima(MateriaPrima molde)
        {
            return await MateriaPrimaRepository.Instance().GuardaMateriaPrima(molde);
        }

        public Response ObtenerMateriaPrimasPorId(Guid IdMateriaPrima)
        {
            try
            {
                //  var user = MateriaPrimaRepository.Instance().ObtenMateriaPrima(IdMateriaPrima);
                return new Response() { Result = ResultType.OK };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "MateriaPrimas/ObtenerTodosMateriaPrimas: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }

        public async Task<Response> EliminarMateriaPrima(Guid IdMateriaPrima)
        {
            try
            {
                var r = await MateriaPrimaRepository.Instance().Eliminar(IdMateriaPrima);
                return new Response() { Result = ResultType.OK, Record = r };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "MateriaPrimas/ObtenerTodosMateriaPrimas: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }
    }
}
