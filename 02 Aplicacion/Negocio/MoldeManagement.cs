﻿
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.Threading.Tasks;

namespace GreinerAssistec.Aplicacion.Negocio
{
    public class MoldeManagement
    {
        public static MoldeManagement Instance()
        {
            return new MoldeManagement();
        }

        public MoldeManagement()
        {
        }

        public Molde ObtenMolde(string idMolde)
        {
            return new Molde();// MoldeRepository.Instance().ObtenMolde(idMolde);
        }

        public object ObtenMolde(Guid idMolde)
        {
            try
            {
                return new Molde();// MoldeRepository.Instance().ObtenMolde(idMolde);
            }
            catch
            {
                return new Response() { Result = ResultType.Error, Message = "Moldes/Listado: Ocurrio un error interno. Contactar con Soporte." };
            }
        }

        public Response ObtenerTodosMoldes()
        {
            try
            {
                var lista = MoldeRepository.Instance().ObtenerTodosMoldes();
                return new Response() { Result = ResultType.OK, Records = lista };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Moldes/ObtenerTodosMoldes: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }


        public async Task<Response> CreaMolde(Molde molde)
        {
            return await MoldeRepository.Instance().GuardaMolde(molde);
        }

        public Response ObtenerMoldesPorId(Guid IdMolde)
        {
            try
            {
                //  var user = MoldeRepository.Instance().ObtenMolde(IdMolde);
                return new Response() { Result = ResultType.OK };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Moldes/ObtenerTodosMoldes: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }

        public async Task<Response> CambiaEstatusMolde(Guid IdMolde, bool Activo)
        {
            try
            {
                var r = await MoldeRepository.Instance().CambiaEstatus(IdMolde, Activo);
                return new Response() { Result = ResultType.OK, Record = r };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Moldes/ObtenerTodosMoldes: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }
    }
}
