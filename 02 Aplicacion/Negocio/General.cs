﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net.Http;
using System.Net.Mail;
using System.IO;

namespace GreinerAssistec.Aplicacion.Negocio
{
    public class General
    {
        #region Singleton
        public static General Instance()
        {
            return new General();
        }
        #endregion

        #region Constructors
        public General()
        {
        }
        #endregion

        public static string GetDetailedError(Exception ex)
        {
            return string.Format("\n Message: {0}, \nData: {1}, \nInnerException: {2}, \nStackTrace: {3}, \nSource: {4}",
                    ex.Message,
                    ex.Data != null ? ex.Data.ToString() : "",
                    ex.InnerException != null ? ex.InnerException.ToString() : "",
                    ex.StackTrace, ex.Source);
        }
    }
}
