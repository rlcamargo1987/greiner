﻿using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.Threading.Tasks;

namespace GreinerAssistec.Aplicacion.Negocio
{
    public class UserManagement
    {
        public static UserManagement Instance()
        {
            return new UserManagement();
        }

        public UserManagement()
        {
        }

        public Usuario VerificaCredenciales(string correo, string contrasena)
        {
            try
            {
                return UserRepository.Instance().VerificaCredenciales(correo, contrasena);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string ObtenNombreCompleto(Usuario usuario)
        {
            return usuario.Nombre + " " + usuario.ApellidoPaterno + " " + usuario.ApellidoMaterno;
        }

        public Usuario ObtenUsuario(string idUsuario)
        {
            return UserRepository.Instance().ObtenUsuario(idUsuario);
        }

        public object ObtenUsuario(Guid idUsuario)
        {
            try
            {
                return UserRepository.Instance().ObtenUsuario(idUsuario);
            }
            catch
            {
                return new Response() { Result = ResultType.Error, Message = "Usuarios/Listado: Ocurrio un error interno. Contactar con Soporte." };
            }
        }

        public Response Info(Guid IdUsuario)
        {
            try
            {
                var original = UserRepository.Instance().ObtenInfoUsuario(IdUsuario);
                return new Response() { Result = ResultType.OK, Record = original };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Usuarios/Info: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }

        public Response ObtenerTodosUsuarios()
        {
            try
            {
                var lista = UserRepository.Instance().ObtenerTodosUsuarios();
                return new Response() { Result = ResultType.OK, Records = lista };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Usuarios/ObtenerTodosUsuarios: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }

        public Response ObtenerEstatusUsuarios()
        {
            try
            {
                var lista = UserRepository.Instance().ObtenerEstatusUsuarios();
                return new Response() { Result = ResultType.OK, Records = lista };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Usuarios/ObtenerTodosUsuarios: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }

        public async Task<bool> CreaUsuario(CompleteUserDto usuario)
        {
            var entity = UserService.Instance().ToEntity(usuario);
            return await UserRepository.Instance().GuardaUsuario(entity);
        }

        public Response ObtenerUsuariosPorId(Guid IdUsuario)
        {
            try
            {
                var user = UserRepository.Instance().ObtenUsuario(IdUsuario);
                user.Contrasena = UserRepository.Instance().ObtenContrasena(user.Correo);
                return new Response() { Result = ResultType.OK, Record = user };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Usuarios/ObtenerTodosUsuarios: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }

        public async Task<Response> CambiaEstatusUsuarios(Guid IdUsuario, Guid Estatus)
        {
            try
            {
                var r = await UserRepository.Instance().CambiaEstatusUsuarios(IdUsuario, Estatus);
                return new Response() { Result = ResultType.OK, Record = r };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Usuarios/ObtenerTodosUsuarios: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }
    }
}
