﻿
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.Threading.Tasks;

namespace GreinerAssistec.Aplicacion.Negocio
{
    public class ComponenteManagement
    {
        public static ComponenteManagement Instance()
        {
            return new ComponenteManagement();
        }

        public ComponenteManagement()
        {
        }

        public Componente ObtenComponente(string idComponente)
        {
            return new Componente();// ComponenteRepository.Instance().ObtenComponente(idComponente);
        }

        public object ObtenComponente(Guid idComponente)
        {
            try
            {
                return new Componente();// ComponenteRepository.Instance().ObtenComponente(idComponente);
            }
            catch
            {
                return new Response() { Result = ResultType.Error, Message = "Componentes/Listado: Ocurrio un error interno. Contactar con Soporte." };
            }
        }

        public Response ObtenerTodosComponentes()
        {
            try
            {
                var lista = ComponenteRepository.Instance().ObtenerTodosComponentes();
                return new Response() { Result = ResultType.OK, Records = lista };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Componentes/ObtenerTodosComponentes: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }


        public async Task<Response> CreaComponente(Componente molde)
        {
            return await ComponenteRepository.Instance().GuardaComponente(molde);
        }

        public Response ObtenerComponentesPorId(Guid IdComponente)
        {
            try
            {
                //  var user = ComponenteRepository.Instance().ObtenComponente(IdComponente);
                return new Response() { Result = ResultType.OK };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Componentes/ObtenerTodosComponentes: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }

        public async Task<Response> EliminarComponente(Guid IdComponente)
        {
            try
            {
                var r = await ComponenteRepository.Instance().Eliminar(IdComponente);
                return new Response() { Result = ResultType.OK, Record = r };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Componentes/ObtenerTodosComponentes: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }
    }
}
