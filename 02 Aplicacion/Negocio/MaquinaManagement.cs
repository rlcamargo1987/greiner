﻿
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;

namespace GreinerAssistec.Aplicacion.Negocio
{
    public class MaquinaManagement
    {
        public static MaquinaManagement Instance()
        {
            return new MaquinaManagement();
        }

        public MaquinaManagement()
        {
        }

        public Response ObtenerTodosMaquinas()
        {
            try
            {
                var lista = MaquinaRepository.Instance().ObtenerTodosMaquinas();
                return new Response() { Result = ResultType.OK, Records = lista };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Maquinas/ObtenerTodosMaquinas: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }

        public async Task<Response> CreaMaquina(Maquina Maquina)
        {
            return await MaquinaRepository.Instance().GuardaMaquina(Maquina);
        }

        public async Task<Response> EliminarMaquina(Guid IdMaquina)
        {
            try
            {
                var r = await MaquinaRepository.Instance().Eliminar(IdMaquina);
                return new Response() { Result = ResultType.OK, Record = r };
            }
            catch (Exception ex)
            {
                return new Response() { Result = ResultType.Error, Message = "Maquinas/ObtenerTodosMaquinas: Ocurrio un error interno. Contactar con Soporte. Error: " + General.GetDetailedError(ex) };
            }
        }
    }
}
