﻿using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using System;

namespace GreinerAssistec.Dominio.Servicio
{
    public class UserService
    {
        public static UserService Instance()
        {
            return new UserService();
        }

        public UserService()
        {
        }

        public Usuario ToEntity(UserDto user)
        {
            return new Usuario()
            {
                IdUsuario = user.IdUsuario != null && user.IdUsuario != Guid.Empty ? user.IdUsuario.Value : Guid.NewGuid(),
                Correo = user.Correo,
                ApellidoPaterno = user.ApellidoPaterno,
                Nombre = user.Nombre,
                Contrasena = user.Contrasena,
                ApellidoMaterno = user.ApellidoMaterno,
                Genero = user.Genero != null ? user.Genero.Value : false,  //Hombre
                TelefonoMovil = user.TelefonoMovil,
                FechaNacimiento = user.FechaNacimiento,
                Area = user.Area,
                Departamento = user.Departamento,
                Puesto = user.Puesto,
                PerfilUsuario = user.PerfilUsuario,
                FechaIngreso = user.FechaIngreso

            };
        }

        public Usuario ToEntity(CompleteUserDto user)
        {
            return new Usuario()
            {
                IdUsuario = user.IdUsuario != null && user.IdUsuario != Guid.Empty ? user.IdUsuario.Value : Guid.NewGuid(),
                Foto = user.Foto,
                Correo = user.Correo,
                Contrasena = user.Contrasena,
                ApellidoPaterno = user.ApellidoPaterno,
                Nombre = user.Nombre,
                ApellidoMaterno = user.ApellidoMaterno,
                Genero = user.Genero != null ? user.Genero.Value : false,  //Hombre
                TelefonoMovil = user.TelefonoMovil,
                FechaNacimiento = user.FechaNacimiento,
                Area = user.Area,
                Departamento = user.Departamento,
                Puesto = user.Puesto,
                PerfilUsuario = user.PerfilUsuario,
                FechaIngreso = user.FechaIngreso,
                Estatus = user.Estatus ?? user.Estatus.Value

            };
        }

    }
}
