﻿using GreinerAssistec.Dominio.Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace GreinerAssistec.Dominio.Servicio
{
    public interface ISmtpSender
    {
        /// <summary>
        /// Sends the specified notification.
        /// </summary>
        /// <param name="notification">The notification.</param>
        /// <param name="emails">The emails.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        bool Send(Notification notification, SmtpConfiguration smtpConfiguration, IEnumerable<string> emails);

        /// <summary>
        /// Sends the specified notification.
        /// </summary>
        /// <param name="notification">The notification.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        bool Send(Notification notification, SmtpConfiguration smtpConfiguration);
    }

    public class SmtpSender : ISmtpSender
    {
        private static SmtpConfiguration GetSmtpConfiguration()
        {

            var password = "Q1w2e3r$";
            var puerto = 587;
            var servidor = "smtp.gmail.com";
            var emisor = "noresponderwp@gmail.com";

            return new SmtpConfiguration
            {
                Host = servidor,
                Password = password,
                Port = puerto,
                Username = emisor,
            };
        }

        public bool Send(Notification notification, SmtpConfiguration config)
        {
            if (config == null)
            {
                config = GetSmtpConfiguration();
            }

            MailAddress to = new MailAddress(notification.To);
            MailAddress from = new MailAddress(notification.From);
            MailMessage message = new MailMessage(from, to);
            message.Subject = notification.Subject;
            message.IsBodyHtml = notification.Html;
            message.Body = notification.Body;

            try
            {
                using (var client = new SmtpClient())
                {
                    client.Port = config.Port;
                    client.Host = config.Host;
                    client.EnableSsl = true;
                    client.Timeout = 10000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new System.Net.NetworkCredential(config.Username, config.Password);
                    client.Send(message);
                    return true;
                }
            }
            catch (Exception ex)
            {
                // log.
                return false;
            }
        }

        public bool Send(Notification notification, SmtpConfiguration config, IEnumerable<string> emails)
        {
            if (config == null)
            {
                config = GetSmtpConfiguration();
            }

            //MailAddress to = new MailAddress(notification.To);
            System.Net.Mail.MailAddress from = new MailAddress(notification.From);

            MailMessage message = new MailMessage();
            message.Subject = notification.Subject;
            message.Body = notification.Body;
            message.IsBodyHtml = notification.Html;
            message.From = from;
            foreach (var email in emails)
            {
                // send.
                message.To.Add(email);
            }

            try
            {
                using (var client = new SmtpClient())
                {
                    client.Port = config.Port;
                    client.Host = config.Host;
                    client.EnableSsl = true;
                    client.Timeout = 10000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new System.Net.NetworkCredential(config.Username, config.Password);
                    client.Send(message);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
    }

    public class NotificacionService
    {

        public static NotificacionService Instance()
        {
            return new NotificacionService();
        }

        public NotificacionService()
        {
        }

        public void EnviarCorreo(Notification notification)
        {
            ISmtpSender smtpSender = new SmtpSender();

            //var notification = new Notification
            //{
            //    To = "hector.flores@webpoint.mx",
            //    Subject = "Test",
            //    Html = false,
            //    Body = "Este es un correo de prueba",
            //    From = "noreply@mail.com"
            //};

            var smtpConfiguration = new SmtpConfiguration
            {
                Host = "smtp.gmail.com",
                Password = "Q1w2e3r$",
                Port = 587,
                Username = "noresponderwp@gmail.com"
            };

            smtpSender.Send(notification, smtpConfiguration);



        }

    }
}
