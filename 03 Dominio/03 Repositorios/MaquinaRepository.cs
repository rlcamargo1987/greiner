﻿using GreinerAssistec.Dominio.Modelo.Administradores.Base;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Repositorio
{
    public class MaquinaRepository : BaseRepository
    {
        public static MaquinaRepository Instance()
        {
            return new MaquinaRepository();
        }

        public MaquinaRepository()
        {
        }

        public Response ObtenerTodosMaquinas()
        {
            try
            {
                var listMaquinas = MaquinaManager.Maquinas.ToList();

                return new Response
                {
                    Result = "OK",
                    Record = new
                    {
                        ListMaquinas = listMaquinas
                    }
                };
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", MaquinaRepository.GetDetailedError(ex)) };
            }
        }

        public async Task<Response> GuardaMaquina(Maquina molde)
        {
            try
            {
                if (!MaquinaManager.Maquinas.Where(x => x.IdMaquina == molde.IdMaquina).Any())
                {
                    molde.IdMaquina = molde.IdMaquina == Guid.Empty ? Guid.NewGuid() : molde.IdMaquina;
                    await MaquinaManager.CreateAsync(molde);
                }
                else
                {
                    await MaquinaManager.UpdateAsync(molde);
                }
                return new Response
                {
                    Result = "OK"
                };
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", MaquinaRepository.GetDetailedError(ex)) };
            }
        }

        public async Task<bool> Eliminar(Guid idMaquina)
        {
            var molde = MaquinaManager.Maquinas.Where(x => x.IdMaquina == idMaquina).FirstOrDefault();
            await MaquinaManager.DeleteAsync(molde);
            return true;
        }
    }
}
