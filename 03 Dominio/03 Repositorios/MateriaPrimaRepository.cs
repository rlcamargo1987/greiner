﻿using GreinerAssistec.Dominio.Modelo.Administradores.Base;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Repositorio
{
    public class MateriaPrimaRepository : BaseRepository
    {
        public static MateriaPrimaRepository Instance()
        {
            return new MateriaPrimaRepository();
        }

        public MateriaPrimaRepository()
        {
        }

        public Response ObtenerTodosMateriaPrimas()
        {
            try
            {
                var listMateriaPrimas = MateriaPrimaManager.MateriaPrimas.ToList();

                return new Response
                {
                    Result = "OK",
                    Record = new
                    {
                        ListMateriaPrimas = listMateriaPrimas
                    }
                };
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", MateriaPrimaRepository.GetDetailedError(ex)) };
            }
        }

        public async Task<Response> GuardaMateriaPrima(MateriaPrima molde)
        {
            try
            {
                if (!MateriaPrimaManager.MateriaPrimas.Where(x => x.IdMateriaPrima == molde.IdMateriaPrima).Any())
                {
                    molde.IdMateriaPrima = molde.IdMateriaPrima == Guid.Empty ? Guid.NewGuid() : molde.IdMateriaPrima;
                    await MateriaPrimaManager.CreateAsync(molde);
                }
                else
                {
                    await MateriaPrimaManager.UpdateAsync(molde);
                }
                return new Response
                {
                    Result = "OK"
                };
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", MateriaPrimaRepository.GetDetailedError(ex)) };
            }
        }

        public async Task<bool> Eliminar(Guid idMateriaPrima)
        {
            var molde = MateriaPrimaManager.MateriaPrimas.Where(x => x.IdMateriaPrima == idMateriaPrima).FirstOrDefault();
            await MateriaPrimaManager.DeleteAsync(molde);
            return true;
        }
    }
}
