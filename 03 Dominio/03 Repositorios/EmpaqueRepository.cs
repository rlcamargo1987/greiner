﻿using GreinerAssistec.Dominio.Modelo.Administradores.Base;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Repositorio
{
    public class EmpaqueRepository : BaseRepository
    {
        public static EmpaqueRepository Instance()
        {
            return new EmpaqueRepository();
        }

        public EmpaqueRepository()
        {
        }

        public Response ObtenerTodosEmpaques()
        {
            try
            {
                var listEmpaques = EmpaqueManager.Empaques.ToList();

                return new Response
                {
                    Result = "OK",
                    Record = new
                    {
                        ListEmpaques = listEmpaques
                    }
                };
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", EmpaqueRepository.GetDetailedError(ex)) };
            }
        }


        public Empaque ObtenerEmpaquePorId(Guid Empaque)
        {
            return EmpaqueManager.Empaques.Where(x => x.IdEmpaque == Empaque).FirstOrDefault();
        }


        public async Task<Response> GuardaEmpaque(Empaque molde)
        {
            try
            {
                if (!EmpaqueManager.Empaques.Where(x => x.IdEmpaque == molde.IdEmpaque).Any())
                {
                    molde.IdEmpaque = molde.IdEmpaque == Guid.Empty ? Guid.NewGuid() : molde.IdEmpaque;
                    await EmpaqueManager.CreateAsync(molde);
                }
                else
                {
                    await EmpaqueManager.UpdateAsync(molde);
                }
                return new Response
                {
                    Result = "OK"
                };
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", EmpaqueRepository.GetDetailedError(ex)) };
            }
        }

        public async Task<bool> Eliminar(Guid idEmpaque)
        {
            var molde = EmpaqueManager.Empaques.Where(x => x.IdEmpaque == idEmpaque).FirstOrDefault();
            await EmpaqueManager.DeleteAsync(molde);
            return true;
        }
    }
}
