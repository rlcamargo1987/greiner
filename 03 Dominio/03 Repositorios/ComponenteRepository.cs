﻿using GreinerAssistec.Dominio.Modelo.Administradores.Base;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Repositorio
{
    public class ComponenteRepository : BaseRepository
    {
        public static ComponenteRepository Instance()
        {
            return new ComponenteRepository();
        }

        public ComponenteRepository()
        {
        }

        public Response ObtenerTodosComponentes()
        {
            try
            {
                var listComponentes = ComponenteManager.Componentes.ToList();

                return new Response
                {
                    Result = "OK",
                    Record = new
                    {
                        ListComponentes = listComponentes
                    }
                };
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", ComponenteRepository.GetDetailedError(ex)) };
            }
        }

        public async Task<Response> GuardaComponente(Componente molde)
        {
            try
            {
                if (!ComponenteManager.Componentes.Where(x => x.IdComponente == molde.IdComponente).Any())
                {
                    molde.IdComponente = molde.IdComponente == Guid.Empty ? Guid.NewGuid() : molde.IdComponente;
                    await ComponenteManager.CreateAsync(molde);
                }
                else
                {
                    await ComponenteManager.UpdateAsync(molde);
                }
                return new Response
                {
                    Result = "OK"
                };
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", ComponenteRepository.GetDetailedError(ex)) };
            }
        }

        public async Task<bool> Eliminar(Guid idComponente)
        {
            var molde = ComponenteManager.Componentes.Where(x => x.IdComponente == idComponente).FirstOrDefault();
            await ComponenteManager.DeleteAsync(molde);
            return true;
        }
    }
}
