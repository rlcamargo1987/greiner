﻿using GreinerAssistec.Dominio.Modelo.Administradores.Base;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Repositorio
{
    public class InsertoRepository : BaseRepository
    {
        public static InsertoRepository Instance()
        {
            return new InsertoRepository();
        }

        public InsertoRepository()
        {
        }

        public Response ObtenerTodosInsertos()
        {
            try
            {
                var listInsertos = InsertoManager.Insertos.ToList();

                return new Response
                {
                    Result = "OK",
                    Record = new
                    {
                        ListInsertos = listInsertos
                    }
                };
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", InsertoRepository.GetDetailedError(ex)) };
            }
        }


        public Inserto ObtenerInsertoPorId(Guid inserto)
        {
            return InsertoManager.Insertos.Where(x => x.IdInserto == inserto).FirstOrDefault();
        }


        public async Task<Response> GuardaInserto(Inserto molde)
        {
            try
            {
                if (!InsertoManager.Insertos.Where(x => x.IdInserto == molde.IdInserto).Any())
                {
                    molde.IdInserto = molde.IdInserto == Guid.Empty ? Guid.NewGuid() : molde.IdInserto;
                    await InsertoManager.CreateAsync(molde);
                }
                else
                {
                    await InsertoManager.UpdateAsync(molde);
                }
                return new Response
                {
                    Result = "OK"
                };
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", InsertoRepository.GetDetailedError(ex)) };
            }
        }

        public async Task<bool> Eliminar(Guid idInserto)
        {
            var molde = InsertoManager.Insertos.Where(x => x.IdInserto == idInserto).FirstOrDefault();
            await InsertoManager.DeleteAsync(molde);
            return true;
        }
    }
}
