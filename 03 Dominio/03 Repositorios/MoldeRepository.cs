﻿using GreinerAssistec.Dominio.Modelo.Administradores.Base;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Repositorio
{
    public class MoldeRepository : BaseRepository
    {
        public static MoldeRepository Instance()
        {
            return new MoldeRepository();
        }

        public MoldeRepository()
        {
        }

        public Response ObtenerTodosMoldes()
        {
            try
            {
                var listMoldes = MoldeManager.Moldes.ToList();

                return new Response
                {
                    Result = "OK",
                    Record = new
                    {
                        ListMoldes = listMoldes
                    }
                };
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", MoldeRepository.GetDetailedError(ex)) };
            }
        }

        public async Task<Response> GuardaMolde(Molde molde)
        {
            try
            {
                if (!MoldeManager.Moldes.Where(x => x.IdMolde == molde.IdMolde).Any())
                {
                    molde.IdMolde = molde.IdMolde == Guid.Empty ? Guid.NewGuid() : molde.IdMolde;
                    await MoldeManager.CreateAsync(molde);
                }
                else
                {
                    await MoldeManager.UpdateAsync(molde);
                }
                return new Response
                {
                    Result = "OK"
                };
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", MoldeRepository.GetDetailedError(ex)) };
            }
        }

        public async Task<bool> CambiaEstatus(Guid idMolde, bool activo)
        {
            var molde = MoldeManager.Moldes.Where(x => x.IdMolde == idMolde).FirstOrDefault();
            molde.Activo = activo;
            await MoldeManager.UpdateAsync(molde);
            return true;
        }
    }
}
