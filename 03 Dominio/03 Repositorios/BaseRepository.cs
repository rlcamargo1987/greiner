﻿using GreinerAssistec.Dominio.Modelo.Administradores;
using System;

namespace GreinerAssistec.Dominio.Repositorio
{
    public class BaseRepository
    {
        private AppUsuarioManager _userManager;
        private AppEstatusUsuarioManager _estatusUsuarioManager;
        private AppMoldeManager _moldeManager;
        private AppComponenteManager _componenteManager;
        private AppInsertoManager _insertoManager;
        private AppEmpaqueManager _empaqueManager;
        private AppMaquinaManager _maquinaManager;
        private AppMateriaPrimaManager _materiaPrimaManager;
        public AppUsuarioManager UserManager
        {
            get
            {
                if (_userManager == null)
                    _userManager = AppUsuarioManager.Create();
                return _userManager;
            }
            private set
            {
                _userManager = value;
            }
        }

        public AppEstatusUsuarioManager EstatusUsuarioManager
        {
            get
            {
                if (_estatusUsuarioManager == null)
                    _estatusUsuarioManager = AppEstatusUsuarioManager.Create();
                return _estatusUsuarioManager;
            }
            private set
            {
                _estatusUsuarioManager = value;
            }

        }

        public AppMoldeManager MoldeManager
        {
            get
            {
                if (_moldeManager == null)
                    _moldeManager = AppMoldeManager.Create();
                return _moldeManager;
            }
            private set
            {
                _moldeManager = value;
            }
        }

        public AppComponenteManager ComponenteManager
        {
            get
            {
                if (_componenteManager == null)
                    _componenteManager = AppComponenteManager.Create();
                return _componenteManager;
            }
            private set
            {
                _componenteManager = value;
            }
        }

        public AppInsertoManager InsertoManager
        {
            get
            {
                if (_insertoManager == null)
                    _insertoManager = AppInsertoManager.Create();
                return _insertoManager;
            }
            private set
            {
                _insertoManager = value;
            }
        }

        public AppEmpaqueManager EmpaqueManager
        {
            get
            {
                if (_empaqueManager == null)
                    _empaqueManager = AppEmpaqueManager.Create();
                return _empaqueManager;
            }
            private set
            {
                _empaqueManager = value;
            }
        }

        public AppMaquinaManager MaquinaManager
        {
            get
            {
                if (_maquinaManager == null)
                    _maquinaManager = AppMaquinaManager.Create();
                return _maquinaManager;
            }
            private set
            {
                _maquinaManager = value;
            }
        }

        public AppMateriaPrimaManager MateriaPrimaManager
        {
            get
            {
                if (_materiaPrimaManager == null)
                    _materiaPrimaManager = AppMateriaPrimaManager.Create();
                return _materiaPrimaManager;
            }
            private set
            {
                _materiaPrimaManager = value;
            }
        }

        public static string GetDetailedError(Exception ex)
        {
            return string.Format("\n Message: {0}, \nData: {1}, \nInnerException: {2}, \nStackTrace: {3}, \nSource: {4}",
                    ex.Message,
                    ex.Data != null ? ex.Data.ToString() : "",
                    ex.InnerException != null ? ex.InnerException.ToString() : "",
                    ex.StackTrace, ex.Source);
        }


    }
}
