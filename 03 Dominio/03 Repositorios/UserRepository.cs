﻿using GreinerAssistec.Dominio.Modelo.Administradores.Base;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Dominio.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Repositorio
{
    public class UserRepository : BaseRepository
    {
        public static UserRepository Instance()
        {
            return new UserRepository();
        }

        public UserRepository()
        {
        }

        public Response GetAllUsers()
        {
            try
            {
                var listUsers = UserManager.Usuarios.ToList();

                return new Response
                {
                    Result = "OK",
                    Record = new
                    {
                        ListUsers = listUsers
                    }
                };
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", UserRepository.GetDetailedError(ex)) };
            }
        }

        public Response GetUserByIdOrEmail(string IdOrEmail)
        {
            try
            {
                Guid guid;
                var itsId = Guid.TryParse(IdOrEmail, out guid);

                if (UserManager.Usuarios.Where(x => itsId ? x.IdUsuario == guid : x.Correo == IdOrEmail).Any())
                {
                    var user = UserManager.Usuarios.Where(x => itsId ? x.IdUsuario == guid : x.Correo == IdOrEmail).FirstOrDefault();
                    return new Response
                    {
                        Result = "OK",
                        Record = user
                    };
                }
                else
                {
                    return new Response
                    {
                        Result = "ERROR",
                        Message = "El usuario no existe"
                    };
                }
            }
            catch (Exception ex)
            {
                return new Response { Result = "ERROR", Message = string.Format("Hubo un problema interno. Error:{0}", UserRepository.GetDetailedError(ex)) };
            }
        }

        public static string GetDetailedError(Exception ex)
        {
            return string.Format("\n Message: {0}, \nData: {1}, \nInnerException: {2}, \nStackTrace: {3}, \nSource: {4}",
                    ex.Message,
                    ex.Data != null ? ex.Data.ToString() : "",
                    ex.InnerException != null ? ex.InnerException.ToString() : "",
                    ex.StackTrace, ex.Source);
        }

        public Usuario VerificaCredenciales(string correo, string contrasena)
        {
            var decrypt = new Crypt();
            var encriptPass = decrypt.Encrypting(contrasena);
            return UserManager
                       .Usuarios.Where(x => x.Correo == correo &&
                                       x.Contrasena == encriptPass)
                       .FirstOrDefault();
        }

        public async Task<bool> GuardaUsuario(Usuario usuario)
        {
            var decrypt = new Crypt();
            var encriptPass = decrypt.Encrypting(usuario.Contrasena);
            usuario.Contrasena = encriptPass;
            if (!UserManager.Usuarios.Where(x => x.IdUsuario == usuario.IdUsuario).Any())
            {
                await UserManager.CreateAsync(usuario);
                var notification = new Notification
                {
                    To = usuario.Correo,
                    Subject = "Aviso de notificación de activacion de cuentas. Greiner Assistec.",
                    Html = false,
                    Body = string.Format("Hola, su usuario ha sido activado. Puede entrar a su cuenta en la liga: http://winco2017-001-site2.dtempurl.com/, con las credenciales siguientes: \n Correo: {0}, Contraseña: {1}",usuario.Correo, decrypt.Decrypting(usuario.Contrasena)),
                    From = "noreply@mail.com"
                };
                try
                {
                    NotificacionService.Instance().EnviarCorreo(notification);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }

            }
            else
            {
                await UserManager.UpdateAsync(usuario);
            }
            return true;
        }

        public Usuario ObtenUsuario(string idUsuario)
        {
            var user = UserManager.Usuarios.Where(x => x.IdUsuario == new Guid(idUsuario)).Single();
            return user;
        }

        public Usuario ObtenUsuario(Guid idUsuario)
        {
            var user = UserManager.Usuarios.Where(x => x.IdUsuario == idUsuario).FirstOrDefault();
            return user;
        }

        public string ObtenContrasena(string correo)
        {
            var contrasena = UserManager.Usuarios.Where(x => x.Correo == correo).FirstOrDefault().Contrasena;
            var decrypt = new Crypt();
            var decriptPass = decrypt.Decrypting(contrasena);
            return decriptPass;
        }

        public Usuario ObtenInfoUsuario(Guid IdUsuario)
        {
            /*, ImgProfile = x.Foto, x.Rol */
            var decrypt = new Crypt();
            var original = UserManager.Usuarios.Where(x => x.IdUsuario == IdUsuario).ToList().Select(x => new Usuario()
            {
                IdUsuario = IdUsuario,
                Nombre = !string.IsNullOrEmpty(x.Nombre) ?
                                    string.Format("{0}{1}{2}", (!string.IsNullOrEmpty(x.Nombre) ? x.Nombre + " " : ""),
                                    (!string.IsNullOrEmpty(x.ApellidoPaterno) ? x.ApellidoMaterno + " " : ""),
                                    (!string.IsNullOrEmpty(x.ApellidoPaterno) ? x.ApellidoMaterno + " " : "")) :
                                    x.Correo,
                Correo = x.Correo,
                Contrasena = decrypt.Decrypting(x.Contrasena)
            }).FirstOrDefault();
            return original;
        }

        public List<Usuario> ObtenerTodosUsuarios()
        {

            var decrypt = new Crypt();
            var original = UserManager.Usuarios.ToList().Select(x => new Usuario()
            {
                IdUsuario = x.IdUsuario,
                Correo = x.Correo,
                //NombreCompleto = !string.IsNullOrEmpty(x.Nombre) ?
                //                    string.Format("{0}{1}{2}", (!string.IsNullOrEmpty(x.Nombre) ? x.Nombre + " " : ""),
                //                    (!string.IsNullOrEmpty(x.ApellidoPaterno) ? x.ApellidoMaterno + " " : ""),
                //                    (!string.IsNullOrEmpty(x.ApellidoPaterno) ? x.ApellidoMaterno + " " : "")) :
                //                    x.Correo,
                Nombre = x.Nombre ?? "",
                Contrasena = "",
                ApellidoPaterno = x.ApellidoPaterno ?? "",
                ApellidoMaterno = x.ApellidoMaterno ?? "",
                Genero = x.Genero,
                TelefonoMovil = x.TelefonoMovil ?? "",
                FechaNacimiento = x.FechaNacimiento ?? new DateTime(),
                Area = x.Area ?? "",
                Departamento = x.Departamento ?? "",
                Puesto = x.Puesto ?? "",
                PerfilUsuario = x.PerfilUsuario ?? "",
                FechaIngreso = x.FechaIngreso ?? new DateTime(),
                Estatus = x.Estatus
            }).ToList();
            return original;
        }

        public List<EstatusUsuario> ObtenerEstatusUsuarios()
        {
            var original = EstatusUsuarioManager.EstatusUsuarios.ToList();
            return original;
        }

        public async Task<bool> CambiaEstatusUsuarios(Guid IdUsuario, Guid Estatus)
        {
            var original = UserManager.Usuarios.Where(x => x.IdUsuario == IdUsuario).FirstOrDefault();
            original.Estatus = Estatus;
            await UserManager.UpdateAsync(original);
            return true;
        }

    }
}
