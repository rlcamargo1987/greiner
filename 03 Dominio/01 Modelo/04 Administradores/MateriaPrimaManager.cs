﻿using GreinerAssistec.Dominio.Modelo.Contexto;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Infraestructura.Base.Administradores;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using GreinerAssistec.Infraestructura.Datos.Almacen;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Administradores
{
    public class AppMateriaPrimaManager : MateriaPrimaManager<MateriaPrima, Guid>
    {
        public AppMateriaPrimaManager(MateriaPrimaStore<MateriaPrima> MateriaPrimaStore)
            : base(MateriaPrimaStore)
        {
        }

        public static AppMateriaPrimaManager Create(
            IdentityFactoryOptions<AppMateriaPrimaManager> options,
            IOwinContext context)
        {
            var con = new GreinerAssistectDbContext();
            var store = new MateriaPrimaStore<MateriaPrima>(con);
            var manager = new AppMateriaPrimaManager(store);

            return manager;
        }

        public static AppMateriaPrimaManager Create()
        {
            var con = new GreinerAssistectDbContext();
            var store = new MateriaPrimaStore<MateriaPrima>(con);
            var manager = new AppMateriaPrimaManager(store);

            return manager;
        }
    }

}
