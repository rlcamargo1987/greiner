﻿using GreinerAssistec.Dominio.Modelo.Contexto;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Infraestructura.Base.Administradores;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using GreinerAssistec.Infraestructura.Datos.Almacen;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Administradores
{
    public class AppMaquinaManager : MaquinaManager<Maquina, Guid>
    {
        public AppMaquinaManager(MaquinaStore<Maquina> MaquinaStore)
            : base(MaquinaStore)
        {
        }

        public static AppMaquinaManager Create(
            IdentityFactoryOptions<AppMaquinaManager> options,
            IOwinContext context)
        {
            var con = new GreinerAssistectDbContext();
            var store = new MaquinaStore<Maquina>(con);
            var manager = new AppMaquinaManager(store);

            return manager;
        }

        public static AppMaquinaManager Create()
        {
            var con = new GreinerAssistectDbContext();
            var store = new MaquinaStore<Maquina>(con);
            var manager = new AppMaquinaManager(store);

            return manager;
        }
    }

}
