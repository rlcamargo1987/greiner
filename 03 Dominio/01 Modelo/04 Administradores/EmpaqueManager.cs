﻿using GreinerAssistec.Dominio.Modelo.Contexto;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Infraestructura.Base.Administradores;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using GreinerAssistec.Infraestructura.Datos.Almacen;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Administradores
{
    public class AppEmpaqueManager : EmpaqueManager<Empaque, Guid>
    {
        public AppEmpaqueManager(EmpaqueStore<Empaque> EmpaqueStore)
            : base(EmpaqueStore)
        {
        }

        public static AppEmpaqueManager Create(
            IdentityFactoryOptions<AppEmpaqueManager> options,
            IOwinContext context)
        {
            var con = new GreinerAssistectDbContext();
            var store = new EmpaqueStore<Empaque>(con);
            var manager = new AppEmpaqueManager(store);

            return manager;
        }

        public static AppEmpaqueManager Create()
        {
            var con = new GreinerAssistectDbContext();
            var store = new EmpaqueStore<Empaque>(con);
            var manager = new AppEmpaqueManager(store);

            return manager;
        }
    }

}
