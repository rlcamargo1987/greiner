﻿using GreinerAssistec.Dominio.Modelo.Contexto;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Infraestructura.Base.Administradores;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using GreinerAssistec.Infraestructura.Datos.Almacen;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Administradores
{
    public class AppMoldeManager : MoldeManager<Molde, Guid>
    {
        public AppMoldeManager(MoldeStore<Molde> MoldeStore)
            : base(MoldeStore)
        {
        }

        public static AppMoldeManager Create(
            IdentityFactoryOptions<AppMoldeManager> options,
            IOwinContext context)
        {
            var con = new GreinerAssistectDbContext();
            var store = new MoldeStore<Molde>(con);
            var manager = new AppMoldeManager(store);

            return manager;
        }

        public static AppMoldeManager Create()
        {
            var con = new GreinerAssistectDbContext();
            var store = new MoldeStore<Molde>(con);
            var manager = new AppMoldeManager(store);

            return manager;
        }
    }

}
