﻿using GreinerAssistec.Dominio.Modelo.Contexto;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Infraestructura.Base.Administradores;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using GreinerAssistec.Infraestructura.Datos.Almacen;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Administradores
{
    public class AppComponenteManager : ComponenteManager<Componente, Guid>
    {
        public AppComponenteManager(ComponenteStore<Componente> ComponenteStore)
            : base(ComponenteStore)
        {
        }

        public static AppComponenteManager Create(
            IdentityFactoryOptions<AppComponenteManager> options,
            IOwinContext context)
        {
            var con = new GreinerAssistectDbContext();
            var store = new ComponenteStore<Componente>(con);
            var manager = new AppComponenteManager(store);

            return manager;
        }

        public static AppComponenteManager Create()
        {
            var con = new GreinerAssistectDbContext();
            var store = new ComponenteStore<Componente>(con);
            var manager = new AppComponenteManager(store);

            return manager;
        }
    }

}
