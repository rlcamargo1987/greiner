﻿using GreinerAssistec.Dominio.Modelo.Contexto;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Infraestructura.Base.Administradores;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using GreinerAssistec.Infraestructura.Datos.Almacen;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Administradores
{
    public class AppUsuarioManager : UsuarioManager<Usuario, Guid>
    {
        public AppUsuarioManager(UsuarioStore<Usuario> UsuarioStore)
            : base(UsuarioStore)
        {
        }

        public static AppUsuarioManager Create(
            IdentityFactoryOptions<AppUsuarioManager> options,
            IOwinContext context)
        {
            var con = new GreinerAssistectDbContext();
            var store = new UsuarioStore<Usuario>(con);
            var manager = new AppUsuarioManager(store);

            return manager;
        }

        public static AppUsuarioManager Create()
        {
            var con = new GreinerAssistectDbContext();
            var store = new UsuarioStore<Usuario>(con);
            var manager = new AppUsuarioManager(store);

            return manager;
        }
    }

}
