﻿using GreinerAssistec.Dominio.Modelo.Contexto;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Infraestructura.Base.Administradores;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using GreinerAssistec.Infraestructura.Datos.Almacen;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Administradores
{
    public class AppInsertoManager : InsertoManager<Inserto, Guid>
    {
        public AppInsertoManager(InsertoStore<Inserto> InsertoStore)
            : base(InsertoStore)
        {
        }

        public static AppInsertoManager Create(
            IdentityFactoryOptions<AppInsertoManager> options,
            IOwinContext context)
        {
            var con = new GreinerAssistectDbContext();
            var store = new InsertoStore<Inserto>(con);
            var manager = new AppInsertoManager(store);

            return manager;
        }

        public static AppInsertoManager Create()
        {
            var con = new GreinerAssistectDbContext();
            var store = new InsertoStore<Inserto>(con);
            var manager = new AppInsertoManager(store);

            return manager;
        }
    }

}
