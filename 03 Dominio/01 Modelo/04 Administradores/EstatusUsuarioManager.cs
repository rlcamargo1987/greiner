﻿using GreinerAssistec.Dominio.Modelo.Contexto;
using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Infraestructura.Base.Administradores;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using GreinerAssistec.Infraestructura.Datos.Almacen;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Administradores
{
    public class AppEstatusUsuarioManager : EstatusUsuarioManager<EstatusUsuario, Guid>
    {
        public AppEstatusUsuarioManager(EstatusUsuarioStore<EstatusUsuario> EstatusUsuarioStore)
            : base(EstatusUsuarioStore)
        {
        }

        public static AppEstatusUsuarioManager Create(
            IdentityFactoryOptions<AppEstatusUsuarioManager> options,
            IOwinContext context)
        {
            var con = new GreinerAssistectDbContext();
            var store = new EstatusUsuarioStore<EstatusUsuario>(con);
            var manager = new AppEstatusUsuarioManager(store);

            return manager;
        }

        public static AppEstatusUsuarioManager Create()
        {
            var con = new GreinerAssistectDbContext();
            var store = new EstatusUsuarioStore<EstatusUsuario>(con);
            var manager = new AppEstatusUsuarioManager(store);

            return manager;
        }
    }

}
