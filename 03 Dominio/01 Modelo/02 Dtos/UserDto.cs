﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GreinerAssistec.Dominio.Modelo.Dtos
{
    public class UserDto
    {
        public Guid? IdUsuario { get; set; }
        public string Correo { get; set; }
        public string Contrasena { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public bool? Genero { get; set; }
        public string TelefonoMovil { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Area { get; set; }
        public string Departamento { get; set; }
        public string Puesto { get; set; }
        public string PerfilUsuario { get; set; }
        public DateTime? FechaIngreso { get; set; }
    }

}
