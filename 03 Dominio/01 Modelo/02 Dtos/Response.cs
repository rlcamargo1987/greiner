﻿using System;

namespace GreinerAssistec.Dominio.Modelo.Dtos
{
    public class CredentialsDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public bool TerminosYCondiciones { get; set; }
    }

    public class ResultType
    {
        public static string OK { private set { } get { return "OK"; } }
        public static string Error { private set { } get { return "Error"; } }
    }

    public class Response
    {
        /// <summary>
        ///Status de la respuesta
        /// </summary>
        public string Result
        { get; set; }

        /// <summary>
        /// Datos para poblar el grid
        /// </summary>
        public object Records { get; set; }

        /// <summary>
        /// elemento
        /// </summary>
        public object Record { get; set; }

        /// <summary>
        /// Total de registros 
        /// </summary>
        public int TotalRecordCount { get; set; }
        /// <summary>
        /// Datos para llenar un combobox
        /// </summary>
        public object Options
        { get; set; }
        /// <summary>
        /// Mensajes si hubo un error
        /// </summary>
        public string Message
        { get; set; }
    }
}

