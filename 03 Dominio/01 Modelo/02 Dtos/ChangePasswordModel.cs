﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Dtos
{
    public class ChangePasswordModel
    {
        public Guid IdUsuario { get; set; }
        public string Email { get; set; }
        public string oldPassword { get; set; }
        public string password { get; set; }
        public string password2 { get; set; }
    }
}
