﻿using GreinerAssistec.Dominio.Modelo.Entidades;
using GreinerAssistec.Infraestructura.Base.Entidades.Anotaciones;
using GreinerAssistec.Infraestructura.Datos.Contexto;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Contexto
{
    public class GreinerAssistectDbContext : GreinerAssistecDbContext<Usuario, EstatusUsuario, Molde, Componente, Inserto, Empaque, Maquina, MateriaPrima, Guid>
    {
        /// <summary>
        ///     Default constructor which uses the DefaultConnection
        /// </summary>
        public GreinerAssistectDbContext() : this("DefaultConnection")
        {            // remove default initializer
            Database.SetInitializer<GreinerAssistectDbContext>(null);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        /// <summary>
        ///     Constructor which takes the connection string to use
        /// </summary>
        /// <param name="nameOrConnectionString"></param>
        public GreinerAssistectDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            // remove default initializer
            Database.SetInitializer<GreinerAssistectDbContext>(null);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        /// <summary>
        ///     Constructs a new context instance using the existing connection to connect to a database, and initializes it from
        ///     the given model.  The connection will not be disposed when the context is disposed if contextOwnsConnection is
        ///     false.
        /// </summary>
        /// <param name="existingConnection">An existing connection to use for the new context.</param>
        /// <param name="model">The model that will back this context.</param>
        /// <param name="contextOwnsConnection">
        ///     Constructs a new context instance using the existing connection to connect to a
        ///     database, and initializes it from the given model.  The connection will not be disposed when the context is
        ///     disposed if contextOwnsConnection is false.
        /// </param>
        public GreinerAssistectDbContext(DbConnection existingConnection, DbCompiledModel model, bool contextOwnsConnection) : base(existingConnection, model, contextOwnsConnection)
        {
            // remove default initializer
            Database.SetInitializer<GreinerAssistectDbContext>(null);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        /// <summary>
        ///     Constructs a new context instance using conventions to create the name of
        ///     the database to which a connection will be made, and initializes it from
        ///     the given model.  The by-convention name is the full name (namespace + class
        ///     name) of the derived context class.  See the class remarks for how this is
        ///     used to create a connection.
        /// </summary>
        /// <param name="model">The model that will back this context.</param>
        public GreinerAssistectDbContext(DbCompiledModel model) : base(model)
        {
            // remove default initializer
            Database.SetInitializer<GreinerAssistectDbContext>(null);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        /// <summary>
        ///     Constructs a new context instance using the existing connection to connect
        ///     to a database.  The connection will not be disposed when the context is disposed
        ///     if contextOwnsConnection is false.
        /// </summary>
        /// <param name="existingConnection">An existing connection to use for the new context.</param>
        /// <param name="contextOwnsConnection">If set to true the connection is disposed when the context is disposed, otherwise
        ///     the caller must dispose the connection.
        /// </param>
        public GreinerAssistectDbContext(DbConnection existingConnection, bool contextOwnsConnection) : base(existingConnection, contextOwnsConnection)
        {
            // remove default initializer
            Database.SetInitializer<GreinerAssistectDbContext>(null);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        /// <summary>
        ///     Constructs a new context instance using the given string as the name or connection
        ///     string for the database to which a connection will be made, and initializes
        ///     it from the given model.  See the class remarks for how this is used to create
        ///     a connection.
        /// </summary>
        /// <param name="nameOrConnectionString">Either the database name or a connection string.</param>
        /// <param name="model">The model that will back this context.</param>
        public GreinerAssistectDbContext(string nameOrConnectionString, DbCompiledModel model) : base(nameOrConnectionString, model)
        {
            // remove default initializer
            Database.SetInitializer<GreinerAssistectDbContext>(null);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("dbo");
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            if (modelBuilder == null)
            {
                throw new ArgumentNullException("modelBuilder");
            }
            modelBuilder.Entity<Usuario>().HasKey(r => new { r.IdUsuario }).ToTable("Usuario", "dbo");

            modelBuilder.Entity<EstatusUsuario>().HasKey(r => new { r.IdEstatusUsuario }).ToTable("EstatusUsuario", "dbo");
            modelBuilder.Entity<Molde>().HasKey(r => new { r.IdMolde }).ToTable("Molde", "dbo");
            modelBuilder.Entity<Componente>().HasKey(r => new { r.IdComponente }).ToTable("Componente", "dbo");
            modelBuilder.Entity<Inserto>().HasKey(r => new { r.IdInserto }).ToTable("Inserto", "dbo");
            modelBuilder.Entity<Empaque>().HasKey(r => new { r.IdEmpaque }).ToTable("Empaque", "dbo");
            modelBuilder.Entity<Maquina>().HasKey(r => new { r.IdMaquina }).ToTable("Maquina", "dbo");
            modelBuilder.Entity<MateriaPrima>().HasKey(r => new { r.IdMateriaPrima }).ToTable("MateriaPrima", "dbo");
            // Precision attribute for decimals
            Precision.ConfigureModelBuilder(modelBuilder);
        }
    }
}
