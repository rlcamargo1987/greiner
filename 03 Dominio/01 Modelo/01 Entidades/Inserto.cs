﻿using GreinerAssistec.Infraestructura.Base.Entidades;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Entidades
{
    public class Inserto : Inserto<Guid>, IInserto<Guid>
    {
        /// <summary>
        ///     Constructor which creates a new Guid for the Id
        /// </summary>
        public Inserto() : base()
        {
        }

        /// <summary>
        ///     Constructor that takes a userName
        /// </summary>
        /// <param name="userName"></param>
        public Inserto(Guid IdInserto) : this()
        {
            this.IdInserto = IdInserto;
        }
    }
    //
}
