﻿using GreinerAssistec.Infraestructura.Base.Entidades;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Entidades
{
    public class EstatusUsuario : EstatusUsuario<Guid>, IEstatusUsuario<Guid>
    {
        /// <summary>
        ///     Constructor which creates a new Guid for the Id
        /// </summary>
        public EstatusUsuario() : base()
        {
        }

        /// <summary>
        ///     Constructor that takes a userName
        /// </summary>
        /// <param name="userName"></param>
        public EstatusUsuario(Guid IdEstatusUsuario) : this()
        {
            this.IdEstatusUsuario = IdEstatusUsuario;
        }
    }
    //
}
