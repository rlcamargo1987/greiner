﻿using GreinerAssistec.Infraestructura.Base.Entidades;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Entidades
{
    public class Empaque : Empaque<Guid>, IEmpaque<Guid>
    {
        /// <summary>
        ///     Constructor which creates a new Guid for the Id
        /// </summary>
        public Empaque() : base()
        {
        }

        /// <summary>
        ///     Constructor that takes a userName
        /// </summary>
        /// <param name="userName"></param>
        public Empaque(Guid IdEmpaque) : this()
        {
            this.IdEmpaque = IdEmpaque;
        }
    }
    //
}
