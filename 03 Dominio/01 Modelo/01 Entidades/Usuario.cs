﻿using GreinerAssistec.Infraestructura.Base.Entidades;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Dominio.Modelo.Entidades
{
    public class Usuario : Usuario<Guid>, IUsuario<Guid>
    {
        /// <summary>
        ///     Constructor which creates a new Guid for the Id
        /// </summary>
        public Usuario() : base()
        {
        }

        /// <summary>
        ///     Constructor that takes a userName
        /// </summary>
        /// <param name="userName"></param>
        public Usuario(Guid IdUsuario) : this()
        {
            this.IdUsuario = IdUsuario;
        }
    }
    //
}
