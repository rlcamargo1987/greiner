﻿using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Modelo.Entidades
{
    public class EstatusUsuario<TKey> : IEstatusUsuario<TKey>
    {
        public TKey IdEstatusUsuario { get; set; }
        public string Nombre { get; set; }
        public bool Activo { get; set; }
    }

}
