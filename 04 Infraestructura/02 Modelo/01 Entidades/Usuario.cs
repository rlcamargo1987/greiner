﻿using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Modelo.Entidades
{
    public class Usuario<TKey> : IUsuario<TKey>
    {
        public TKey IdUsuario { get; set; } //int not null autoinc
        public string Foto { get; set; }
        public string Correo { get; set; }
        public string Contrasena { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public bool Genero { get; set; } //0 Hombre, 1 mujer
        public string TelefonoMovil { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Area { get; set; }
        public string Departamento { get; set; }
        public string Puesto { get; set; }
        public string PerfilUsuario { get; set; }
        public DateTime? FechaIngreso { get; set; }
        public Guid Estatus { get; set; }
    }

}
