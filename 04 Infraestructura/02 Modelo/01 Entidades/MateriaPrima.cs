﻿
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Modelo.Entidades
{
    public class MateriaPrima<TKey> : IMateriaPrima<TKey>
    {
        public TKey IdMateriaPrima { get; set; }
        public int NumeroParteInterno { get; set; }
        public int Tipo { get; set; }      //Pigmento, Resina, Aditivo, Inserto, Etiquetas 
        public string Descripcion { get; set; }
        public bool Secado { get; set; }    //no-si
        public decimal? Temperatura { get; set; }
        public string Tiempo { get; set; }
        public string Familia { get; set; }
        public decimal? PorcentajeHumedad { get; set; }
        public bool AmorfoSemi { get; set; }                //Amorfo / Semicristalino
        public string Polimero { get; set; }    
    }
}
