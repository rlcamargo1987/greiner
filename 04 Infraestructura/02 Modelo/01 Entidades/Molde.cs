﻿
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Modelo.Entidades
{
    public class Molde<TKey> : IMolde<TKey>
    {
        public TKey IdMolde { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public int TonelajeCotizado { get; set; }
        public int TonelajeReal { get; set; }
        public string Cliente { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
    }

}
