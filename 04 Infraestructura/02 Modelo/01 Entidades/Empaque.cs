﻿
using GreinerAssistec.Infraestructura.Base.Entidades;
using GreinerAssistec.Infraestructura.Base.Entidades.Anotaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Modelo.Entidades
{
    public class Empaque<TKey> : IEmpaque<TKey>
    {
        public TKey IdEmpaque { get; set; }
        public int NPInterno { get; set; }
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public int? CajasPallets { get; set; }
        [Precision(20, 2)]
        public decimal? KilosSoporta { get; set; }
        public int? NivelesCajaTarima { get; set; }
        public bool Estatus { get; set; }
    }

}
