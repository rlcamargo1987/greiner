﻿
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Modelo.Entidades
{
    public class Componente<TKey> : IComponente<TKey>
    {
        public TKey IdComponente { get; set; }
        public int NPInterno { get; set; }
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public string Medida { get; set; }
        public string Cliente { get; set; }
    }
}
