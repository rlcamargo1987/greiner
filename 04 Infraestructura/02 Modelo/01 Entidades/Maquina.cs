﻿using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Modelo.Entidades
{
    public class Maquina<TKey> : IMaquina<TKey>
    {
        public TKey IdMaquina { get; set; }
        public string NumeroActivo { get; set; }
        public string Nombre { get; set; }
        public int? Tonelaje { get; set; }
        public int? DiametroHusillo { get; set; }
        public string Comentarios { get; set; }
        public int Tipo { get; set; }  // Ensamble Inyeccion Trial
        public string Descripcion { get; set; }
    }
}
