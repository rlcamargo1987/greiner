﻿
using GreinerAssistec.Infraestructura.Base.Entidades;
using GreinerAssistec.Infraestructura.Base.Entidades.Anotaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Modelo.Entidades
{
    public class Inserto<TKey> : IInserto<TKey>
    {
        public TKey IdInserto { get; set; }
        public string Codigo { get; set; }
        [Precision(25, 5)]
        public decimal Peso { get; set; }
        public int Cavidades { get; set; }
        public int CavidadesFuncionales { get; set; }
        [Precision(25, 5)]
        public decimal Colada { get; set; }
        public bool ColadaCotizada { get; set; }
        public int Ciclo { get; set; }
        public int CicloReal { get; set; }
        [Precision(10, 5)]
        public decimal Maqhrs { get; set; }
        public string Instruccion { get; set; }
    }

}
