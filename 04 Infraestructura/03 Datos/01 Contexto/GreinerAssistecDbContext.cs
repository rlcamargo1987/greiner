﻿
using GreinerAssistec.Infraestructura.Base.Entidades;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace GreinerAssistec.Infraestructura.Datos.Contexto
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TUsuario"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public class GreinerAssistecDbContext<TUsuario, TEstatusUsuario, TMolde, TComponente, TInserto, TEmpaque, TMaquina, TMateriaPrima, TKey> : DbContext
        where TUsuario : IUsuario<TKey>
        where TEstatusUsuario : IEstatusUsuario<TKey>
        where TMolde : IMolde<TKey>
        where TComponente : IComponente<TKey>
        where TInserto : IInserto<TKey>
        where TEmpaque : IEmpaque<TKey>
        where TMaquina : IMaquina<TKey>
        where TMateriaPrima : IMateriaPrima<TKey>
    {
        /// <summary>
        ///     Default constructor which uses the "DefaultConnection" connectionString
        /// </summary>
        public GreinerAssistecDbContext() : this("DefaultConnection")
        {
        }

        /// <summary>
        ///     Constructor which takes the connection string to use
        /// </summary>
        /// <param name="nameOrConnectionString"></param>
        public GreinerAssistecDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }

        /// <summary>
        ///     Constructs a new context instance using the existing connection to connect to a database, and initializes it from
        ///     the given model.  The connection will not be disposed when the context is disposed if contextOwnsConnection is
        ///     false.
        /// </summary>
        /// <param name="existingConnection">An existing connection to use for the new context.</param>
        /// <param name="model">The model that will back this context.</param>
        /// <param name="contextOwnsConnection">
        ///     Constructs a new context instance using the existing connection to connect to a
        ///     database, and initializes it from the given model.  The connection will not be disposed when the context is
        ///     disposed if contextOwnsConnection is false.
        /// </param>
        public GreinerAssistecDbContext(DbConnection existingConnection, DbCompiledModel model, bool contextOwnsConnection) : base(existingConnection, model, contextOwnsConnection)
        {
        }

        /// <summary>
        ///     Constructs a new context instance using conventions to create the name of
        ///     the database to which a connection will be made, and initializes it from
        ///     the given model.  The by-convention name is the full name (namespace + class
        ///     name) of the derived context class.  See the class remarks for how this is
        ///     used to create a connection.
        /// </summary>
        /// <param name="model">The model that will back this context.</param>
        public GreinerAssistecDbContext(DbCompiledModel model) : base(model)
        {
        }

        /// <summary>
        ///     Constructs a new context instance using the existing connection to connect
        ///     to a database.  The connection will not be disposed when the context is disposed
        ///     if contextOwnsConnection is false.
        /// </summary>
        /// <param name="existingConnection">An existing connection to use for the new context.</param>
        /// <param name="contextOwnsConnection">If set to true the connection is disposed when the context is disposed, otherwise
        ///     the caller must dispose the connection.
        /// </param>
        public GreinerAssistecDbContext(DbConnection existingConnection, bool contextOwnsConnection) : base(existingConnection, contextOwnsConnection)
        {
        }

        /// <summary>
        ///     Constructs a new context instance using the given string as the name or connection
        ///     string for the database to which a connection will be made, and initializes
        ///     it from the given model.  See the class remarks for how this is used to create
        ///     a connection.
        /// </summary>
        /// <param name="nameOrConnectionString">Either the database name or a connection string.</param>
        /// <param name="model">The model that will back this context.</param>
        public GreinerAssistecDbContext(string nameOrConnectionString, DbCompiledModel model) : base(nameOrConnectionString, model)
        {
        }

    }
}
