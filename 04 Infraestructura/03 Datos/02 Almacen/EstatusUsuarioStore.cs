﻿using GreinerAssistec.Infraestructura.Base.Consultas;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using GreinerAssistec.Infraestructura.Base.Almacen;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Datos.Almacen
{
    public class EstatusUsuarioStore<TEstatusUsuario> :
    IQueryableEstatusUsuarioStore<TEstatusUsuario>,
    IEstatusUsuarioStore<TEstatusUsuario>, IDisposable
where TEstatusUsuario : EstatusUsuario<Guid>
        //where TKey : IEquatable<TKey>
    {
        //private readonly IDbSet<TUserLogin> _logins;

        private bool _disposed;

        private EntityStore<TEstatusUsuario> _EstatusUsuarioStore;

        /// <summary>
        ///     If true will call SaveChanges after Create/Update/Delete
        /// </summary>
        public bool AutoSaveChanges
        {
            get;
            set;
        }

        /// <summary>
        ///     Context for the store
        /// </summary>
        public DbContext Context
        {
            get;
            private set;
        }

        /// <summary>
        ///     If true will call dispose on the DbContext during Dispose
        /// </summary>
        public bool DisposeContext
        {
            get;
            set;
        }

        /// <summary>
        ///     Returns an IQueryable of users
        /// </summary>
        public IQueryable<TEstatusUsuario> EstatusUsuarios
        {
            get
            {
                return this._EstatusUsuarioStore.EntitySet;
            }
        }

        /// <summary>
        ///     Constructor which takes a db context and wires up the stores with default instances using the context
        /// </summary>
        /// <param name="context"></param>
        public EstatusUsuarioStore(DbContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            this.Context = context;
            this.AutoSaveChanges = true;
            this._EstatusUsuarioStore = new EntityStore<TEstatusUsuario>(context);
        }

        /// <summary>
        ///     Insert an entity
        /// </summary>
        /// <param name="user"></param>
        public virtual async Task CreateAsync(TEstatusUsuario city)
        {
            this.ThrowIfDisposed();
            if (city == null)
            {
                throw new ArgumentNullException("user");
            }
            this._EstatusUsuarioStore.Create(city);
            await this.SaveChanges().WithCurrentCulture();
        }

        /// <summary>
        ///     Mark an entity for deletion
        /// </summary>
        /// <param name="user"></param>
        public virtual async Task DeleteAsync(TEstatusUsuario city)
        {
            this.ThrowIfDisposed();
            if (city == null)
            {
                throw new ArgumentNullException("city");
            }
            this._EstatusUsuarioStore.Delete(city);
            await this.SaveChanges().WithCurrentCulture();
        }

        /// <summary>
        ///     Dispose the store
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     If disposing, calls dispose on the Context.  Always nulls out the Context
        /// </summary>
        /// <param name="disposing"></param>
        public virtual void Dispose(bool disposing)
        {
            if (this.DisposeContext && disposing && this.Context != null)
            {
                this.Context.Dispose();
            }
            this._disposed = true;
            this.Context = null;
            this._EstatusUsuarioStore = null;
        }

        /// <summary>
        ///     Find a user by id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public virtual Task<TEstatusUsuario> FindByIdAsync(Guid IdEstatusUsuario)
        {
            this.ThrowIfDisposed();
            throw new NotImplementedException();
            //return this.GetUserAggregateAsync((TEstatusUsuario u) => u.Id.Equals(userId));
        }
        /*public virtual Task<TEstatusUsuario> FindByIdAsync(TKey IdEstatusUsuario)
        {
            throw new NotImplementedException();
        }*/

        public async Task<TEstatusUsuario> FindByNameAsync(string Name)
        {
            this.ThrowIfDisposed();
            if (Name == null)
            {
                throw new ArgumentNullException("Name");
            }
            var cultureAwaiter = this._EstatusUsuarioStore.DbEntitySet.Where(x => x.Nombre == Name).FirstOrDefaultAsync().WithCurrentCulture();
            return await cultureAwaiter;
        }

        private async Task SaveChanges()
        {
            if (this.AutoSaveChanges)
            {
                //TaskExtensions.CultureAwaiter<int> cultureAwaiter = 
                int res = await this.Context.SaveChangesAsync();//.WithCurrentCulture<int>();
                //await cultureAwaiter;
            }
        }

        private void ThrowIfDisposed()
        {
            if (this._disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }
        }

        /// <summary>
        ///     Update an entity
        /// </summary>
        /// <param name="user"></param>
        public virtual async Task UpdateAsync(TEstatusUsuario city)
        {
            this.ThrowIfDisposed();
            if (city == null)
            {
                throw new ArgumentNullException("user");
            }
            this._EstatusUsuarioStore.Update(city);
            await this.SaveChanges().WithCurrentCulture();
        }

        public Task<TEstatusUsuario> FindByIdAsync(int Id)
        {
            throw new NotImplementedException();
        }
    }

}
