﻿using GreinerAssistec.Infraestructura.Base.Consultas;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using GreinerAssistec.Infraestructura.Modelo.Entidades;
using GreinerAssistec.Infraestructura.Base.Almacen;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Datos.Almacen
{
    public class MoldeStore<TMolde> :
    IQueryableMoldeStore<TMolde>,
    IMoldeStore<TMolde>, IDisposable
where TMolde : Molde<Guid>
        //where TKey : IEquatable<TKey>
    {
        //private readonly IDbSet<TUserLogin> _logins;

        private bool _disposed;

        private EntityStore<TMolde> _MoldeStore;

        /// <summary>
        ///     If true will call SaveChanges after Create/Update/Delete
        /// </summary>
        public bool AutoSaveChanges
        {
            get;
            set;
        }

        /// <summary>
        ///     Context for the store
        /// </summary>
        public DbContext Context
        {
            get;
            private set;
        }

        /// <summary>
        ///     If true will call dispose on the DbContext during Dispose
        /// </summary>
        public bool DisposeContext
        {
            get;
            set;
        }

        /// <summary>
        ///     Returns an IQueryable of users
        /// </summary>
        public IQueryable<TMolde> Moldes
        {
            get
            {
                return this._MoldeStore.EntitySet;
            }
        }

        /// <summary>
        ///     Constructor which takes a db context and wires up the stores with default instances using the context
        /// </summary>
        /// <param name="context"></param>
        public MoldeStore(DbContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            this.Context = context;
            this.AutoSaveChanges = true;
            this._MoldeStore = new EntityStore<TMolde>(context);
        }

        /// <summary>
        ///     Insert an entity
        /// </summary>
        /// <param name="user"></param>
        public virtual async Task CreateAsync(TMolde city)
        {
            this.ThrowIfDisposed();
            if (city == null)
            {
                throw new ArgumentNullException("user");
            }
            this._MoldeStore.Create(city);
            await this.SaveChanges().WithCurrentCulture();
        }

        /// <summary>
        ///     Mark an entity for deletion
        /// </summary>
        /// <param name="user"></param>
        public virtual async Task DeleteAsync(TMolde city)
        {
            this.ThrowIfDisposed();
            if (city == null)
            {
                throw new ArgumentNullException("city");
            }
            this._MoldeStore.Delete(city);
            await this.SaveChanges().WithCurrentCulture();
        }

        /// <summary>
        ///     Dispose the store
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     If disposing, calls dispose on the Context.  Always nulls out the Context
        /// </summary>
        /// <param name="disposing"></param>
        public virtual void Dispose(bool disposing)
        {
            if (this.DisposeContext && disposing && this.Context != null)
            {
                this.Context.Dispose();
            }
            this._disposed = true;
            this.Context = null;
            this._MoldeStore = null;
        }

        /// <summary>
        ///     Find a user by id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public virtual Task<TMolde> FindByIdAsync(Guid IdMolde)
        {
            this.ThrowIfDisposed();
            throw new NotImplementedException();
            //return this.GetUserAggregateAsync((TMolde u) => u.Id.Equals(userId));
        }
        /*public virtual Task<TMolde> FindByIdAsync(TKey IdMolde)
        {
            throw new NotImplementedException();
        }*/

        public async Task<TMolde> FindByNameAsync(string Name)
        {
            this.ThrowIfDisposed();
            if (Name == null)
            {
                throw new ArgumentNullException("Name");
            }
            var cultureAwaiter = this._MoldeStore.DbEntitySet.Where(x => x.Nombre == Name).FirstOrDefaultAsync().WithCurrentCulture();
            return await cultureAwaiter;
        }

        private async Task SaveChanges()
        {
            if (this.AutoSaveChanges)
            {
                //TaskExtensions.CultureAwaiter<int> cultureAwaiter = 
                int res = await this.Context.SaveChangesAsync();//.WithCurrentCulture<int>();
                //await cultureAwaiter;
            }
        }

        private void ThrowIfDisposed()
        {
            if (this._disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }
        }

        /// <summary>
        ///     Update an entity
        /// </summary>
        /// <param name="user"></param>
        public virtual async Task UpdateAsync(TMolde city)
        {
            this.ThrowIfDisposed();
            if (city == null)
            {
                throw new ArgumentNullException("user");
            }
            this._MoldeStore.Update(city);
            await this.SaveChanges().WithCurrentCulture();
        }

        public Task<TMolde> FindByIdAsync(int Id)
        {
            throw new NotImplementedException();
        }
    }

}
