﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Fabricas
{
    public static class OwinContextExtensions
    {
        private readonly static string IdentityKeyPrefix;

        static OwinContextExtensions()
        {
            OwinContextExtensions.IdentityKeyPrefix = "AspNet.Identity.Owin:";
        }

        /// <summary>
        ///     Retrieves an object from the OwinContext using a key based on the AssemblyQualified type name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context"></param>
        /// <returns></returns>
        public static T Get<T>(this IOwinContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            return context.Get<T>(OwinContextExtensions.GetKey(typeof(T)));
        }

        private static string GetKey(Type t)
        {
            return string.Concat(OwinContextExtensions.IdentityKeyPrefix, t.AssemblyQualifiedName);
        }

        /// <summary>
        ///     Get the user manager from the context
        /// </summary>
        /// <typeparam name="TManager"></typeparam>
        /// <param name="context"></param>
        /// <returns></returns>
        public static TManager GetDummieTableManager<TManager>(this IOwinContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            return context.Get<TManager>();
        }

        /// <summary>
        ///     Stores an object in the OwinContext using a key based on the AssemblyQualified type name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static IOwinContext Set<T>(this IOwinContext context, T value)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            return context.Set<T>(OwinContextExtensions.GetKey(typeof(T)), value);
        }
    }
}
