﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Entidades
{
    public interface IMaquina<out TKey>
    {
        TKey IdMaquina { get; }
        string NumeroActivo { get; set; }
        string Nombre { get; set; }
        int? Tonelaje { get; set; }
        int? DiametroHusillo { get; set; }
        string Comentarios { get; set; }
        int Tipo { get; set; }  // Ensamble Inyeccion Trial
        string Descripcion { get; set; }

    }
}
