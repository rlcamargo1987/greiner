﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Entidades
{
    public interface IComponente<out TKey>
    {
        TKey IdComponente { get; }
        int NPInterno { get; set; }
        string Tipo { get; set; }
        string Descripcion { get; set; }
        string Medida { get; set; }
        string Cliente { get; set; }
    }
}
