﻿using GreinerAssistec.Infraestructura.Base.Entidades.Anotaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Entidades
{
    public interface IEmpaque<out TKey>
    {
        TKey IdEmpaque { get; }
        int NPInterno { get; set; }
        string Tipo { get; set; }
        string Descripcion { get; set; }
        int? CajasPallets { get; set; }
        [Precision(20, 2)]
        decimal? KilosSoporta { get; set; }
        int? NivelesCajaTarima { get; set; }
        bool Estatus { get; set; }
    }
}
