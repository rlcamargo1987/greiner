
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Entidades.Base
{
    /// <summary>
    ///     Represents the result of an identity operation
    /// </summary>
    public class IdentityResult
    {
        private readonly static IdentityResult _success;

        /// <summary>
        ///     List of errors
        /// </summary>
        public IEnumerable<string> Errors
        {
            get;
            private set;
        }

        /// <summary>
        ///     True if the operation was successful
        /// </summary>
        public bool Succeeded
        {
            get;
            private set;
        }

        /// <summary>
        ///     Static success result
        /// </summary>
        /// <returns></returns>
        public static IdentityResult Success
        {
            get
            {
                return IdentityResult._success;
            }
        }

        static IdentityResult()
        {
            IdentityResult._success = new IdentityResult(true);
        }

        /// <summary>
        ///     Failure constructor that takes error messages
        /// </summary>
        /// <param name="errors"></param>
        public IdentityResult(params string[] errors) : this((IEnumerable<string>)errors)
        {
        }

        /// <summary>
        ///     Failure constructor that takes error messages
        /// </summary>
        /// <param name="errors"></param>
        public IdentityResult(IEnumerable<string> errors)
        {
            if (errors == null)
            {
                errors = new string[] { "" };
            }
            this.Succeeded = false;
            this.Errors = errors;
        }

        /// <summary>
        /// Constructor that takes whether the result is successful
        /// </summary>
        /// <param name="success"></param>
        protected IdentityResult(bool success)
        {
            this.Succeeded = success;
            this.Errors = new string[0];
        }

        /// <summary>
        ///     Failed helper method
        /// </summary>
        /// <param name="errors"></param>
        /// <returns></returns>
        public static IdentityResult Failed(params string[] errors)
        {
            return new IdentityResult(errors);
        }
    }

    public static class TaskExtensions
    {
        public static GreinerAssistec.Infraestructura.Base.Entidades.Base.TaskExtensions.CultureAwaiter<T> WithCurrentCulture<T>(this Task<T> task)
        {
            return new GreinerAssistec.Infraestructura.Base.Entidades.Base.TaskExtensions.CultureAwaiter<T>(task);
        }

        public static GreinerAssistec.Infraestructura.Base.Entidades.Base.TaskExtensions.CultureAwaiter WithCurrentCulture(this Task task)
        {
            return new GreinerAssistec.Infraestructura.Base.Entidades.Base.TaskExtensions.CultureAwaiter(task);
        }

        public struct CultureAwaiter : ICriticalNotifyCompletion, INotifyCompletion
        {
            private readonly Task _task;

            public bool IsCompleted
            {
                get
                {
                    return this._task.IsCompleted;
                }
            }

            public CultureAwaiter(Task task)
            {
                this._task = task;
            }

            public GreinerAssistec.Infraestructura.Base.Entidades.Base.TaskExtensions.CultureAwaiter GetAwaiter()
            {
                return this;
            }

            public void GetResult()
            {
                this._task.GetAwaiter().GetResult();
            }

            public void OnCompleted(Action continuation)
            {
                throw new NotImplementedException();
            }

            public void UnsafeOnCompleted(Action continuation)
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                CultureInfo cultureInfo1 = Thread.CurrentThread.CurrentUICulture;
                ConfiguredTaskAwaitable.ConfiguredTaskAwaiter awaiter = this._task.ConfigureAwait(false).GetAwaiter();
                awaiter.UnsafeOnCompleted(() =>
                {
                    CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                    CultureInfo currentUICulture = Thread.CurrentThread.CurrentUICulture;
                    Thread.CurrentThread.CurrentCulture = cultureInfo;
                    Thread.CurrentThread.CurrentUICulture = cultureInfo1;
                    try
                    {
                        continuation();
                    }
                    finally
                    {
                        Thread.CurrentThread.CurrentCulture = currentCulture;
                        Thread.CurrentThread.CurrentUICulture = currentUICulture;
                    }
                });
            }
        }

        public struct CultureAwaiter<T> : ICriticalNotifyCompletion, INotifyCompletion
        {
            private readonly Task<T> _task;

            public bool IsCompleted
            {
                get
                {
                    return this._task.IsCompleted;
                }
            }

            public CultureAwaiter(Task<T> task)
            {
                this._task = task;
            }

            public GreinerAssistec.Infraestructura.Base.Entidades.Base.TaskExtensions.CultureAwaiter<T> GetAwaiter()
            {
                return this;
            }

            public T GetResult()
            {
                return this._task.GetAwaiter().GetResult();
            }

            public void OnCompleted(Action continuation)
            {
                throw new NotImplementedException();
            }

            public void UnsafeOnCompleted(Action continuation)
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                CultureInfo cultureInfo1 = Thread.CurrentThread.CurrentUICulture;
                ConfiguredTaskAwaitable<T>.ConfiguredTaskAwaiter awaiter = this._task.ConfigureAwait(false).GetAwaiter();
                awaiter.UnsafeOnCompleted(() =>
                {
                    CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                    CultureInfo currentUICulture = Thread.CurrentThread.CurrentUICulture;
                    Thread.CurrentThread.CurrentCulture = cultureInfo;
                    Thread.CurrentThread.CurrentUICulture = cultureInfo1;
                    try
                    {
                        continuation();
                    }
                    finally
                    {
                        Thread.CurrentThread.CurrentCulture = currentCulture;
                        Thread.CurrentThread.CurrentUICulture = currentUICulture;
                    }
                });
            }
        }
    }
}