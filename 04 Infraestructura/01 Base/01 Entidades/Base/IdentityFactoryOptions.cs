﻿using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Entidades.Base
{
    /// <summary>
    ///     Interface used to create objects per request
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IIdentityFactoryProvider<T>
    where T : IDisposable
    {
        /// <summary>
        ///     Called once per request to create an object
        /// </summary>
        /// <param name="options"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        T Create(IdentityFactoryOptions<T> options, IOwinContext context);

        /// <summary>
        ///     Called at the end of the request to dispose the object created
        /// </summary>
        /// <param name="options"></param>
        /// <param name="instance"></param>
        void Dispose(IdentityFactoryOptions<T> options, T instance);
    }

    /// <summary>
    ///     Configuration options for a IdentityFactoryMiddleware
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class IdentityFactoryOptions<T>
    where T : IDisposable
    {
        /// <summary>
        ///     Used to configure the data protection provider
        /// </summary>
        public IDataProtectionProvider DataProtectionProvider
        {
            get;
            set;
        }

        /// <summary>
        ///     Provider used to Create and Dispose objects
        /// </summary>
        public IIdentityFactoryProvider<T> Provider
        {
            get;
            set;
        }

        public IdentityFactoryOptions()
        {
        }
    }
}
