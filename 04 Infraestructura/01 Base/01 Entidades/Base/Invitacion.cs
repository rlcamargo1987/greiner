﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Entidades.Base
{
    public class Invitacion
    {
        public Guid IdUsuario { get; set; }
        public string Direccion { get; set; }
        public Guid IdUsuarioQueCancelo { get; set; }
        public int IdPedido { get; set; }
    }
}
