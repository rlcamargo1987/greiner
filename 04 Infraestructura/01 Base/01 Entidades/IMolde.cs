﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Entidades
{
    public interface IMolde<out TKey>
    {
        TKey IdMolde { get; }
        string Codigo { get; set; }
        string Nombre { get; set; }
        int TonelajeCotizado { get; set; }
        int TonelajeReal { get; set; }
        string Cliente { get; set; }
        string Descripcion { get; set; }
        bool Activo { get; set; }
    }

}
