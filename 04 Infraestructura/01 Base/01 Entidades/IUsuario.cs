﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Entidades
{
    public interface IUsuario<out TKey>
    {
        TKey IdUsuario { get; }
        string Foto { get; set; }
        string Correo { get; set; }
        string Contrasena { get; set; }
        string Nombre { get; set; }
        string ApellidoPaterno { get; set; }
        string ApellidoMaterno { get; set; }
        bool Genero { get; set; } //0 Hombre, 1 mujer
        string TelefonoMovil { get; set; }
        DateTime? FechaNacimiento { get; set; }
        string Area { get; set; }
        string Departamento { get; set; }
        string Puesto { get; set; }
        string PerfilUsuario { get; set; }
        DateTime? FechaIngreso { get; set; }
        Guid Estatus { get; set; }
    }

}
