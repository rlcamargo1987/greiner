﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Entidades
{
    public interface IMateriaPrima<out TKey>
    {
        TKey IdMateriaPrima { get; }
        int NumeroParteInterno { get; set; }
        int Tipo { get; set; }      //Pigmento, Resina, Aditivo, Inserto, Etiquetas 
        string Descripcion { get; set; }
        bool Secado { get; set; }    //no-si
        decimal? Temperatura { get; set; }
        string Tiempo { get; set; }
        string Familia { get; set; }
        decimal? PorcentajeHumedad { get; set; }
        bool AmorfoSemi { get; set; }                //Amorfo / Semicristalino
        string Polimero { get; set; }
    }
}
