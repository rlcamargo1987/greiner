﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Entidades
{
    public interface IEstatusUsuario<out TKey>
    {
        TKey IdEstatusUsuario { get; }
        string Nombre { get; set; }
        bool Activo { get; set; }
    }

}
