﻿using GreinerAssistec.Infraestructura.Base.Entidades.Anotaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Entidades
{
    public interface IInserto<out TKey>
    {
        TKey IdInserto { get; }
        string Codigo { get; set; }
        [Precision(25, 5)]
        decimal Peso { get; set; }
        int Cavidades { get; set; }
        int CavidadesFuncionales { get; set; }
        [Precision(25, 5)]
        decimal Colada { get; set; }
        bool ColadaCotizada { get; set; }
        int Ciclo { get; set; }
        int CicloReal { get; set; }
        [Precision(10, 5)]
        decimal Maqhrs { get; set; }
        string Instruccion { get; set; }
    }
}
