﻿using GreinerAssistec.Infraestructura.Base.Almacen;
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Consultas
{
    public interface IQueryableUsuarioStore<TUsuario, in TKey> :
                                        IUsuarioStore<TUsuario, TKey>, IDisposable
                                                            where TUsuario : class, IUsuario<TKey>
    {
        IQueryable<TUsuario> Usuarios
        {
            get;
        }
    }

    public interface IQueryableUsuarioStore<TUsuario>
                            : IQueryableUsuarioStore<TUsuario, Guid>, IUsuarioStore<TUsuario, Guid>, IDisposable
                                                                        where TUsuario : class, IUsuario<Guid>
    {

    }

}
