﻿using GreinerAssistec.Infraestructura.Base.Almacen;
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Consultas
{
    public interface IQueryableComponenteStore<TComponente, in TKey> :
                                        IComponenteStore<TComponente, TKey>, IDisposable
                                                            where TComponente : class, IComponente<TKey>
    {
        IQueryable<TComponente> Componentes
        {
            get;
        }
    }

    public interface IQueryableComponenteStore<TComponente>
                            : IQueryableComponenteStore<TComponente, Guid>, IComponenteStore<TComponente, Guid>, IDisposable
                                                                        where TComponente : class, IComponente<Guid>
    {

    }

}
