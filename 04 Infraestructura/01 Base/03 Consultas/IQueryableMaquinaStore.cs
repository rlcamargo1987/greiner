﻿using GreinerAssistec.Infraestructura.Base.Almacen;
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Consultas
{
    public interface IQueryableMaquinaStore<TMaquina, in TKey> :
                                        IMaquinaStore<TMaquina, TKey>, IDisposable
                                                            where TMaquina : class, IMaquina<TKey>
    {
        IQueryable<TMaquina> Maquinas
        {
            get;
        }
    }

    public interface IQueryableMaquinaStore<TMaquina>
                            : IQueryableMaquinaStore<TMaquina, Guid>, IMaquinaStore<TMaquina, Guid>, IDisposable
                                                                        where TMaquina : class, IMaquina<Guid>
    {

    }

}
