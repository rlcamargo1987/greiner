﻿using GreinerAssistec.Infraestructura.Base.Almacen;
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Consultas
{
    public interface IQueryableMoldeStore<TMolde, in TKey> :
                                        IMoldeStore<TMolde, TKey>, IDisposable
                                                            where TMolde : class, IMolde<TKey>
    {
        IQueryable<TMolde> Moldes
        {
            get;
        }
    }

    public interface IQueryableMoldeStore<TMolde>
                            : IQueryableMoldeStore<TMolde, Guid>, IMoldeStore<TMolde, Guid>, IDisposable
                                                                        where TMolde : class, IMolde<Guid>
    {

    }

}
