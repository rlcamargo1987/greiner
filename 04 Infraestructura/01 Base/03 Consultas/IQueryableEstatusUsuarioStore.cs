﻿using GreinerAssistec.Infraestructura.Base.Almacen;
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Consultas
{
    public interface IQueryableEstatusUsuarioStore<TEstatusUsuario, in TKey> :
                                        IEstatusUsuarioStore<TEstatusUsuario, TKey>, IDisposable
                                                            where TEstatusUsuario : class, IEstatusUsuario<TKey>
    {
        IQueryable<TEstatusUsuario> EstatusUsuarios
        {
            get;
        }
    }

    public interface IQueryableEstatusUsuarioStore<TEstatusUsuario>
                            : IQueryableEstatusUsuarioStore<TEstatusUsuario, Guid>, IEstatusUsuarioStore<TEstatusUsuario, Guid>, IDisposable
                                                                        where TEstatusUsuario : class, IEstatusUsuario<Guid>
    {

    }

}
