﻿using GreinerAssistec.Infraestructura.Base.Almacen;
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Consultas
{
    public interface IQueryableMateriaPrimaStore<TMateriaPrima, in TKey> :
                                        IMateriaPrimaStore<TMateriaPrima, TKey>, IDisposable
                                                            where TMateriaPrima : class, IMateriaPrima<TKey>
    {
        IQueryable<TMateriaPrima> MateriaPrimas
        {
            get;
        }
    }

    public interface IQueryableMateriaPrimaStore<TMateriaPrima>
                            : IQueryableMateriaPrimaStore<TMateriaPrima, Guid>, IMateriaPrimaStore<TMateriaPrima, Guid>, IDisposable
                                                                        where TMateriaPrima : class, IMateriaPrima<Guid>
    {

    }

}
