﻿using GreinerAssistec.Infraestructura.Base.Almacen;
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Consultas
{
    public interface IQueryableEmpaqueStore<TEmpaque, in TKey> :
                                        IEmpaqueStore<TEmpaque, TKey>, IDisposable
                                                            where TEmpaque : class, IEmpaque<TKey>
    {
        IQueryable<TEmpaque> Empaques
        {
            get;
        }
    }

    public interface IQueryableEmpaqueStore<TEmpaque>
                            : IQueryableEmpaqueStore<TEmpaque, Guid>, IEmpaqueStore<TEmpaque, Guid>, IDisposable
                                                                        where TEmpaque : class, IEmpaque<Guid>
    {

    }

}
