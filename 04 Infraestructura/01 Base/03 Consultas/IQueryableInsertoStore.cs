﻿using GreinerAssistec.Infraestructura.Base.Almacen;
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Consultas
{
    public interface IQueryableInsertoStore<TInserto, in TKey> :
                                        IInsertoStore<TInserto, TKey>, IDisposable
                                                            where TInserto : class, IInserto<TKey>
    {
        IQueryable<TInserto> Insertos
        {
            get;
        }
    }

    public interface IQueryableInsertoStore<TInserto>
                            : IQueryableInsertoStore<TInserto, Guid>, IInsertoStore<TInserto, Guid>, IDisposable
                                                                        where TInserto : class, IInserto<Guid>
    {

    }

}
