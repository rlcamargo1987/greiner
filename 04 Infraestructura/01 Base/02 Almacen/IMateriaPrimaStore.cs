﻿using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Almacen
{
    public interface IMateriaPrimaStore<TMateriaPrima, in TKey> : IDisposable
                                      where TMateriaPrima : class, IMateriaPrima<TKey>
    {
        Task CreateAsync(TMateriaPrima entity);
        Task DeleteAsync(TMateriaPrima entity);
        Task<TMateriaPrima> FindByIdAsync(TKey Id);
        Task UpdateAsync(TMateriaPrima entity);
    }

    public interface IMateriaPrimaStore<TMateriaPrima>
                            : IMateriaPrimaStore<TMateriaPrima, Guid>, IDisposable
                                                        where TMateriaPrima : class, IMateriaPrima<Guid>
    {

    }
}
