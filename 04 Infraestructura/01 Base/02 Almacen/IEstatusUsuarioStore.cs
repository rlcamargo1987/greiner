﻿using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Almacen
{
    public interface IEstatusUsuarioStore<TEstatusUsuario, in TKey> : IDisposable
                                      where TEstatusUsuario : class, IEstatusUsuario<TKey>
    {
        Task CreateAsync(TEstatusUsuario entity);
        Task DeleteAsync(TEstatusUsuario entity);
        Task<TEstatusUsuario> FindByIdAsync(TKey Id);
        Task UpdateAsync(TEstatusUsuario entity);
    }

    public interface IEstatusUsuarioStore<TEstatusUsuario>
                            : IEstatusUsuarioStore<TEstatusUsuario, Guid>, IDisposable
                                                        where TEstatusUsuario : class, IEstatusUsuario<Guid>
    {

    }
}
