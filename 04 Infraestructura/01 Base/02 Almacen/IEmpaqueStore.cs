﻿
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Almacen
{

    public interface IEmpaqueStore<TEmpaque, in TKey> : IDisposable
                                      where TEmpaque : class, IEmpaque<TKey>
    {
        Task CreateAsync(TEmpaque entity);
        Task DeleteAsync(TEmpaque entity);
        Task<TEmpaque> FindByIdAsync(TKey Id);
        Task UpdateAsync(TEmpaque entity);
    }

    public interface IEmpaqueStore<TEmpaque>
                            : IEmpaqueStore<TEmpaque, Guid>, IDisposable
                                                        where TEmpaque : class, IEmpaque<Guid>
    {

    }
}
