﻿using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Almacen
{
    public interface IUsuarioStore<TUsuario, in TKey> : IDisposable
                                      where TUsuario : class, IUsuario<TKey>
    {
        Task CreateAsync(TUsuario entity);
        Task DeleteAsync(TUsuario entity);
        Task<TUsuario> FindByIdAsync(TKey Id);
        Task UpdateAsync(TUsuario entity);
    }

    public interface IUsuarioStore<TUsuario>
                            : IUsuarioStore<TUsuario, Guid>, IDisposable
                                                        where TUsuario : class, IUsuario<Guid>
    {

    }
}
