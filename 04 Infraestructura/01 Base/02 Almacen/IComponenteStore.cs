﻿using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Almacen
{
    public interface IComponenteStore<TComponente, in TKey> : IDisposable
                                      where TComponente : class, IComponente<TKey>
    {
        Task CreateAsync(TComponente entity);
        Task DeleteAsync(TComponente entity);
        Task<TComponente> FindByIdAsync(TKey Id);
        Task UpdateAsync(TComponente entity);
    }

    public interface IComponenteStore<TComponente>
                            : IComponenteStore<TComponente, Guid>, IDisposable
                                                        where TComponente : class, IComponente<Guid>
    {

    }
}
