﻿
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Almacen
{
    public interface IInsertoStore<TInserto, in TKey> : IDisposable
                                      where TInserto : class, IInserto<TKey>
    {
        Task CreateAsync(TInserto entity);
        Task DeleteAsync(TInserto entity);
        Task<TInserto> FindByIdAsync(TKey Id);
        Task UpdateAsync(TInserto entity);
    }

    public interface IInsertoStore<TInserto>
                            : IInsertoStore<TInserto, Guid>, IDisposable
                                                        where TInserto : class, IInserto<Guid>
    {

    }
}
