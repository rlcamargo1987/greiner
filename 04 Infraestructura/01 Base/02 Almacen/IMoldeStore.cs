﻿
using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Almacen
{
    public interface IMoldeStore<TMolde, in TKey> : IDisposable
                                      where TMolde : class, IMolde<TKey>
    {
        Task CreateAsync(TMolde entity);
        Task DeleteAsync(TMolde entity);
        Task<TMolde> FindByIdAsync(TKey Id);
        Task UpdateAsync(TMolde entity);
    }

    public interface IMoldeStore<TMolde>
                            : IMoldeStore<TMolde, Guid>, IDisposable
                                                        where TMolde : class, IMolde<Guid>
    {

    }
}
