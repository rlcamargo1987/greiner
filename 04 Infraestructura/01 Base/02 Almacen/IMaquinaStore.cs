﻿using GreinerAssistec.Infraestructura.Base.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Almacen
{
    public interface IMaquinaStore<TMaquina, in TKey> : IDisposable
                                      where TMaquina : class, IMaquina<TKey>
    {
        Task CreateAsync(TMaquina entity);
        Task DeleteAsync(TMaquina entity);
        Task<TMaquina> FindByIdAsync(TKey Id);
        Task UpdateAsync(TMaquina entity);
    }

    public interface IMaquinaStore<TMaquina>
                            : IMaquinaStore<TMaquina, Guid>, IDisposable
                                                        where TMaquina : class, IMaquina<Guid>
    {

    }
}
