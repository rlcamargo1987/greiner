﻿using GreinerAssistec.Infraestructura.Base.Almacen;
using GreinerAssistec.Infraestructura.Base.Consultas;
using GreinerAssistec.Infraestructura.Base.Entidades;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Administradores
{
    public class InsertoManager<TInserto, TKey> : IDisposable
where TInserto : class, IInserto<TKey>
where TKey : IEquatable<TKey>
    {
        private bool _disposed;

        protected internal IInsertoStore<TInserto, TKey> Store
        {
            get;
            set;
        }

        /// <summary>
        ///     Returns an IQueryable of users if the store is an IQueryableUserStore
        /// </summary>
        public virtual IQueryable<TInserto> Insertos
        {
            get
            {
                IQueryableInsertoStore<TInserto, TKey> store = this.Store as IQueryableInsertoStore<TInserto, TKey>;
                if (store == null)
                {
                    throw new NotSupportedException("Resources.StoreNotIQueryableInsertoStore");
                }
                return store.Insertos;
            }
        }


        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="store">The IUserStore is responsible for commiting changes via the UpdateAsync/CreateAsync methods</param>
        public InsertoManager(IInsertoStore<TInserto, TKey> store)
        {
            if (store == null)
            {
                throw new ArgumentNullException("store");
            }
            this.Store = store;
        }

        /// <summary>
        ///     Create a user with no password
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> CreateAsync(TInserto Inserto)
        {
            IdentityResult success;
            this.ThrowIfDisposed();
            await this.Store.CreateAsync(Inserto).WithCurrentCulture();
            success = IdentityResult.Success;
            return success;
        }

        /// <summary>
        ///     Delete a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> DeleteAsync(TInserto user)
        {
            this.ThrowIfDisposed();
            await this.Store.DeleteAsync(user).WithCurrentCulture();
            return IdentityResult.Success;
        }

        /// <summary>
        ///     Dispose this object
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     When disposing, actually dipose the store
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !this._disposed)
            {
                this.Store.Dispose();
                this._disposed = true;
            }
        }

        /// <summary>
        ///     Return a user with the specified username and password or null if there is no match.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        /*public virtual async Task<TInserto> FindAsync(string IdInserto, string Nombre)
        {
            this.ThrowIfDisposed();
            OxxoGas.Infraestructura.Base.Entidades.Base.TaskExtensions.CultureAwaiter<TInserto> cultureAwaiter = this.FindByNameAsync(Nombre).WithCurrentCulture<TInserto>();
            TInserto tInserto1 = await cultureAwaiter;

            if (tInserto1 == null)
            {
                tInserto1 = default(TInserto);
            }
            return tInserto1;
        }*/

        /// <summary>
        ///     Find a user by id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public virtual Task<TInserto> FindByIdAsync(TKey IdInserto)
        {
            this.ThrowIfDisposed();
            return this.Store.FindByIdAsync(IdInserto);
        }

        /// <summary>
        ///     Find a user by user name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        /*public virtual Task<TInserto> FindByNameAsync(string Name)
        {
            this.ThrowIfDisposed();
            if (Name == null)
            {
                throw new ArgumentNullException("Name");
            }
            return this.Store. FindByNameAsync(Name);
        }*/

        private IInsertoStore<TInserto, TKey> GetInsertoStore()
        {
            IInsertoStore<TInserto, TKey> store = this.Store as IInsertoStore<TInserto, TKey>;
            if (store == null)
            {
                throw new NotSupportedException("Resources.StoreNotIUserLoginStore");
            }
            return store;
        }

        /// <summary>
        ///     Update a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> UpdateAsync(TInserto dummieTable)
        {
            IdentityResult success;
            this.ThrowIfDisposed();
            if (dummieTable == null)
            {
                throw new ArgumentNullException("user");
            }
            await this.Store.UpdateAsync(dummieTable).WithCurrentCulture();
            success = IdentityResult.Success;
            return success;
        }

        private void ThrowIfDisposed()
        {
            if (this._disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }
        }
    }


}
