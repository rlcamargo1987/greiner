﻿using GreinerAssistec.Infraestructura.Base.Almacen;
using GreinerAssistec.Infraestructura.Base.Consultas;
using GreinerAssistec.Infraestructura.Base.Entidades;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Administradores
{
    public class MoldeManager<TMolde, TKey> : IDisposable
where TMolde : class, IMolde<TKey>
where TKey : IEquatable<TKey>
    {
        private bool _disposed;

        protected internal IMoldeStore<TMolde, TKey> Store
        {
            get;
            set;
        }

        /// <summary>
        ///     Returns an IQueryable of users if the store is an IQueryableUserStore
        /// </summary>
        public virtual IQueryable<TMolde> Moldes
        {
            get
            {
                IQueryableMoldeStore<TMolde, TKey> store = this.Store as IQueryableMoldeStore<TMolde, TKey>;
                if (store == null)
                {
                    throw new NotSupportedException("Resources.StoreNotIQueryableMoldeStore");
                }
                return store.Moldes;
            }
        }


        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="store">The IUserStore is responsible for commiting changes via the UpdateAsync/CreateAsync methods</param>
        public MoldeManager(IMoldeStore<TMolde, TKey> store)
        {
            if (store == null)
            {
                throw new ArgumentNullException("store");
            }
            this.Store = store;
        }

        /// <summary>
        ///     Create a user with no password
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> CreateAsync(TMolde Molde)
        {
            IdentityResult success;
            this.ThrowIfDisposed();
            await this.Store.CreateAsync(Molde).WithCurrentCulture();
            success = IdentityResult.Success;
            return success;
        }

        /// <summary>
        ///     Create a user with the given password
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> CreateAsync(TMolde dummieTable, string Name)
        {
            IdentityResult identityResult;
            this.ThrowIfDisposed();
            IMoldeStore<TMolde, TKey> store = this.GetMoldeStore();
            if (dummieTable == null)
            {
                throw new ArgumentNullException("Molde");
            }
            if (string.IsNullOrEmpty(Name))
            {
                throw new ArgumentNullException("password");
            }
            Entidades.Base.TaskExtensions.CultureAwaiter<IdentityResult> cultureAwaiter = this.UpdateName(store, dummieTable, Name).WithCurrentCulture<IdentityResult>();
            IdentityResult identityResult1 = await cultureAwaiter;
            Entidades.Base.TaskExtensions.CultureAwaiter<IdentityResult> cultureAwaiter1 = this.CreateAsync(dummieTable).WithCurrentCulture<IdentityResult>();
            identityResult = await cultureAwaiter1;
            return identityResult;
        }

        protected virtual async Task<IdentityResult> UpdateName(IMoldeStore<TMolde, TKey> store, TMolde dummieTable, string newName)
        {
            IdentityResult success;
            List<string> errors = new List<string>();
            dummieTable.Nombre = newName;
            Entidades.Base.TaskExtensions.CultureAwaiter cultureAwaiter1 = store.UpdateAsync(dummieTable).WithCurrentCulture();
            await cultureAwaiter1;
            success = IdentityResult.Success;
            return success;
        }

        /// <summary>
        ///     Delete a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> DeleteAsync(TMolde user)
        {
            this.ThrowIfDisposed();
            await this.Store.DeleteAsync(user).WithCurrentCulture();
            return IdentityResult.Success;
        }

        /// <summary>
        ///     Dispose this object
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     When disposing, actually dipose the store
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !this._disposed)
            {
                this.Store.Dispose();
                this._disposed = true;
            }
        }

        /// <summary>
        ///     Return a user with the specified username and password or null if there is no match.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        /*public virtual async Task<TMolde> FindAsync(string IdMolde, string Nombre)
        {
            this.ThrowIfDisposed();
            OxxoGas.Infraestructura.Base.Entidades.Base.TaskExtensions.CultureAwaiter<TMolde> cultureAwaiter = this.FindByNameAsync(Nombre).WithCurrentCulture<TMolde>();
            TMolde tMolde1 = await cultureAwaiter;

            if (tMolde1 == null)
            {
                tMolde1 = default(TMolde);
            }
            return tMolde1;
        }*/

        /// <summary>
        ///     Find a user by id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public virtual Task<TMolde> FindByIdAsync(TKey IdMolde)
        {
            this.ThrowIfDisposed();
            return this.Store.FindByIdAsync(IdMolde);
        }

        /// <summary>
        ///     Find a user by user name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        /*public virtual Task<TMolde> FindByNameAsync(string Name)
        {
            this.ThrowIfDisposed();
            if (Name == null)
            {
                throw new ArgumentNullException("Name");
            }
            return this.Store. FindByNameAsync(Name);
        }*/

        private IMoldeStore<TMolde, TKey> GetMoldeStore()
        {
            IMoldeStore<TMolde, TKey> store = this.Store as IMoldeStore<TMolde, TKey>;
            if (store == null)
            {
                throw new NotSupportedException("Resources.StoreNotIUserLoginStore");
            }
            return store;
        }

        /// <summary>
        ///     Update a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> UpdateAsync(TMolde dummieTable)
        {
            IdentityResult success;
            this.ThrowIfDisposed();
            if (dummieTable == null)
            {
                throw new ArgumentNullException("user");
            }
            await this.Store.UpdateAsync(dummieTable).WithCurrentCulture();
            success = IdentityResult.Success;
            return success;
        }

        private void ThrowIfDisposed()
        {
            if (this._disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }
        }
    }


}
