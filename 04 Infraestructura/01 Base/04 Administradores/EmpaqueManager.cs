﻿using GreinerAssistec.Infraestructura.Base.Almacen;
using GreinerAssistec.Infraestructura.Base.Consultas;
using GreinerAssistec.Infraestructura.Base.Entidades;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Administradores
{
    public class EmpaqueManager<TEmpaque, TKey> : IDisposable
where TEmpaque : class, IEmpaque<TKey>
where TKey : IEquatable<TKey>
    {
        private bool _disposed;

        protected internal IEmpaqueStore<TEmpaque, TKey> Store
        {
            get;
            set;
        }

        /// <summary>
        ///     Returns an IQueryable of users if the store is an IQueryableUserStore
        /// </summary>
        public virtual IQueryable<TEmpaque> Empaques
        {
            get
            {
                IQueryableEmpaqueStore<TEmpaque, TKey> store = this.Store as IQueryableEmpaqueStore<TEmpaque, TKey>;
                if (store == null)
                {
                    throw new NotSupportedException("Resources.StoreNotIQueryableEmpaqueStore");
                }
                return store.Empaques;
            }
        }


        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="store">The IUserStore is responsible for commiting changes via the UpdateAsync/CreateAsync methods</param>
        public EmpaqueManager(IEmpaqueStore<TEmpaque, TKey> store)
        {
            if (store == null)
            {
                throw new ArgumentNullException("store");
            }
            this.Store = store;
        }

        /// <summary>
        ///     Create a user with no password
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> CreateAsync(TEmpaque Empaque)
        {
            IdentityResult success;
            this.ThrowIfDisposed();
            await this.Store.CreateAsync(Empaque).WithCurrentCulture();
            success = IdentityResult.Success;
            return success;
        }

        /// <summary>
        ///     Delete a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> DeleteAsync(TEmpaque user)
        {
            this.ThrowIfDisposed();
            await this.Store.DeleteAsync(user).WithCurrentCulture();
            return IdentityResult.Success;
        }

        /// <summary>
        ///     Dispose this object
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     When disposing, actually dipose the store
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !this._disposed)
            {
                this.Store.Dispose();
                this._disposed = true;
            }
        }

        /// <summary>
        ///     Return a user with the specified username and password or null if there is no match.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        /*public virtual async Task<TEmpaque> FindAsync(string IdEmpaque, string Nombre)
        {
            this.ThrowIfDisposed();
            OxxoGas.Infraestructura.Base.Entidades.Base.TaskExtensions.CultureAwaiter<TEmpaque> cultureAwaiter = this.FindByNameAsync(Nombre).WithCurrentCulture<TEmpaque>();
            TEmpaque tEmpaque1 = await cultureAwaiter;

            if (tEmpaque1 == null)
            {
                tEmpaque1 = default(TEmpaque);
            }
            return tEmpaque1;
        }*/

        /// <summary>
        ///     Find a user by id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public virtual Task<TEmpaque> FindByIdAsync(TKey IdEmpaque)
        {
            this.ThrowIfDisposed();
            return this.Store.FindByIdAsync(IdEmpaque);
        }

        /// <summary>
        ///     Find a user by user name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        /*public virtual Task<TEmpaque> FindByNameAsync(string Name)
        {
            this.ThrowIfDisposed();
            if (Name == null)
            {
                throw new ArgumentNullException("Name");
            }
            return this.Store. FindByNameAsync(Name);
        }*/

        private IEmpaqueStore<TEmpaque, TKey> GetEmpaqueStore()
        {
            IEmpaqueStore<TEmpaque, TKey> store = this.Store as IEmpaqueStore<TEmpaque, TKey>;
            if (store == null)
            {
                throw new NotSupportedException("Resources.StoreNotIUserLoginStore");
            }
            return store;
        }

        /// <summary>
        ///     Update a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> UpdateAsync(TEmpaque dummieTable)
        {
            IdentityResult success;
            this.ThrowIfDisposed();
            if (dummieTable == null)
            {
                throw new ArgumentNullException("user");
            }
            await this.Store.UpdateAsync(dummieTable).WithCurrentCulture();
            success = IdentityResult.Success;
            return success;
        }

        private void ThrowIfDisposed()
        {
            if (this._disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }
        }
    }


}
