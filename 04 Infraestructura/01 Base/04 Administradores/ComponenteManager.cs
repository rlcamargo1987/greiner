﻿using GreinerAssistec.Infraestructura.Base.Almacen;
using GreinerAssistec.Infraestructura.Base.Consultas;
using GreinerAssistec.Infraestructura.Base.Entidades;
using GreinerAssistec.Infraestructura.Base.Entidades.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreinerAssistec.Infraestructura.Base.Administradores
{
    public class ComponenteManager<TComponente, TKey> : IDisposable
where TComponente : class, IComponente<TKey>
where TKey : IEquatable<TKey>
    {
        private bool _disposed;

        protected internal IComponenteStore<TComponente, TKey> Store
        {
            get;
            set;
        }

        /// <summary>
        ///     Returns an IQueryable of users if the store is an IQueryableUserStore
        /// </summary>
        public virtual IQueryable<TComponente> Componentes
        {
            get
            {
                IQueryableComponenteStore<TComponente, TKey> store = this.Store as IQueryableComponenteStore<TComponente, TKey>;
                if (store == null)
                {
                    throw new NotSupportedException("Resources.StoreNotIQueryableComponenteStore");
                }
                return store.Componentes;
            }
        }


        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="store">The IUserStore is responsible for commiting changes via the UpdateAsync/CreateAsync methods</param>
        public ComponenteManager(IComponenteStore<TComponente, TKey> store)
        {
            if (store == null)
            {
                throw new ArgumentNullException("store");
            }
            this.Store = store;
        }

        /// <summary>
        ///     Create a user with no password
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> CreateAsync(TComponente Componente)
        {
            IdentityResult success;
            this.ThrowIfDisposed();
            await this.Store.CreateAsync(Componente).WithCurrentCulture();
            success = IdentityResult.Success;
            return success;
        }

        /// <summary>
        ///     Delete a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> DeleteAsync(TComponente user)
        {
            this.ThrowIfDisposed();
            await this.Store.DeleteAsync(user).WithCurrentCulture();
            return IdentityResult.Success;
        }

        /// <summary>
        ///     Dispose this object
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     When disposing, actually dipose the store
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !this._disposed)
            {
                this.Store.Dispose();
                this._disposed = true;
            }
        }

        /// <summary>
        ///     Return a user with the specified username and password or null if there is no match.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        /*public virtual async Task<TComponente> FindAsync(string IdComponente, string Nombre)
        {
            this.ThrowIfDisposed();
            OxxoGas.Infraestructura.Base.Entidades.Base.TaskExtensions.CultureAwaiter<TComponente> cultureAwaiter = this.FindByNameAsync(Nombre).WithCurrentCulture<TComponente>();
            TComponente tComponente1 = await cultureAwaiter;

            if (tComponente1 == null)
            {
                tComponente1 = default(TComponente);
            }
            return tComponente1;
        }*/

        /// <summary>
        ///     Find a user by id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public virtual Task<TComponente> FindByIdAsync(TKey IdComponente)
        {
            this.ThrowIfDisposed();
            return this.Store.FindByIdAsync(IdComponente);
        }

        /// <summary>
        ///     Find a user by user name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        /*public virtual Task<TComponente> FindByNameAsync(string Name)
        {
            this.ThrowIfDisposed();
            if (Name == null)
            {
                throw new ArgumentNullException("Name");
            }
            return this.Store. FindByNameAsync(Name);
        }*/

        private IComponenteStore<TComponente, TKey> GetComponenteStore()
        {
            IComponenteStore<TComponente, TKey> store = this.Store as IComponenteStore<TComponente, TKey>;
            if (store == null)
            {
                throw new NotSupportedException("Resources.StoreNotIUserLoginStore");
            }
            return store;
        }

        /// <summary>
        ///     Update a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> UpdateAsync(TComponente dummieTable)
        {
            IdentityResult success;
            this.ThrowIfDisposed();
            if (dummieTable == null)
            {
                throw new ArgumentNullException("user");
            }
            await this.Store.UpdateAsync(dummieTable).WithCurrentCulture();
            success = IdentityResult.Success;
            return success;
        }

        private void ThrowIfDisposed()
        {
            if (this._disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }
        }
    }


}
