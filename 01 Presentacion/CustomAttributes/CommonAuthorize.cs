﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Net;

namespace GreinerAssistec.CustomAttributes
{
    //[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class CommonAuthorize : AuthorizeAttribute
    {
        public CommonAuthorize()//string Permiso
        {

        }
        // Custom property
        public string Option { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool resp = true;
            if (AuthorizeHelper.SesionVigente(Option))
              return true;
            else
                 return false;
            return resp;
        }

        protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var request = httpContext.Request;
            var response = httpContext.Response;
            var user = httpContext.User;

            if (request.IsAjaxRequest())
            {
                if (user.Identity.IsAuthenticated == false)
                    response.StatusCode = (int)HttpStatusCode.Unauthorized;
                else
                    response.StatusCode = (int)HttpStatusCode.Forbidden;

                response.SuppressFormsAuthenticationRedirect = true;
                response.End();
            }

            base.HandleUnauthorizedRequest(filterContext);

            if (HttpContext.Current.User.Identity.IsAuthenticated && ((System.Web.Mvc.HttpUnauthorizedResult)filterContext.Result).StatusCode == (int)HttpStatusCode.Unauthorized)
                filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary(
                                new
                                {
                                    controller = "Login",
                                    action = "NotAuthorized",
                                    Area = "Security"
                                })
                            );
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary(
                                new
                                {
                                    controller = "Acceso",
                                    action = "Inicio",
                                    Area = "Seguridad"
                                })
                            );
        }
    }
}