﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Security.Claims;
using Microsoft.Owin.Security;
using System.Configuration;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Dominio.Modelo.Administradores.Base;
using GreinerAssistec.Dominio.Modelo.Entidades;

namespace GreinerAssistec.CustomAttributes
{
    public static class AuthorizeHelper
    {

        public static void CreaSesion(CredentialsDTO credentials, Microsoft.Owin.IOwinContext ctx)
        {
            var manager = Dominio.Modelo.Administradores.AppUsuarioManager.Create();
            var decrypt = new Crypt();
            var encriptPass = decrypt.Encrypting(credentials.Password);
            var user = manager.Usuarios.Where(x => x.Correo.ToLower().Equals(credentials.Email.ToLower()) &&
                                              x.Contrasena.ToLower().Equals(encriptPass.ToLower()))
                                        .FirstOrDefault();

            ctx.Authentication.SignOut(new string[] { "ExternalCookie", "TwoFactorCookie" });

            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, user.IdUsuario.ToString()),
                new Claim(ClaimTypes.Email,user.Correo)
      }, "ApplicationCookie");
            //Si IsPersistent es false el tiempo de caducidad de la cookie se toma de Session
            //Si IsPersistent es true y no especifica ExpiresUtc el valor se toma de ExpireTimeSpan de startup.(Marcada)
            //Si IsPersistent es true y SI especifica ExpiresUtc el valor se respeta (No marcada)
            var d = DateTime.UtcNow.AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["Session_Timeout"]));
            AuthenticationProperties authenticationProperty = new AuthenticationProperties();
            if (credentials.RememberMe)
                authenticationProperty = new AuthenticationProperties()
                {
                    IsPersistent = true,
                    AllowRefresh = true
                };
            else
            if (credentials.RememberMe)
                authenticationProperty = new AuthenticationProperties()
                {
                    IsPersistent = true,
                    ExpiresUtc = d,
                    AllowRefresh = true
                };
            //authenticationProperty.IssuedUtc = d;
            var authManager = ctx.Authentication;
            authManager.SignIn(authenticationProperty, identity);

            //  HttpRuntime.Cache.Insert(string.Format("UserRols_{0}", user.IdUsuario.ToString()), new List<string>() { rol }, null,
            // System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["Session_Timeout"])));

            // log
            // ip && browser version
            var ipAddress = HttpContext.Current.Request.UserHostAddress;
            var version = HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version;
        }

        public static Response ValidaSesion(CredentialsDTO credentials, Microsoft.Owin.IOwinContext ctx)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                CreaSesion(credentials, ctx);
            return new Response() { Options = ResultType.OK, Record = credentials };
        }

        public static bool SesionVigente(string Permiso)
        {
            string Rol = "";
            Guid Id = new Guid();

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (Permiso == "HOME")
                {
                    return true;
                }
                var IdObject = ((ClaimsIdentity)HttpContext.Current.User.Identity).Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault();
                if (IdObject != null && IdObject.Value != null)
                {
                    Id = new Guid(IdObject.Value.ToString());
                }
                return true;
            }
            return false;
        }

    }
}