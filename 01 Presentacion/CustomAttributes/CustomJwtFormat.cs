﻿using System;
using System.Text;
using Microsoft.Owin.Security;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

namespace GreinerAssistec.CustomAttributes
{
    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private const string AudiencePropertyKey = "audience";

        private readonly string _issuer = string.Empty;

        public CustomJwtFormat(string issuer)
        {
            _issuer = issuer;
        }

        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            string ClientId = data.Properties.Dictionary.ContainsKey(AudiencePropertyKey) ? data.Properties.Dictionary[AudiencePropertyKey] : null;

            if (string.IsNullOrWhiteSpace(ClientId)) throw new InvalidOperationException("AuthenticationTicket.Properties does not include audience");

            //Audience audience =
            //var cred = Aplicacion.Negocio.ClientesAutenticacion.Instance().ObtenerCredenciales(ClientId).Record as ClienteAutenticacion;

            string symmetricKeyAsBase64 = "Contrasena01";// cred.Contrasena;

            const string sec = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
            var now = DateTime.UtcNow;
            //var keyByteArray = TextEncodings.Base64Url.Decode(symmetricKeyAsBase64);
            var keyByteArray = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.Default.GetBytes(sec));

            //var signingKey = new HmacSigningCredentials(keyByteArray);
            var signingKey = new Microsoft.IdentityModel.Tokens.SigningCredentials(keyByteArray, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);
            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            var token = new JwtSecurityToken(_issuer, ClientId, data.Identity.Claims, issued.Value.UtcDateTime,
                                             expires.Value.UtcDateTime, signingKey);
            
            var handler = new JwtSecurityTokenHandler();

            var jwt = handler.WriteToken(token);

            return jwt;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}