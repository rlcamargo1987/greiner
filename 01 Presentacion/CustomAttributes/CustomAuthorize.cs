﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GreinerAssistec.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class CustomAuthorize : AuthorizeAttribute
    {
        public CustomAuthorize(string Controller)
        {
            var cacheKey = string.Format("UserRoles_{0}", HttpContext.Current.User.Identity.Name.Substring(HttpContext.Current.User.Identity.Name.IndexOf(@"\") + 1));
            string[] AuthorizedControllers = (string[])HttpRuntime.Cache[cacheKey];

            if (!AuthorizedControllers.Contains(Controller))
                throw new ArgumentException("not authorized");
        }
    }
}