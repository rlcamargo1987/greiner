﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Claims;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System.Web.Http.Controllers;
using Microsoft.IdentityModel.Tokens;
using System.Security.Principal;

namespace GreinerAssistec.CustomAttributes
{
    public class CommonApiAuthorize : System.Web.Http.AuthorizeAttribute
    {
        public CommonApiAuthorize()//string Permiso
        {
            //this.Users
            //this.Option
            //this.Roles
        }
        // Custom property
        public string Option { get; set; }
        public List<string> ActivatedRoles { get; set; }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var resp = base.IsAuthorized(actionContext);
            if (actionContext.Request.Headers.Authorization == null)
                return false;
            var jwt = actionContext.Request.Headers.Authorization.ToString().Replace("Bearer ", "");
            var r = AuthenticateJwtToken(jwt).Result;
            if (r == null)
            {
                return false;
            }
            var claim = (r as System.Security.Claims.ClaimsPrincipal).Claims.Where(x => x.Type == ClaimTypes.Role).FirstOrDefault();
            if (claim == null)
            {
                return false;
            }
            var id = new Guid(claim.Value);
            if (id == null)
            {
                return false;
            }
            /*
            var RolYPermisos = Aplicacion.Negocio.Roles.Instance().ObtenerPermisos(id);
            if (RolYPermisos.Record == null || (RolYPermisos.Record as RolYPermisoDto).Permisos == null)
            {
                return false;
            }*/
            var isAuthorized = true;// (RolYPermisos.Record as RolYPermisoDto).Permisos.Where(x => x.Nombre == Option).Any();
            return isAuthorized;
        }

        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;

                const string sec = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
                var now = DateTime.UtcNow;
                //var keyByteArray = TextEncodings.Base64Url.Decode(symmetricKeyAsBase64);
                var symmetricKey = Encoding.Default.GetBytes(sec);// Convert.FromBase64String(Secret);

                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(symmetricKey)
                };

                Microsoft.IdentityModel.Tokens.SecurityToken securityToken;
                var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

                return principal;
            }

            catch (Exception ex)
            {
                //should write log
                return null;
            }
        }

        private static bool ValidateToken(string token, out string username, out string id)
        {
            //, out string rolYPermisos
            username = null;
            id = null;
            var simplePrinciple = GetPrincipal(token);
            if (simplePrinciple == null) return false;
            var identity = simplePrinciple.Identity as ClaimsIdentity;

            if (identity == null)
                return false;

            if (!identity.IsAuthenticated)
                return false;

            var usernameClaim = identity.FindFirst(ClaimTypes.Name);
            username = usernameClaim?.Value;

            if (string.IsNullOrEmpty(username))
                return false;

            var RoleClaim = identity.FindFirst(ClaimTypes.Role);
            id = RoleClaim?.Value;

            if (string.IsNullOrEmpty(id))
                return false;

            return true;
        }

        protected Task<IPrincipal> AuthenticateJwtToken(string token)
        {
            string username;
            string id;//new Claim(ClaimTypes.Role, id)
            if (ValidateToken(token, out username, out id))
            {
                // based on username to get more information from database in order to build local identity
                var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, username),
                new Claim(ClaimTypes.Role, id)
                // Add more claims if needed: Roles, ...
            };

                var identity = new ClaimsIdentity(claims, "Jwt");
                IPrincipal user = new ClaimsPrincipal(identity);

                return Task.FromResult(user);
            }

            return Task.FromResult<IPrincipal>(null);
        }
    }
}