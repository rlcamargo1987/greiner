﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace GreinerAssistec.Models
{
    public class Permisos
    {

        public const string ConfiguracionController_ParametrosPortal = "Administrador_ConfiguracionController_ParametrosPortal";
        public const string ConfiguracionController_ConfiguracionVolumetricos = "Administrador_ConfiguracionController_ConfiguracionVolumetricos";
        public const string UsuariosController_ListadoUsuarios = "Administrador_UsuariosController_ListadoUsuarios";
        public const string DashboardController_Tablero = "Dashboard_DashboardController_Tablero";
        public const string ConsultaController_Volumetrico = "Administrador_ConsultaController_Volumetrico";
        public const string ConsultaController_Merma = "Administrador_ConsultaController_Merma";
        public const string eCommerceController_Clientes = "Layout_eCommerceController_Clientes";
        public const string eCommerceController_Productos = "Layout_eCommerceController_Productos";
        public const string eCommerceController_Proveedores = "Layout_eCommerceController_Proveedores";
        public const string InventariosController_Index = "Layout_InventariosController_Index";
        public const string InventariosController_Consultas = "Layout_InventariosController_Consultas";
        public const string LayoutController_Index = "Layout_LayoutController_Index";
        public const string LayoutController_Conciliacion = "Layout_LayoutController_Conciliacion";
        public const string LoginController_Access = "Security_LoginController_Access";
        public const string PerfilController_Index = "Usuario_PerfilController_Index";
        public const string PerfilController_Editar = "Usuario_PerfilController_Editar";
        public const string SeguridadController_CambiarContrasena = "Usuario_SeguridadController_CambiarContrasena";
        public const string SeguridadController_ReseteaContrasena = "Usuario_SeguridadController_ReseteaContrasena";
        public const string SeguridadController_ActivarContrasena = "Usuario_SeguridadController_ActivarContrasena"; 
        public const string BannerController_Index = "Banners_BannerController_Index";
        public const string BannerController_Crear = "Banners_BannerController_Crear";
        public const string BannerController_Editar = "Banners_BannerController_Editar";
        public const string BannerController_Detalle = "Banners_BannerController_Detalle";
        public const string HorarioAtencionController_Index = "Horarios_HorarioAtencionController_Index";
        public const string Pedido_Abasto_AbastoController_Index = "Pedido_Abasto_AbastoController_Index";
        public const string Pedido_Abasto_AbastoController_EgresosPagos = "Pedido_Abasto_AbastoController_EgresosPagos";
        public const string Pedido_Abasto_AbastoController_Capacidades = "Pedido_Abasto_AbastoController_Capacidades";
        public const string Pedido_Abasto_AbastoController_Workflow = "Pedido_Abasto_AbastoController_Workflow";
        public const string Pedido_Abasto_AbastoController_Turnos = "Pedido_Abasto_AbastoController_Turnos";
        public const string Pedido_Abasto_AbastoController_TAD = "Pedido_Abasto_AbastoController_TAD";

    }

    public static class PermisosExtension
    {
        public static string GetDescription<T>(this T enumerationValue)
    where T : struct
        {
            Type type = enumerationValue.GetType();
            if (!type.IsEnum)
            {
                throw new ArgumentException("EnumerationValue must be of Enum type", "enumerationValue");
            }

            //Tries to find a DescriptionAttribute for a potential friendly name
            //for the enum
            MemberInfo[] memberInfo = type.GetMember(enumerationValue.ToString());
            if (memberInfo != null && memberInfo.Length > 0)
            {
                object[] attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    //Pull out the description value
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }
            //If we have no description attribute, just return the ToString of the enum
            return enumerationValue.ToString();
        }
    }
}