﻿/// <reference path="Scripts/DefinedUser/GreinerAssistec.js" />
// Inicialización de namespace GreinerAssistec
var GreinerAssistec = window.GreinerAssistec || {};
// Módulo de inicialización y funciones genéricas que se utilizaran en todos los módulos
GreinerAssistec.App = (function ($, window, document, undefined) {
    var isCompleted = false;
    var Scheme;

    var typeStatusHouses = [{ id: 'BFE36FE8-6B66-460A-B329-8023394F16AD', name: 'Solded/Rented' }, { id: 'A8CE1A69-A949-4FDE-A000-FF0739FF5ADF', name: 'Active' }]

    /**
     * Variables
     **/
    var vModel;
    var ajaxCompleted = false;

    var ViewModel = function () {
        return vModel;
    }

    var isAjaxCompleted = function () {
        return ajaxCompleted;
    }

    var VModel = function (path) {
        var self = this;
        self.General = new Object();
        self.General.RelativePath = ko.observable(path);
    }

    var onBeforeSendBasicHeader = function (xhr, settings) {
        var token = 'Y2xpZW50MTpzZWNyZXQ=';
        var headers = {};
        headers.Authorization = 'Basic ' + token;
        return headers;
    }

    var onBeforeSendBearerHeader = function (xhr, settings) {
        var token = JSON.parse(sessionStorage.access_token).access_token;
        var headers = {};
        if (token) {
            console.log(1);
            headers.Authorization = 'Bearer ' + token;
        }
        console.log(headers);
        console.log(2);
        return headers;
        if (typeof sessionStorage.access_token !== null) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + JSON.parse(sessionStorage.access_token).access_token);
        }
        else {
            onBeforeSendBasicHeader(xhr, settings);
        }
    }

    var initKOControls = function () {
        if (!isCompleted) {
            console.log("Controls KO inicializados");
            $.datepicker.setDefaults($.datepicker.regional['es']);
            //KO Selects

            ko.bindingHandlers.selectPicker = {
                init: function (element, valueAccessor, allBindingsAccessor) {
                    if ($(element).is('select')) {
                        if (ko.isObservable(valueAccessor())) {
                            if ($(element).prop('multiple') && $.isArray(ko.utils.unwrapObservable(valueAccessor()))) {
                                // in the case of a multiple select where the valueAccessor() is an observableArray, call the default Knockout selectedOptions binding
                                ko.bindingHandlers.selectedOptions.init(element, valueAccessor, allBindingsAccessor);
                            } else {
                                // regular select and observable so call the default value binding
                                ko.bindingHandlers.value.init(element, valueAccessor, allBindingsAccessor);
                            }
                        }
                        $(element).addClass('selectpicker').selectpicker();
                    }
                },
                update: function (element, valueAccessor, allBindingsAccessor) {
                    if ($(element).is('select')) {
                        var selectPickerOptions = allBindingsAccessor().selectPickerOptions;
                        if (typeof selectPickerOptions !== 'undefined' && selectPickerOptions !== null) {
                            var options = selectPickerOptions.optionsArray,
                              optionsText = selectPickerOptions.optionsText,
                              optionsValue = selectPickerOptions.optionsValue,
                              optionsCaption = selectPickerOptions.optionsCaption,
                              isDisabled = selectPickerOptions.disabledCondition || false,
                              resetOnDisabled = selectPickerOptions.resetOnDisabled || false;
                            if (ko.utils.unwrapObservable(options).length > 0) {
                                // call the default Knockout options binding
                                ko.bindingHandlers.options.update(element, options, allBindingsAccessor);
                            }
                            if (isDisabled && resetOnDisabled) {
                                // the dropdown is disabled and we need to reset it to its first option
                                $(element).selectpicker('val', $(element).children('option:first').val());
                            }
                            $(element).prop('disabled', isDisabled);
                        }
                        if (ko.isObservable(valueAccessor())) {
                            if ($(element).prop('multiple') && $.isArray(ko.utils.unwrapObservable(valueAccessor()))) {
                                // in the case of a multiple select where the valueAccessor() is an observableArray, call the default Knockout selectedOptions binding
                                ko.bindingHandlers.selectedOptions.update(element, valueAccessor);

                            } else {
                                // call the default Knockout value binding
                                ko.bindingHandlers.value.update(element, valueAccessor);
                            }
                        }
                        $(element).selectpicker('refresh');
                    }
                }
            }

            ko.validation.rules.pattern.message = 'Invalid.';

            ko.validation.init({
                registerExtenders: true,
                messagesOnModified: true,
                insertMessages: false,
                parseInputAttributes: true,
                messageTemplate: null,
            }, true);
            //KO Dates
            isCompleted = true;
        }
    }

    var initNotificationControls = function () {
        var _oldShow = $.fn.show;

        var notifications = new $.ttwNotificationMenu({
            notificationList: {
                anchor: 'item',
                offset: '0 15'
            }
        });

        notifications.initMenu({
            projects: '#projects',
            tasks: '#tasks',
            messages: '#messages'
        });


        $('#notification-form').submit(function (e) {

            var $form = $(this), type;
            type = $form.find('#type').val();

            var options = {
                category: $form.find('#category').val(),
                message: ($form.find('#message') != '') ? $form.find('#message').val() : 'Sample Notification'
            };

            if ($form.find('#use-icon').is(':checked')) {
                var icon = $form.find('#use-icon-url').val();

                options.icon = icon;
            }

            notifications.createNotification(options);

            return false;
        });

        $('#auto-hide-container').find('input').change(function () {
            if ($(this).is(':checked')) {
                $('#auto-hide-delay-container').css('display', 'block');
            }
            else {
                $('#auto-hide-delay-container').css('display', 'none');
            }
        });

        $('#field2-container').find('select').change(function () {
            if ($.inArray($(this).val(), ['success', 'error', 'warning', 'notice']) != -1) {
                $('#helper-container').css('display', 'block');

                $('#use-icon-container, #use-icon-url-container, #auto-hide-container, #auto-hide-delay-container').css('display', 'none');

            }
            else {
                $('#helper-container').css('display', 'none');
                $('#use-icon-container,  #auto-hide-container').css('display', 'block');
            }
        });

        $('#use-icon-container').find('input').change(function () {
            if ($(this).is(':checked')) {
                $('#use-icon-url-container').css('display', 'block');
            }
            else {
                $('#use-icon-url-container').css('display', 'none');
            }
        });

        $('#notification-form').find("[title]").tooltip({
            position: 'center right',
            offset: [0, 24]
        });

        /*!
         * jQuery Tools v1.2.5 - The missing UI library for the Web
         *
         * tooltip/tooltip.js
         * tooltip/tooltip.slide.js
         *
         * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
         *
         * http://flowplayer.org/tools/
         *
         */
        (function (a) { a.tools = a.tools || { version: "v1.2.5" }, a.tools.tooltip = { conf: { effect: "toggle", fadeOutSpeed: "fast", predelay: 0, delay: 30, opacity: 1, tip: 0, position: ["top", "center"], offset: [0, 0], relative: !1, cancelDefault: !0, events: { def: "mouseenter,mouseleave", input: "focus,blur", widget: "focus mouseenter,blur mouseleave", tooltip: "mouseenter,mouseleave" }, layout: "<div/>", tipClass: "tooltip" }, addEffect: function (a, c, d) { b[a] = [c, d] } }; var b = { toggle: [function (a) { var b = this.getConf(), c = this.getTip(), d = b.opacity; d < 1 && c.css({ opacity: d }), c.show(), a.call() }, function (a) { this.getTip().hide(), a.call() }], fade: [function (a) { var b = this.getConf(); this.getTip().fadeTo(b.fadeInSpeed, b.opacity, a) }, function (a) { this.getTip().fadeOut(this.getConf().fadeOutSpeed, a) }] }; function c(b, c, d) { var e = d.relative ? b.position().top : b.offset().top, f = d.relative ? b.position().left : b.offset().left, g = d.position[0]; e -= c.outerHeight() - d.offset[0], f += b.outerWidth() + d.offset[1], /iPad/i.test(navigator.userAgent) && (e -= a(window).scrollTop()); var h = c.outerHeight() + b.outerHeight(); g == "center" && (e += h / 2), g == "bottom" && (e += h), g = d.position[1]; var i = c.outerWidth() + b.outerWidth(); g == "center" && (f -= i / 2), g == "left" && (f -= i); return { top: e, left: f } } function d(d, e) { var f = this, g = d.add(f), h, i = 0, j = 0, k = d.attr("title"), l = d.attr("data-tooltip"), m = b[e.effect], n, o = d.is(":input"), p = o && d.is(":checkbox, :radio, select, :button, :submit"), q = d.attr("type"), r = e.events[q] || e.events[o ? p ? "widget" : "input" : "def"]; if (!m) throw "Nonexistent effect \"" + e.effect + "\""; r = r.split(/,\s*/); if (r.length != 2) throw "Tooltip: bad events configuration for " + q; d.bind(r[0], function (a) { clearTimeout(i), e.predelay ? j = setTimeout(function () { f.show(a) }, e.predelay) : f.show(a) }).bind(r[1], function (a) { clearTimeout(j), e.delay ? i = setTimeout(function () { f.hide(a) }, e.delay) : f.hide(a) }), k && e.cancelDefault && (d.removeAttr("title"), d.data("title", k)), a.extend(f, { show: function (b) { if (!h) { l ? h = a(l) : e.tip ? h = a(e.tip).eq(0) : k ? h = a(e.layout).addClass(e.tipClass).appendTo(document.body).hide().append(k) : (h = d.next(), h.length || (h = d.parent().next())); if (!h.length) throw "Cannot find tooltip for " + d } if (f.isShown()) return f; h.stop(!0, !0); var o = c(d, h, e); e.tip && h.html(d.data("title")), b = b || a.Event(), b.type = "onBeforeShow", g.trigger(b, [o]); if (b.isDefaultPrevented()) return f; o = c(d, h, e), h.css({ position: "absolute", top: o.top, left: o.left }), n = !0, m[0].call(f, function () { b.type = "onShow", n = "full", g.trigger(b) }); var p = e.events.tooltip.split(/,\s*/); h.data("__set") || (h.bind(p[0], function () { clearTimeout(i), clearTimeout(j) }), p[1] && !d.is("input:not(:checkbox, :radio), textarea") && h.bind(p[1], function (a) { a.relatedTarget != d[0] && d.trigger(r[1].split(" ")[0]) }), h.data("__set", !0)); return f }, hide: function (c) { if (!h || !f.isShown()) return f; c = c || a.Event(), c.type = "onBeforeHide", g.trigger(c); if (!c.isDefaultPrevented()) { n = !1, b[e.effect][1].call(f, function () { c.type = "onHide", g.trigger(c) }); return f } }, isShown: function (a) { return a ? n == "full" : n }, getConf: function () { return e }, getTip: function () { return h }, getTrigger: function () { return d } }), a.each("onHide,onBeforeShow,onShow,onBeforeHide".split(","), function (b, c) { a.isFunction(e[c]) && a(f).bind(c, e[c]), f[c] = function (b) { b && a(f).bind(c, b); return f } }) } a.fn.tooltip = function (b) { var c = this.data("tooltip"); if (c) return c; b = a.extend(!0, {}, a.tools.tooltip.conf, b), typeof b.position == "string" && (b.position = b.position.split(/,?\s/)), this.each(function () { c = new d(a(this), b), a(this).data("tooltip", c) }); return b.api ? c : this } })(jQuery);
        (function (a) { var b = a.tools.tooltip; a.extend(b.conf, { direction: "up", bounce: !1, slideOffset: 10, slideInSpeed: 200, slideOutSpeed: 200, slideFade: !a.browser.msie }); var c = { up: ["-", "top"], down: ["+", "top"], left: ["-", "left"], right: ["+", "left"] }; b.addEffect("slide", function (a) { var b = this.getConf(), d = this.getTip(), e = b.slideFade ? { opacity: b.opacity } : {}, f = c[b.direction] || c.up; e[f[1]] = f[0] + "=" + b.slideOffset, b.slideFade && d.css({ opacity: 0 }), d.show().animate(e, b.slideInSpeed, a) }, function (b) { var d = this.getConf(), e = d.slideOffset, f = d.slideFade ? { opacity: 0 } : {}, g = c[d.direction] || c.up, h = "" + g[0]; d.bounce && (h = h == "+" ? "-" : "+"), f[g[1]] = h + "=" + e, this.getTip().animate(f, d.slideOutSpeed, function () { a(this).hide(), b.call() }) }) })(jQuery);

    }

    var Initialize = function (path, scheme) {

        //initKOControls();
        //initNotificationControls();
        vModel = new VModel(path);
        Scheme = scheme;

        arrayRules = {
            LongitudMinima: { Activa: true, Valor: 5, Error: "La longitud minima es 5" },
            Minuscula: { Activa: true, Error: "La contraseña debe contener al menos una letra minuscula." },
            Mayuscula: { Activa: true, Error: "La contraseña debe contener al menos una letra mayuscula." },
            Numeros: { Activa: true, Error: "La contraseña debe contener al menos un numero." },
            CaracteresEspeciales: { Activa: true, Error: "La contraseña debe contener al uno de los siguientes caracteres especiales !@#$&*." }
        };


    }

    var Redirect = function (module, idUser, idHouse) {
        var url;
        console.log(url);
        switch (module) {
            case 'Home':
                url = vModel.General.RelativePath();
                window.location.href = url;
                break;
            case 'Login':
                url = vModel.General.RelativePath() + "Seguridad/Acceso/Inicio";
                window.location.href = url;
                break;
            case 'VerPerfil':
                url = vModel.General.RelativePath() + "Usuario/Perfil/Index";
                console.log(url);
                window.location.href = url;
                break;
            case 'Listado Usuarios':
                url = vModel.General.RelativePath() + "Administrador/Usuarios/ListadoUsuarios";
                window.location.href = url;
                break;
            case 'EditarPerfil':
                url = vModel.General.RelativePath() + "Usuario/Perfil/Editar";
                window.location.href = url;
                break;
            case 'CambiarContrasena':
                url = vModel.General.RelativePath() + "Usuario/Seguridad/CambiarContrasena";
                window.location.href = url;
                break;
            case 'Layout':
                url = vModel.General.RelativePath() + "Layout/Layout/Index";
                window.location.href = url;
                break;
            case 'Conciliacion':
                url = vModel.General.RelativePath() + "Layout/Layout/Conciliacion";
                window.location.href = url;
                break;
            case 'Carga de Layouts':
                url = vModel.General.RelativePath() + "Layout/Layout/LoadLayout";
                window.location.href = url;
                break;
            case 'Estado de Cuenta Bancario':
                url = vModel.General.RelativePath() + "Layout/Layout/LoadBalance";
                window.location.href = url;
                break;
            case 'Carga Recepcion Ventas':
                url = vModel.General.RelativePath() + "layout/layout/loadreportecompra";
                window.location.href = url;
                break;
            case 'Inventarios':
                url = vModel.General.RelativePath() + "Layout/Inventarios/Index";
                window.location.href = url;
                break;
            case 'Consultas':
                url = vModel.General.RelativePath() + "Layout/Inventarios/Consultas";
                window.location.href = url;
                break;
            case 'ParametrosPortal':
                url = vModel.General.RelativePath() + "Administrador/Configuracion/ParametrosPortal";
                window.location.href = url;
                break;
            case 'ConfiguracionVolumetricos':
                url = vModel.General.RelativePath() + "Administrador/Configuracion/ConfiguracionVolumetricos";
                window.location.href = url;
                break;
            case 'ConfiguracionIoT':
                url = vModel.General.RelativePath() + "Administrador/Configuracion/ConfiguracionIoT";
                window.location.href = url;
                break;
            case 'ConsultaVolumetrico':
                url = vModel.General.RelativePath() + "Administrador/Consulta/Volumetrico";
                window.location.href = url;
                break;
            case 'ConsultaMerma':
                url = vModel.General.RelativePath() + "Administrador/Consulta/Merma";
                window.location.href = url;
                break;
            case 'Dashboard':
                url = vModel.General.RelativePath() + "Dashboard/Dashboard/Tablero";
                window.location.href = url;
                break;
            case 'MasterDashboard':
                url = vModel.General.RelativePath() + "Dashboard/Dashboard/Maestro";
                window.location.href = url;
                break;
            case 'Clientes':
                url = vModel.General.RelativePath() + "Layout/eCommerce/Clientes";
                window.location.href = url;
                break;
            case 'Productos':
                url = vModel.General.RelativePath() + "Layout/eCommerce/Productos";
                window.location.href = url;
                break;
            case 'Proveedores':
                url = vModel.General.RelativePath() + "Layout/eCommerce/Proveedores";
                window.location.href = url;
                break;
            case 'Pedidos':
                url = vModel.General.RelativePath() + "Layout/eCommerce/Pedidos";
                window.location.href = url;
                break;
            case 'PedidosArchivos':
                url = vModel.General.RelativePath() + "Layout/eCommerce/ArchivosPedidos";
                window.location.href = url;
                break;
            case 'Banners':
                url = vModel.General.RelativePath() + "Banners/Banner/Index";
                window.location.href = url;
                break;
            case 'HorarioAtencion':
                url = vModel.General.RelativePath() + "Horarios/HorarioAtencion/Index";
                window.location.href = url;
                break;
            case 'Carga Estado Cuenta':
                url = vModel.General.RelativePath() + "Layout/eCommerce/CargaEstadoCuenta";
                window.location.href = url;
                break;
            case 'Conciliar Pedido':
                url = vModel.General.RelativePath() + "Layout/eCommerce/Conciliacion";
                window.location.href = url;
                break;
            case 'ReportesModificacion':
                url = vModel.General.RelativePath() + "Combustible/Abasto/ReporteModificacion";
                window.location.href = url;
                break;
            case 'ReporteAdicion':
                url = vModel.General.RelativePath() + "Combustible/Abasto/ReporteAdicion";
                window.location.href = url;
                break;
            case 'Abasto':
                url = vModel.General.RelativePath() + "Combustible/Abasto/Index";
                window.location.href = url;
                break;
            case 'Reporte Fact':
                url = vModel.General.RelativePath() + "Combustible/Abasto/ReporteFact";
                window.location.href = url;
                break;
            case 'Egresos Pagos':
                url = vModel.General.RelativePath() + "Combustible/Abasto/EgresosPagos";
                window.location.href = url;
                break;
            case 'Facturación de Venta de Combustible':
                url = vModel.General.RelativePath() + "Combustible/Abasto/VentaCombustible";
                window.location.href = url;
                break;
            case 'Venta de Combustible Gasydi':
                url = vModel.General.RelativePath() + "Combustible/Abasto/VentaCombustibleGasydi";
                window.location.href = url;
                break;
            case 'Facturas Curioso':
                url = vModel.General.RelativePath() + "Combustible/Abasto/FacturasCurioso";
                window.location.href = url;
                break;
            case 'Reporte Facturacion':
                url = vModel.General.RelativePath() + "Combustible/Abasto/ReporteFacturacion";
                window.location.href = url;
                break;
            case 'Capacidades':
                url = vModel.General.RelativePath() + "Combustible/Abasto/Capacidades";
                window.location.href = url;
                break;
            case 'Workflow':
                url = vModel.General.RelativePath() + "Combustible/Abasto/Workflow";
                window.location.href = url;
                break;
            case 'Turnos':
                url = vModel.General.RelativePath() + "Combustible/Abasto/Turnos";
                window.location.href = url;
                break;
            case 'Facturas Portal PEMEX':
                url = vModel.General.RelativePath() + "Combustible/Abasto/FacturasPortalPemex";
                window.location.href = url;
                break;
            case 'TAD':
                url = vModel.General.RelativePath() + "Combustible/Abasto/TAD";
                window.location.href = url;
                break;
            case 'Calendario':
                url = vModel.General.RelativePath() + "Combustible/Calendario/Index";
                window.location.href = url;
                break;
            case 'RegionesCuentas':
                url = vModel.General.RelativePath() + "Layout/eCommerce/RegionesCuentas";
                window.location.href = url;
                break;
            case 'ReporteHistoricos':
                url = vModel.General.RelativePath() + "Layout/eCommerce/ReporteHistoricos";
                window.location.href = url;
                break;
            case 'CatalogoAlmacenes':
                url = vModel.General.RelativePath() + "ExtremoAExtremo/Almacenes/CatalogoAlmacenes";
                window.location.href = url;
                break;
            case 'TipoMovimientos':
                url = vModel.General.RelativePath() + "ExtremoAExtremo/Almacenes/TipoMovimientos";
                window.location.href = url;
                break;
            case 'Movimientos':
                url = vModel.General.RelativePath() + "ExtremoAExtremo/Almacenes/Movimientos";
                window.location.href = url;
                break;
            case 'ProcesosFacturacion':
                url = vModel.General.RelativePath() + "Combustible/Abasto/ProcesosFacturacion";
                window.location.href = url;
                break;
            case 'EstacionesServicio':
                url = vModel.General.RelativePath() + "Combustible/Abasto/EstacionesServicio";
                window.location.href = url;
                break;
            case 'ReglasVencimientoCredito':
                url = vModel.General.RelativePath() + "Combustible/Abasto/ReglasVencimientoCredito";
                window.location.href = url;
                break;
            default:
                break;
        }
    }

    var getScheme = function () {
        return Scheme;
    }

    var callRegister = function () {
        $('#modal-login').modal('hide');
        setTimeout(function () { $('#modal-register').modal('show'); }, 1000);

    }

    var callForgetPassword = function () {
        $('#modal-login').modal('hide');
        setTimeout(function () { $('#ForgetPass').modal('show'); }, 1000);
    }

    var callLogin = function () {
        $('#modal-register').modal('hide');
        setTimeout(function () { $('#modal-login').modal('show'); }, 1000);
    }

    /*success, info warning danger*/
    var showAlert = function (idContainer, message, msjType, delay) {
        $("#" + idContainer).addClass("alert alert-" + msjType);
        $("#" + idContainer).attr("style", "min-height:100px;visibility:visible");
        $("#" + idContainer + " .alertContent").html(message);
        setTimeout(function () {
            //$("#" + idContainer + " .alertContent").empty();
            $("#" + idContainer).removeClass("alert alert-" + msjType);
            $("#" + idContainer).attr("style", "height:0px;visibility:collapse");
        }, delay);
    }

    var showConfirm = function (title, text, type, confirmButtomClass, confirmButtonText, closeOnConfirm, showCancelButton, cancelButtonText, closeOnCancel, fnResult) {
        //title = "Are you sure?";
        //text = "Your will not be able to recover this imaginary file!";
        //type = "warning";   //warning  |  info   | error  |  success
        //confirmButtomClass = "btn-danger";
        //confirmButtonText = "Yes, delete it!";
        //cancelButtonText = "No, cancel plx!"
        //fnResult = function (isConfirm) {
        //    if (isConfirm) {
        //        swal("Deleted!", "Your imaginary file has been deleted.", "success");
        //    } else {
        //        swal("Cancelled", "Your imaginary file is safe :)", "error");
        //    }
        //};
        swal({
            title: title,
            text: text,
            type: type,
            confirmButtonClass: confirmButtomClass,
            confirmButtonText: confirmButtonText,
            closeOnConfirm: closeOnConfirm,
            showCancelButton: showCancelButton,
            cancelButtonText: cancelButtonText,
            closeOnCancel: closeOnCancel,
            closeOnClickOutside: true,
            closeOnEsc: true,
            timer: 10000
        },
         fnResult
        );

    }

    var blockUI = function (id) {
        $('#' + id).block({
            message: '<img src="' + GreinerAssistec.App.ViewModel().General.RelativePath() + 'assets/images/loading.gif" width="50" height="50" />',
            css: { backgroundColor: 'rgb(255,255,255,0.0)', borderStyle: 'none' }
        });
    }

    var unblockUI = function (id) {
        $('#' + id).unblock();
    }

    var openWindow = function (url, name, iWidth, iHeight) {
        var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
        var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
        window.open(url, name, 'height=' + iHeight + ',innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=yes,resizeable=yes   ,location=no,status=no');
    }

    var statesBASE = function () {
        return s;
    }

    var TypeStatusHouses = function () {
        return typeStatusHouses;
    }

    function RemoveAccents(strAccents) {
        var accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
        var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
        strAccents = strAccents.split('');
        var strLen = strAccents.length;
        var i, x;
        for (i = 0; i < strLen; i++) {
            if ((x = accents.indexOf(strAccents[i])) != -1) {
                strAccents[i] = accentsOut[x];
            }
        }
        return strAccents.join('');
    }

    var sqlToJsDate = function (sqlDate) {
        //sqlDate in SQL DATETIME format ("yyyy-mm-dd hh:mm:ss.ms")
        var sqlDateArr1 = sqlDate.split("-");
        //format of sqlDateArr1[] = ['yyyy','mm','dd hh:mm:ms']
        var sYear = sqlDateArr1[0];
        var sMonth = (Number(sqlDateArr1[1]) - 1).toString();
        var sqlDateArr2 = sqlDateArr1[2].split("T");
        //format of sqlDateArr2[] = ['dd', 'hh:mm:ss.ms']
        var sDay = sqlDateArr2[0];
        var sqlDateArr3 = sqlDateArr2[1].split(":");
        //format of sqlDateArr3[] = ['hh','mm','ss.ms']
        var sHour = sqlDateArr3[0];
        var sMinute = sqlDateArr3[1];
        //var sqlDateArr4 = sqlDateArr3[2].split(".");
        //format of sqlDateArr4[] = ['ss','ms']
        var sSecond = sqlDateArr3[0];
        var sMillisecond = 0;// sqlDateArr4[1];

        return new Date(sYear, sMonth, sDay, sHour, sMinute, sSecond, sMillisecond);
    }

    function FormatDate(dateVal) {
        var newDate = new Date(dateVal);

        var sMonth = padValue(newDate.getMonth() + 1);
        var sDay = padValue(newDate.getDate());
        var sYear = newDate.getFullYear();
        var sHour = newDate.getHours();
        var sMinute = padValue(newDate.getMinutes());
        var sAMPM = "AM";

        var iHourCheck = parseInt(sHour);

        if (iHourCheck > 12) {
            sAMPM = "PM";
            sHour = iHourCheck - 12;
        }
        else if (iHourCheck === 0) {
            sHour = "12";
        }

        sHour = padValue(sHour);

        return sDay + "-" + sMonth + "-" + sYear + " " + sHour + ":" + sMinute + " " + sAMPM;
    }

    function padValue(value) {
        return (value < 10) ? "0" + value : value;
    }

    var DateFormated = function (date) {
        var dformat = [date.getMonth() + 1,
                       date.getDate(),
                       date.getFullYear()].join('/') + ' ' +
                      [date.getHours(),
                       date.getMinutes(),
                       date.getSeconds()].join(':');
        return dformat;
    }

    var OnlyDate = function (date) {
        var dformat = [date.getDate(),
                       date.getMonth() + 1,
                       date.getFullYear()].join('/');
        return dformat;
    }

    var ShowLoading = function () {
        $("#LoadingDiv").fadeIn();
    }

    var HideLoading = function () {
        $("#LoadingDiv").fadeOut();
    }

    var estaAuthenticado = function (auth, name, rel) {
        if (auth == "True" && name.indexOf('@') >= 0) {
            alert("Desloguea");
            GreinerAssistec.App.Login.Logout(rel);
            return false;
        } else
            return true;
    }

    var FormatMoney = function (value, withsign) {
        if (value == null || value == Window.undefined)
            return value;
        if (isNaN(value))//Si es cadena, se castea  -- value.toString().indexOf('.') != -1
            value = parseFloat(value);
        var num = (withsign ? '$' : '') + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        return num;
    }

    var FormatQuantity = function (value) {
        if (value == null || value == Window.undefined)
            return value;
        var num = value.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        return num;
    }

    var downloadFile = function (data, fileName, fileType) {
        // Set objects for file generation.
        var blob, url, a, extension;

        // Get time stamp for fileName.
        var stamp = new Date().getTime();

        // Set MIME type and encoding.
        fileType = (fileType || "text/csv;charset=UTF-8");
        extension = fileType.split("/")[1].split(";")[0];
        // Set file name.
        fileName = (fileName || "ActiveVoice_" + stamp + "." + extension);

        // Set data on blob.
        blob = new Blob([data], { type: fileType });

        // Set view.
        if (blob) {
            // Read blob.
            url = window.URL.createObjectURL(blob);

            // Create link.
            a = document.createElement("a");
            // Set link on DOM.
            document.body.appendChild(a);
            // Set link's visibility.
            a.style = "display: none";
            // Set href on link.
            a.href = url;
            // Set file name on link.
            a.download = fileName;

            // Trigger click of link.
            a.click();

            // Clear.
            window.URL.revokeObjectURL(url);
        } else {
            // Handle error.
        }
    };

    var goBack = function () {
        console.log("GO BACK");
        window.history.back();
    }

    var session = function () {
        $.ajax({
            type: 'GET',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + "Home/CurrentSession",
            cache: false,// obtener siempre los datos mas actualizados
        }).done(function (data) {
            var currentdate = new Date();
            var datetime = "Ultima sincronización: " + currentdate.getDate() + "/"
                            + (currentdate.getMonth() + 1) + "/"
                            + currentdate.getFullYear() + " @ "
                            + currentdate.getHours() + ":"
                            + currentdate.getMinutes() + ":"
                            + currentdate.getSeconds();
            if (!data.Record.EstaAutenticado) {
                swal({
                    title: "Notificación",
                    text: "Se ha terminado su sesión por seguridad",
                    icon: "warning",
                    button: "Enterado",
                });
                console.log("Se ha terminado la sesión: " + datetime);
                setTimeout(function () {
                    //location.reload();
                    GreinerAssistec.App.Login.Logout();
                }, 3000);
            } else {
                console.log("Logueado: " + datetime);
            }
        }).fail(function (e) { console.log("only getToken"); console.log(e); });
    }

    //Example Summary Bootstrap Table http://jsfiddle.net/L6tm1tt3/
    var sumMoneyFormatter = function (data) {
        field = this.field;
        return GreinerAssistec.App.FormatMoney(data.reduce(function (sum, row) {
            var r = sum + (+row[field]);
            return r;
        }, 0), true);
    }

    var sumFormatter = function (data) {
        field = this.field;
        return GreinerAssistec.App.FormatMoney(data.reduce(function (sum, row) {
            return sum + (+row[field]);
        }, 0), false);
    }

    var runningFormatter = function (value, row, index) {
        return index;
    }

    var totalTextFormatter = function (data) {
        return 'Total';
    }

    var totalFormatter = function (data) {
        return data.length;
    }

    var avgFormatter = function (data) {
        return sumFormatter.call(this, data) / data.length;
    }

    var msjError = "";
    var arrayRules;
    var validateStructurePassword = function (str) {
        msjError = "";
        return validate(str);
    }

    // Check if all the characters are present in string
    function validate(string) {
        var counter = 0;// Initialize counter to zero

        if (arrayRules.LongitudMinima.Activa == true && string.length >= arrayRules.LongitudMinima.Valor) {
            counter++;
        } else if (arrayRules.LongitudMinima.Activa == true) {
            msjError = arrayRules.LongitudMinima.Error + "\n";
        }

        if (arrayRules.Minuscula.Activa == true && /[a-z]/.test(string)) { // If string contain at least one lowercase alphabet character
            counter++;
        } else if (arrayRules.Minuscula.Activa == true) {
            msjError += arrayRules.Minuscula.Error + "\n";
        }

        if (arrayRules.Mayuscula.Activa == true && /[A-Z]/.test(string)) {
            counter++;
        } else if (arrayRules.Mayuscula.Activa == true) {
            msjError += arrayRules.Mayuscula.Error + "\n";
        }

        if (arrayRules.Numeros.Activa == true && /[0-9]/.test(string)) {
            counter++;
        } else if (arrayRules.Numeros.Activa == true) {
            msjError += arrayRules.Numeros.Error + "\n";
        }

        if (arrayRules.CaracteresEspeciales.Activa == true && /[!@#$&*]/.test(string)) {
            counter++;
        } else if (arrayRules.CaracteresEspeciales.Activa == true) {
            msjError += arrayRules.CaracteresEspeciales.Error + "\n";
        }
        if (msjError != "") {
            swal({
                title: "Atención",
                text: "Contraseña no valida. \n" + msjError,
                icon: "error",
                button: "Enterado",
            });
        }
        return counter == 5;
    }

    return {
        Initialize: Initialize,
        ShowConfirm: showConfirm,
        ViewModel: ViewModel,
        onBeforeSendBasicHeader: onBeforeSendBasicHeader,
        onBeforeSendBearerHeader: onBeforeSendBearerHeader,
        ajaxCompleted: isAjaxCompleted,
        ShowAlert: showAlert,
        getScheme: getScheme,
        CallRegister: callRegister,
        CallForgetPassword: callForgetPassword,
        CallLogin: callLogin,
        BlockUI: blockUI,
        UnblockUI: unblockUI,
        Redirect: Redirect,
        OpenWindow: openWindow,
        RemoveAccents: RemoveAccents,
        statesBASE: statesBASE,
        FormatDate: FormatDate,
        TypeStatusHouses: TypeStatusHouses,
        DateFormated: DateFormated,
        OnlyDate: OnlyDate,
        ShowLoading: ShowLoading,
        HideLoading: HideLoading,
        EstaAuthenticado: estaAuthenticado,
        FormatMoney: FormatMoney,
        FormatQuantity: FormatQuantity,
        DownloadFile: downloadFile,
        goBack: goBack,
        SqlToJsDate: sqlToJsDate,
        Session: session,
        SumFormatter: sumFormatter,
        SumMoneyFormatter: sumMoneyFormatter,
        RunningFormatter: runningFormatter,
        TotalTextFormatter: totalTextFormatter,
        TotalFormatter: totalFormatter,
        AvgFormatter: avgFormatter,
        ValidateStructurePassword: validateStructurePassword
    };
}(jQuery, window, document, navigator, undefined));

/**
 * Bootstrap Responsive Table Dropdown Plugin
 *
 * Currently if you attempt to use the bootstrap dropdown
 * plugin within a table wrapped in the .table-responsive
 * class the dropdown menu will be 'cut-off' (a portion of
 * the menu will be hidden).
 *
 * This plugin fixes this problem using fixed positioning
 * rather than absolute positioning for the elements that
 * need rescuing.
 *
 * @author Jake Wilkinson <jake.wilkinson@backslash.co.nz>
 * @version 1.0.0
 * @license MIT
 */
!function ($, o, n) { $(n).on("ready", function () { var n = $(o); $(".table-responsive [data-toggle=dropdown]").each(function () { var o = $(this), e = o.parent(), r = e.find(".dropdown-menu"); o.parent().on("show.bs.dropdown.bs-responsive-table-dropdown", function () { var o = e.position(); r.css({ top: o.top - n.scrollTop() + e.height(), left: o.left - n.scrollLeft() - r.width() + e.width(), right: "inherit", position: "fixed" }) }) }), n.on("scroll.bs-responsive-table-dropdown resize.bs-responsive-table-dropdown", function () { var o = $(".open > [data-toggle=dropdown]"), n = o.parent(), e = n.find(".dropdown-menu"); n.removeClass("open"), e.prop("aria-expanded", !1), o.blur() }) }) }(jQuery, window, document);

String.prototype.replaceAll = function (target, replacement) {
    return this.split(target).join(replacement);
};
/*
function ValidaCorreo(direccion) {
    var filtro = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var res = filtro.test(direccion);
    return res;
}

var IniciaControles = function () {
    $(document).ajaxError(function (xhr, props) {
        if (props.status === 401) {
            location.reload();
        }
    });


    $(document).on('click', '.panel-heading span.clickable', function (e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    })

    ko.bindingHandlers.datepicker = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            //initialize datepicker with some optional options
            var options = allBindingsAccessor().datepickerOptions || {
                format: 'DD/MM/YYYY',
                defaultDate: valueAccessor()()
            };
            $(element).datetimepicker(options);
            var picker = $(element).data('datetimepicker');

            //when a user changes the date, update the view model
            ko.utils.registerEventHandler(element, "dp.change", function (event) {
                var value = valueAccessor();
                if (ko.isObservable(value)) {
                    value(event.date._d);
                }
            });
        },
        update: function (element, valueAccessor) {
            $(element).datetimepicker("update", ko.utils.unwrapObservable(valueAccessor()));
            var widget = $(element).data("DateTimePicker");// $(element).data("datepicker");
            element.value = valueAccessor()().toLocaleDateString();
        }
    };

    ko.bindingHandlers.selectPicker = {
        after: ['options'],   // KO 3.0 feature to ensure binding execution order 







        init: function (element, valueAccessor, allBindingsAccessor) {
            var $element = $(element);
            $element.addClass('selectpicker').selectpicker();

            var doRefresh = function () {
                $element.selectpicker('refresh');
            }, subscriptions = [];

            // KO 3 requires subscriptions instead of relying on this binding's update
            // function firing when any other binding on the element is updated.

            // Add them to a subscription array so we can remove them when KO
            // tears down the element.  Otherwise you will have a resource leak.
            var addSubscription = function (bindingKey) {
                var targetObs = allBindingsAccessor.get(bindingKey);

                if (targetObs && ko.isObservable(targetObs)) {
                    subscriptions.push(targetObs.subscribe(doRefresh));
                }
            };

            addSubscription('options');
            addSubscription('value');           // Single
            addSubscription('selectedOptions'); // Multiple

            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                while (subscriptions.length) {
                    subscriptions.pop().dispose();
                }
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor) {
        }
    };

    ko.validation.rules.pattern.message = 'Invalid.';

    ko.validation.init({
        registerExtenders: true,
        messagesOnModified: true,
        insertMessages: false,
        parseInputAttributes: true,
        messageTemplate: null,
    }, true);
};


var UpdateLoaderViewModel = function (data) {
    var playerData = ko.mapping.fromJS(data);
    for (var propertyName in playerData.Loader) {
        vModel.Loader[propertyName](playerData.Loader[propertyName]());
    }
}

**
 * Autor: Rodrigo López Camargo
 * Nombre: 
 * Descripcion: 
 * Parametros de entrada: 
 * Retorno de la Funcion: 
 * Ejemplo de Implementacion:    
 *
var Notifier = function (message, _type, _delay, _position) {
    //Default  5
    var delay = 5;
    // default bottom-right,    Options: top, bottom, left, right
    var position = 'top-left';
    //Options: success, error, warning
    var type = 'success';

    if (_type != null)
        type = _type;

    if (_delay != null)
        delay = _delay;

    if (_position != null)
        position = _position;

    alertify.set('notifier', 'position', position);
    alertify.set('notifier', 'delay', delay);

    alertify.notify(message, type, delay, function () { });
};

var Redirect = function (url) {
    window.location.href = url;
};

var CloseDialogModal = function (idModal) {
    //$('#' + idModal).modal();
    $('#' + idModal).modal("hide");
};

var InitDialogModal = function (idModal) {
    // this.OpenPartialView(idContent,action,data);
    $('#' + idModal).modal("show");
};
*/