﻿/// <reference path="Scripts/DefinedUser/Login/GreinerAssistec.Login.js" />
//  Inicialización de namespace GreinerAssistec
var GreinerAssistec = window.GreinerAssistec || {};

// Módulo de inicialización y funciones genéricas que se utilizaran en todos los módulos
GreinerAssistec.App.Login = (function ($, window, document, undefined) {

    var validateCards = function () {
        var errores = ko.validation.group(GreinerAssistec.App.ViewModel().Login);

        if (errores() != '') {
            GreinerAssistec.App.ViewModel().Register.email(GreinerAssistec.App.ViewModel().Login.Email() + ' ');
            GreinerAssistec.App.ViewModel().Register.password(GreinerAssistec.App.ViewModel().Login.Password() + ' ');
            GreinerAssistec.App.ViewModel().Register.email(GreinerAssistec.App.ViewModel().Login.Email().trim());
            GreinerAssistec.App.ViewModel().Register.password(GreinerAssistec.App.ViewModel().Login.Password().trim());
            GreinerAssistec.App.ShowAlert("alertLogin", errores(), 'danger', 5000);
            console.log('validateCards 02');
            return;
        }
        console.log('validateCards 03');
        if (GreinerAssistec.App.ViewModel().Login.TerminosYCondiciones()) {
            if (!$("#checkTerminosYCondiciones").is(":checked")) {
                GreinerAssistec.App.ShowAlert("alertLogin", "Es necesario aceptar terminos y condiciones", 'danger', 5000);
                console.log('validateCards 04');
                return;
            }
        }
        console.log('validateCards 05');
        var data = JSON.stringify({
            Email: GreinerAssistec.App.ViewModel().Login.Email(),
            Password: GreinerAssistec.App.ViewModel().Login.Password(),
            RememberMe: GreinerAssistec.App.ViewModel().Login.MantenerLogueado(),
            TerminosYCondiciones: GreinerAssistec.App.ViewModel().Login.TerminosYCondiciones()
        });
        var url = GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/seguridad/validacredenciales';
        $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json; charset=utf-8',
            data: data,
            success: function (data) {
                console.log(data);
                if (data.Result == "OK") {
                    GreinerAssistec.App.ShowAlert("alertLogin", "Credenciales Correctas!", 'success', 5000);
                    GreinerAssistec.App.Redirect("Home", '', '');
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertLogin", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {
                GreinerAssistec.App.UnblockUI("divUserLogin");
            }
        }).fail(function (e) { alert('Error: ' + JSON.stringify(e)) });
    }

    var logout = function (e) {
        GreinerAssistec.App.ShowConfirm("Atención!", "¿Desea finalizar su sesión?", "warning", "btn-warning", "Si", true, true, "No", true, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/seguridad/logout',
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        
                        if (data.Result == "OK") {
                            GreinerAssistec.App.Redirect("Login", '', '');
                        }
                        else {
                            GreinerAssistec.App.ShowAlert("alertLogin", data.Message, 'danger', 5000);
                        }
                    },
                    complete: function (data) {

                    }
                }).fail(function (e) { alert('Error: ' + JSON.stringify(e)) });
                //setTimeout(function () { swal("Deleted!", "Your imaginary file has been deleted.", "success"); }, 1000);
            }
        });
    }

    var RecuperarContrasena = function () {
        var errores = ko.validation.group(GreinerAssistec.App.ViewModel().Login.Email);
        if (errores() != '') {
            GreinerAssistec.App.ViewModel().Register.email(GreinerAssistec.App.ViewModel().Login.Email() + ' ');
            GreinerAssistec.App.ViewModel().Register.email(GreinerAssistec.App.ViewModel().Login.Email().trim());
            GreinerAssistec.App.ShowAlert("alertLogin", errores(), 'danger', 5000);
            return;
        }
        var url = GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/seguridad/recuperacontrasena?correo=' + GreinerAssistec.App.ViewModel().Login.Email();
        $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);
                if (data.Result == "OK") {
                    GreinerAssistec.App.ShowAlert("alertLogin", "Te hemos enviado un email con tu contraseña", 'success', 5000);
                    //GreinerAssistec.App.Redirect("Home", '', '');
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertLogin", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {
            }
        }).fail(function (e) { alert('Error: ' + JSON.stringify(e)) });
    }

    var initLoginView = function (isPrincipal) {
        if (isPrincipal) {
            $("#chRememberMe").click(function () {
                if (GreinerAssistec.App.ViewModel().Login.MantenerLogueado())
                    GreinerAssistec.App.ViewModel().Login.MantenerLogueado(false);
                else
                    GreinerAssistec.App.ViewModel().Login.MantenerLogueado(true);
            });
            ActivatePassword();
        }
    }

    var ObtenInfoTarjetaPresentacion = function (IdUsuario) {
        var url = GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/usuarios/info?IdUsuario=' + IdUsuario;
        $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);
                if (data.Result == "OK") {

                    GreinerAssistec.App.ViewModel().CardLogged.UserName(data.Record.Nombre);
                    GreinerAssistec.App.ViewModel().CardLogged.Email(data.Record.Correo);
                    //GreinerAssistec.App.ViewModel().CardLogged.ImgProfile(GreinerAssistec.App.ViewModel().General.RelativePath() + data.Record.ImgProfile);
                    //GreinerAssistec.App.ViewModel().CardLogged.Rol(data.Record.Rol);
                    GreinerAssistec.App.ViewModel().CardLogged.P(data.Record.Contrasena);
                    GreinerAssistec.App.ViewModel().CardLogged.IdUsuario(data.Record.IdUsuario);
                    //token(data.Record.Email, data.Record.Contrasena);
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertLogin", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {
            }
        }).fail(function (e) { alert('Error: ' + JSON.stringify(e)) });
    }

    var CambiaContrasena = function () {
        var errores = ko.validation.group(GreinerAssistec.App.ViewModel().Register.oldPassword);
        if (errores().length > 0) {
            GreinerAssistec.App.ShowAlert("alertChangePassword", errores()[0], 'danger', 5000);
            return;
        }
        errores = ko.validation.group(GreinerAssistec.App.ViewModel().Register.password);
        if (errores().length > 0) {
            GreinerAssistec.App.ShowAlert("alertChangePassword", errores()[0], 'danger', 5000);
            return;
        }
        errores = ko.validation.group(GreinerAssistec.App.ViewModel().Register.password2);
        if (errores().length > 0) {
            GreinerAssistec.App.ShowAlert("alertChangePassword", errores()[0], 'danger', 5000);
            return;
        }

        var data = {
            IdUsuario: GreinerAssistec.App.ViewModel().CardLogged.IdUsuario(),
            Email: GreinerAssistec.App.ViewModel().CardLogged.Email(),
            oldPassword: GreinerAssistec.App.ViewModel().Register.oldPassword(),
            password: GreinerAssistec.App.ViewModel().Register.password(),
            password2: GreinerAssistec.App.ViewModel().Register.password2()
        };
        var url = GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/seguridad/cambiacontrasena';
        $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            success: function (data) {
                console.log(data);
                if (data.Result == "OK") {
                    GreinerAssistec.App.ShowAlert("alertChangePassword", "Se ha cambiado la contraseña correctamente", 'success', 5000);
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertChangePassword", data.Message, 'danger', 5000);
                }
                setTimeout(function () { $("#changePasswordModal").modal("hide"); }, 5000);
            },
            complete: function (data) {
                GreinerAssistec.App.UnblockUI("divUserLogin");
            }
        }).fail(function (e) { alert('Error: ' + JSON.stringify(e)) });
    }

    var registerUser = function (obj, e) {
        e.preventDefault(); // <------------------ stop default behaviour of button

        if (!GreinerAssistec.App.ValidateStructurePassword(GreinerAssistec.App.ViewModel().Register.password())) {
            return;
        }
        var errores = ko.validation.group(GreinerAssistec.App.ViewModel().Register);
        if (errores() != '') {
            GreinerAssistec.App.ViewModel().Register.email(GreinerAssistec.App.ViewModel().Register.email() + ' ');
            GreinerAssistec.App.ViewModel().Register.password(GreinerAssistec.App.ViewModel().Register.password() + ' ');
            GreinerAssistec.App.ViewModel().Register.password2(GreinerAssistec.App.ViewModel().Register.password2() + ' ');
            GreinerAssistec.App.ViewModel().Register.email(GreinerAssistec.App.ViewModel().Register.email().trim());
            GreinerAssistec.App.ViewModel().Register.password(GreinerAssistec.App.ViewModel().Register.password().trim());
            GreinerAssistec.App.ViewModel().Register.password2(GreinerAssistec.App.ViewModel().Register.password2().trim());
            GreinerAssistec.App.ShowAlert("alertRegister", errores(), 'danger', 5000);
            return;
        }
        var data = JSON.stringify({
            Email: GreinerAssistec.App.ViewModel().Register.email(),
            Password: GreinerAssistec.App.ViewModel().Register.password(),
            ConfirmPassword: GreinerAssistec.App.ViewModel().Register.password2(),
        });
        var url = GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/Authentication/Register';
        console.log(data);

        $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json; charset=utf-8',
            data: data,
            headers: GreinerAssistec.App.onBeforeSendBasicHeader(),
            beforeSend: function () {
                GreinerAssistec.App.BlockUI("divUserRegister");
            }, complete: function (data) {
                GreinerAssistec.App.UnblockUI("divUserRegister");
            }
        }).done(function (data) {
            if (data.StatusCode == "OK") {
                GreinerAssistec.App.ViewModel().Login.email(GreinerAssistec.App.ViewModel().Register.email());
                GreinerAssistec.App.ViewModel().Login.password(GreinerAssistec.App.ViewModel().Register.password());
                token();
            }
            else {
                GreinerAssistec.App.ShowAlert("alertRegister", data.Message, 'danger', 5000);
            }
            GreinerAssistec.App.UnblockUI("divUserRegister");
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("alertRegister", 'Error: ' + JSON.stringify(e), 'danger', 5000); });
    };

    var forgetpassword = function (obj, e) {
        e.preventDefault(); // <------------------ stop default behaviour of button
        var errores = ko.validation.group(GreinerAssistec.App.ViewModel().ForgetPassword);
        if (errores() != '') {
            GreinerAssistec.App.ViewModel().Register.email(GreinerAssistec.App.ViewModel().ForgetPassword.email() + ' ');
            GreinerAssistec.App.ViewModel().Register.email(GreinerAssistec.App.ViewModel().ForgetPassword.email().trim());
            GreinerAssistec.App.ShowAlert("alertForgetPass", errores(), 'danger', 5000);
            return;
        }

        var url = GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/Authentication/forgetpassword?email=' + GreinerAssistec.App.ViewModel().ForgetPassword.email();
        $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json; charset=utf-8',
            //data: data,
            beforeSend: function () {
                GreinerAssistec.App.BlockUI("divContentForgetPass");
            },
            success: function (data) {
                if (data.Result == "OK") {
                    GreinerAssistec.App.ShowAlert("alertForgetPass", 'Su contraseña ha sido enviada a su correo.', 'success', 5000);

                }
                else {
                    GreinerAssistec.App.ShowAlert("alertForgetPass", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {
                console.log("verificaCredenciales.complete");
                setTimeout(function () { $('#ForgetPass').modal('hide') }, 5000);
                GreinerAssistec.App.UnblockUI("divContentForgetPass");
            }
        }).fail(function (e) { alert('Error: ' + JSON.stringify(e)) });
    }

    var initialized = false;

    var Initialize = function (authenticated, uNameLogged, isPrincipal, esActivacion) {
        //true, "", false, false
        initLoginView(isPrincipal);

        new ViewModel(authenticated, esActivacion);

        GreinerAssistec.App.ViewModel().Register.IdUsuario(uNameLogged);
        if (authenticated) {
            ObtenInfoTarjetaPresentacion(uNameLogged);
        }

        if (isPrincipal) {
            ko.applyBindings(GreinerAssistec.App.ViewModel());
        }
    }

    var ViewModel = function (authenticated, esActivacion) {
        //var self = this;
        GreinerAssistec.App.ViewModel().t = ko.observable();
        GreinerAssistec.App.ViewModel().AjaxCompleted = ko.observable(false);
        GreinerAssistec.App.ViewModel().Session = new Object();
        GreinerAssistec.App.ViewModel().Session.Authenticated = ko.observable(authenticated);
        //Card logged
        GreinerAssistec.App.ViewModel().CardLogged = new Object();
        GreinerAssistec.App.ViewModel().CardLogged.UserName = ko.observable();
        GreinerAssistec.App.ViewModel().CardLogged.Email = ko.observable();
        GreinerAssistec.App.ViewModel().CardLogged.ImgProfile = ko.observable();
        GreinerAssistec.App.ViewModel().CardLogged.Rol = ko.observable();
        GreinerAssistec.App.ViewModel().CardLogged.IdUsuario = ko.observable();
        GreinerAssistec.App.ViewModel().CardLogged.P = ko.observable();
        //User
        GreinerAssistec.App.ViewModel().Register = new Object();
        GreinerAssistec.App.ViewModel().Register.IdUsuario = ko.observable('');
        GreinerAssistec.App.ViewModel().Register.email = ko.observable('')
            .extend({
                required: { message: 'El Correo es requerido.' }
            })
            .extend({
                pattern: { message: 'El correo no tiene el formato correcto.', params: '^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$' }
            });
        GreinerAssistec.App.ViewModel().Register.oldPassword = ko.observable('')
            .extend({
                required: { message: 'Contraseña actual: La contraseña es requerida.' }
            })
            .extend({
                equal: {
                    params: GreinerAssistec.App.ViewModel().CardLogged.P,
                    message: 'Contraseña actual: La contraseña actual no coincide con la contraseña presentada.'
                }
            });

        GreinerAssistec.App.ViewModel().Register.password = ko.observable('')
            .extend({
                required: { message: 'Nueva Contraseña: La contraseña es requerida.' }
            });
        GreinerAssistec.App.ViewModel().Register.password2 = ko.observable('')
            .extend({
                required: { message: 'Repetición Nueva Contraseña: La confirmacion de la contraseña es requerida.' }
            })
            .extend({
                equal: {
                    params: GreinerAssistec.App.ViewModel().Register.password,
                    message: 'Repetición Nueva Contraseña: La confirmacion no coincide con la contraseña presentada.'
                }
            });
        GreinerAssistec.App.ViewModel().Register.ChangePassword = CambiaContrasena;

        GreinerAssistec.App.ViewModel().Login = new Object();
        GreinerAssistec.App.ViewModel().Login.Email = ko.observable('')
            .extend({
                required: { message: 'El Correo es requerido.' }
            })
            .extend({
                pattern: { message: 'El correo no tiene el formato correcto.', params: '^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$' }
            });
        GreinerAssistec.App.ViewModel().Login.Password = ko.observable('')
            .extend({
                required: { message: 'La contraseña es requerida.' }
            });
        GreinerAssistec.App.ViewModel().Login.TerminosYCondiciones = ko.observable(esActivacion);
        GreinerAssistec.App.ViewModel().Login.MantenerLogueado = ko.observable(false);

        GreinerAssistec.App.ViewModel().Login.Accesar = validateCards;
        GreinerAssistec.App.ViewModel().Login.RecuperarContrasena = RecuperarContrasena;
        GreinerAssistec.App.ViewModel().Login.LogOut = logout;

        GreinerAssistec.App.ViewModel().Location = new Object();
        GreinerAssistec.App.ViewModel().Location.Controller = ko.observable();
        GreinerAssistec.App.ViewModel().Location.View = ko.observable();


        GreinerAssistec.App.ViewModel().ForgetPassword = new Object();
        GreinerAssistec.App.ViewModel().ForgetPassword.email = ko.observable('')
            .extend({
                required: { message: 'El Correo es requerido.' }
            })
            .extend({
                pattern: { message: 'El correo no tiene el formato correcto.', params: '^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$' }
            });
        GreinerAssistec.App.ViewModel().ModalConfirm = new Object();
        GreinerAssistec.App.ViewModel().ModalConfirm.Type = ko.observable();
        GreinerAssistec.App.ViewModel().ModalConfirm.VarData = ko.observable();

        GreinerAssistec.App.ViewModel().RecoverPassword = function () {
            $('#LoginView').animateCSS('fadeOutLeft', 500, function () {
                $('#LoginView').css("display", "none");
                $('#forgotPassView').css("display", "block");
                $('#forgotPassView').animateCSS('fadeInRight', 500, function () {
                });
            });
        }

        GreinerAssistec.App.ViewModel().BackToLoginView = function () {
            $('#forgotPassView').animateCSS('fadeOutRight', 500, function () {
                $('#forgotPassView').css("display", "none");
                $('#LoginView').css("display", "block");
                $('#LoginView').animateCSS('fadeInLeft', 500, function () {
                });
            });
        }
        GreinerAssistec.App.ViewModel().SendPasswordToEmail = RecuperarContrasena;
        GreinerAssistec.App.ViewModel().ShowProfile = function () { alert("ShowProfile"); };
        GreinerAssistec.App.ViewModel().EditProfile = function () { alert("EditProfile"); };
        GreinerAssistec.App.ViewModel().ChangePassword = function () {
            GreinerAssistec.App.ViewModel().Register.oldPassword('');
            GreinerAssistec.App.ViewModel().Register.password('');
            GreinerAssistec.App.ViewModel().Register.password2('');
            $("#changePasswordModal").modal("show");
        }
        /*function () {
            }*/
    }

    var ActivatePassword = function () {
        console.log(1)
        if (window.location.pathname.indexOf("Login/Activar") != -1) {
            try {
                var paramsString = window.location.href.split("?")[1];
                var IdCliente = paramsString.split("=");
                console.log(IdCliente[1])
                var url = GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/usuarios/ValidateUrl?token=' + IdCliente[1];
                $.ajax({
                    type: 'POST',
                    url: url,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        console.log(data);
                        if (data.Result != "OK") {
                            GreinerAssistec.App.Redirect("Login", '', '');
                        }
                    },
                    complete: function (data) {
                        GreinerAssistec.App.UnblockUI("divUserLogin");
                    }
                }).fail(function (e) { alert('Error: ' + JSON.stringify(e)) });
            } catch (err) {
                GreinerAssistec.App.Redirect("Login", '', '');
            }

        }
    }

    var SaveNewPassword = function () {
        if ($("#updatePasswordCliente").val() != "" && $("#updatePasswordCliente2").val() != "") {
            if ($("#updatePasswordCliente").val() == $("#updatePasswordCliente2").val()) {
                try {
                    var paramsString = window.location.href.split("?")[1];
                    var IdCliente = paramsString.split("=");
                    console.log(IdCliente[1])
                    var url = GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/usuarios/UpdatePassword?token=' + IdCliente[1] + '&password=' + $("#updatePasswordCliente").val();
                    $.ajax({
                        type: 'POST',
                        url: url,
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            console.log(data);
                            if (data.Result == "OK") {
                                GreinerAssistec.App.Redirect("Login", '', '');
                            }

                        },
                        complete: function (data) {
                            GreinerAssistec.App.UnblockUI("divUserLogin");
                        }
                    }).fail(function (e) { alert('Error: ' + JSON.stringify(e)) });
                } catch (err) {
                    GreinerAssistec.App.Redirect("Login", '', '');
                }
            } else {
                $("#ModalAlertTitle").empty();
                $("#ModalAlertMessage").empty();
                $("#ModalAlert").modal("show");
                $("#ModalAlertTitle").append("Guardar Contraseña");
                $("#ModalAlertMessage").append("Las contraseñas deben ser iguales.");
            }
        } else {
            $("#ModalAlertTitle").empty();
            $("#ModalAlertMessage").empty();
            $("#ModalAlert").modal("show");
            $("#ModalAlertTitle").append("Guardar Contraseña");
            $("#ModalAlertMessage").append("Es necesario llenar los campos.");
        }
    }

    var token = function (email, password) {
        var loginData = {
            grant_type: 'password',
            username: email,
            password: password
        };

        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'oauth/token',
            data: loginData,
            headers: { "Accept": "application/json", 'Authorization': 'Basic Q2xpZW50ZTE6Q29udHJhc2VuYTAx' },
            //beforeSend: function (jqXHR) {
            //    var aut = BRaices.App.onBeforeSendBasicHeader().Authorization;
            //    jqXHR.setRequestHeader("Authorization", aut);
            //    this.data = "grant_type=password&username=" + 'Cliente1'.replace(/ /g, '+') + "&password=Contrasena01";
            //},
        }).done(function (data) {
            GreinerAssistec.App.ViewModel().t(data);
        }).fail(function (e) { console.log("only getToken"); console.log(e); });
    }

    return {
        Logout: logout,
        Initialize: Initialize,
        ViewModel: ViewModel,
        SaveNewPassword: SaveNewPassword,
    };

}(jQuery, window, document, navigator, undefined));