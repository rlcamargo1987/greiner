﻿/// <reference path="Scripts/DefinedUser/GreinerAssistec.js" />
// Inicialización de namespace GreinerAssistec
var GreinerAssistec = window.GreinerAssistec || {};

// Módulo de inicialización y funciones genéricas que se utilizaran en todos los módulos
GreinerAssistec.App.Inserto = (function ($, window, document, undefined) {

    var AltaInserto = function () {
        $("#exampleInputFile").val("");
        GreinerAssistec.App.ViewModel().Inserto.IdInserto('00000000-0000-0000-0000-000000000000');
        GreinerAssistec.App.ViewModel().Inserto.Codigo('');
        GreinerAssistec.App.ViewModel().Inserto.Peso(0);
        GreinerAssistec.App.ViewModel().Inserto.Cavidades(0);
        GreinerAssistec.App.ViewModel().Inserto.CavidadesFuncionales(0);
        GreinerAssistec.App.ViewModel().Inserto.Colada(0);
        GreinerAssistec.App.ViewModel().Inserto.ColadaCotizada('false');
        GreinerAssistec.App.ViewModel().Inserto.Ciclo(0);
        GreinerAssistec.App.ViewModel().Inserto.CicloReal(0);
        GreinerAssistec.App.ViewModel().Inserto.Maqhrs('');
        GreinerAssistec.App.ViewModel().Inserto.Instruccion('');


        $('#ListadoInsertosPanel').animateCSS('fadeOutLeft', 500, function () {
            $('#ListadoInsertosPanel').css("display", "none");
            $('#EditarInsertoPanel').css("display", "block");
            $('#EditarInsertoPanel').animateCSS('fadeInRight', 500, function () {
            });
        });
    }

    var ConsultarInserto = function (IdInserto) {
        ObtieneInfoInserto(IdInserto, function () {
            $('#ListadoInsertosPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoInsertosPanel').css("display", "none");
                $('#PerfilInsertoPanel').css("display", "block");
                $('#PerfilInsertoPanel').animateCSS('fadeInRight', 500, function () {
                });
            });
        });

    }

    var EditarInserto = function (IdInserto) {
        $("#exampleInputFile").val("");

        ObtieneInfoInserto(IdInserto, function () {
            $('#ListadoInsertosPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoInsertosPanel').css("display", "none");
                $('#EditarInsertoPanel').css("display", "block");
                $('#EditarInsertoPanel').animateCSS('fadeInRight', 500, function () {
                });
            });
            $('#PerfilInsertoPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#PerfilInsertoPanel').css("display", "none");
                $('#EditarInsertoPanel').css("display", "block");
                $('#EditarInsertoPanel').animateCSS('fadeInRight', 500, function () {
                });
            });

        });
    }

    var ObtieneInfoInserto = function (IdInserto, fn) {
        var item = Enumerable.from(GreinerAssistec.App.ViewModel().DataSource()).where(function (x) { return x.IdInserto == IdInserto; }).firstOrDefault();
        GreinerAssistec.App.ViewModel().Inserto.IdInserto(item.IdInserto);
        GreinerAssistec.App.ViewModel().Inserto.Codigo(item.Codigo);
        GreinerAssistec.App.ViewModel().Inserto.Peso(item.Peso);
        GreinerAssistec.App.ViewModel().Inserto.Cavidades(item.Cavidades);
        GreinerAssistec.App.ViewModel().Inserto.CavidadesFuncionales(item.CavidadesFuncionales);
        GreinerAssistec.App.ViewModel().Inserto.Colada(item.Colada);
        GreinerAssistec.App.ViewModel().Inserto.ColadaCotizada(item.ColadaCotizada.toString());
        GreinerAssistec.App.ViewModel().Inserto.Ciclo(item.Ciclo);
        GreinerAssistec.App.ViewModel().Inserto.CicloReal(item.CicloReal);
        GreinerAssistec.App.ViewModel().Inserto.Maqhrs(item.Maqhrs);
        GreinerAssistec.App.ViewModel().Inserto.Instruccion(item.Instruccion);


        fn();
    }

    var EliminarInserto = function (IdInserto) {
        GreinerAssistec.App.ShowConfirm("Atención!", "¿Desea eliminar el inserto?", "warning", "btn-warning", "Si", true, true, "No", true, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/insertos/eliminarinserto?IdInserto=' + IdInserto,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        if (data.Result == "OK") {
                            swal("Aviso", "El inserto ha sido eliminado.", "success");
                            ObtenerTodosInsertos();
                        }
                        else {
                            GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                        }
                    },
                    complete: function (data) {

                    }
                }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });
            }
        });
    }

    var RegresarListadoInsertos = function (isEdit, confirm) {
        if (isEdit && confirm)
            $('#EditarInsertoPanel').animateCSS('fadeOutRight', 500, function () {
                $('#EditarInsertoPanel').css("display", "none");
                $('#PerfilInsertoPanel').css("display", "none");
                $('#ListadoInsertosPanel').css("display", "block");
                $('#ListadoInsertosPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
        else if (isEdit && (confirm == false || confirm == Window.undefined)) {
            // Se manda mensaje para confirmación
        }
        else
            $('#PerfilInsertoPanel').animateCSS('fadeOutRight', 500, function () {
                $('#PerfilInsertoPanel').css("display", "none");
                $('#EditarInsertoPanel').css("display", "none");
                $('#ListadoInsertosPanel').css("display", "block");
                $('#ListadoInsertosPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
    }

    var ObtenerTodosInsertos = function () {
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/insertos/obtenertodosinsertos',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Result == "OK") {
                    console.log(data.Records);
                    $('#InsertoTbl').DataTable().destroy();
                    GreinerAssistec.App.ViewModel().DataSource(data.Records.Record.ListInsertos);

                    $('#InsertoTbl').DataTable({
                        data: GreinerAssistec.App.ViewModel().DataSource(),
                        columns: [
                                     { data: 'Codigo', title: 'Inserto' },
                                     { data: 'Peso' },
                                     { data: 'Cavidades' },
                                     { data: 'CavidadesFuncionales', title: 'Cavidades Funcionales' },
                                     { data: 'Colada' },
                                     { data: 'ColadaCotizada', title: 'Colada Cotizada' },
                                     { data: 'Ciclo' },
                                     { data: 'CicloReal', title: 'Ciclo Real' },
                                     { data: 'Maqhrs', title: 'Maq. hrs' },
                                     { data: 'Instruccion' },
                                     { data: null, class: 'all' }
                        ],
                        columnDefs: [
                            {
                                "render": function (data, type, row) {//Acciones
                                    var res =
                                    '<a class="btn-group bcs" onclick="GreinerAssistec.App.Inserto.ConsultarInserto(\'' + row.IdInserto + '\')">' +
                                    row.Codigo +
                                    '</a>';
                                    return res;
                                },
                                "targets": 0
                            },
                                       {
                                           "render": function (data, type, row) {//Acciones
                                               return '<a onclick=' +
                                                      '"GreinerAssistec.App.Inserto.DescargaArchivo(\'' +
                                                      GreinerAssistec.App.ViewModel().General.RelativePath() + row.Instruccion + '\')">Descargar</a>';
                                           },
                                           "targets": 9
                                       },
                                        {
                                            "render": function (data, type, row) {//Acciones
                                                var res =
                                                '<div class="btn-group bcs">' +
                                                '    <button class="btn dropdown-toggle dropdown-toggle caActions" data-toggle="dropdown" aria-expanded="false">acciones</button>' +
                                                '    <ul class="dropdown-menu pull-right">' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Inserto.ConsultarInserto(\'' + row.IdInserto + '\')">Consultar</a></li>' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Inserto.EditarInserto(\'' + row.IdInserto + '\')">Editar</a></li>' +
                                                '       <li><a role="button" onclick="GreinerAssistec.App.Inserto.EliminarInserto (\'' + row.IdInserto + '\')">Archivar</a></li>' +
                                                '    </ul>' +
                                                '</div>';
                                                return res;
                                            },
                                            "targets": 10
                                        }
                        ]

                    });
                    $('body .dropdown-toggle').dropdown();
                    $('.table-scrollable').on('show.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "auto");
                    });

                    $('.table-scrollable').on('hide.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "hidden");
                    })
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });

    }

    var GuardaInserto = function () {
        var errores = ko.validation.group(GreinerAssistec.App.ViewModel().Inserto);
        if (errores().length > 0) {
            swal("Error!", errores()[0], "error");
            return;
        }
        var data = new FormData();

        if ($("#exampleInputFile").get(0).files.length > 0) {
            data.append("UploadedFile", $("#exampleInputFile").get(0).files[0]);
        }

        var d = ko.mapping.toJSON(GreinerAssistec.App.ViewModel().Inserto);

        data.append("AditionalData", d);

        // Make Ajax request with the contentType = false, and procesDate = false
        var ajaxRequest = $.ajax({
            type: "POST",
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + "mod/api/insertos/guardainserto",
            method: "POST",
            dataType: 'json',
            contentType: false,
            processData: false,
            cache: false,
            data: data
        }).error(function (e) { GreinerAssistec.App.ShowAlert("Error"); console.log(JSON.stringify(e)); console.log(e) });

        ajaxRequest.done(function (data, textStatus) {
            // Do other operation
            if (data.Result == "OK") {
                RegresarListadoInsertos(true, true);
                setTimeout(function () { ObtenerTodosInsertos(); }, 1000);
            }
            else {
                GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
            }
        });
    }

    var DescargaArchivo = function (fileName) {
        console.log(fileName);
        window.open(fileName, "_blank")
    }

    /**
    * Constructor
    **/
    var Initialize = function () {
        /***************************************************/
        GreinerAssistec.App.ViewModel().DataSource = ko.observableArray();
        GreinerAssistec.App.ViewModel().ListaClientes = ko.observableArray();
        GreinerAssistec.App.ViewModel().Inserto = new Object();
        GreinerAssistec.App.ViewModel().Inserto.IdInserto = ko.observable().extend({
            required: { message: 'El IdInserto es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Inserto.Codigo = ko.observable().extend({
            required: { message: 'El Codigo es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Inserto.Peso = ko.observable().extend({
            required: { message: 'El Peso es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Inserto.Cavidades = ko.observable().extend({
            required: { message: 'Cavidades es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Inserto.CavidadesFuncionales = ko.observable().extend({
            required: { message: 'Cavidades Funcionales es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Inserto.Colada = ko.observable().extend({
            required: { message: 'Colada es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Inserto.ColadaCotizada = ko.observable().extend({
            required: { message: 'Colada Cotizada es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Inserto.Ciclo = ko.observable().extend({
            required: { message: 'Ciclo es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Inserto.CicloReal = ko.observable().extend({
            required: { message: 'Ciclo real es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Inserto.Maqhrs = ko.observable().extend({
            required: { message: 'Maqhrs es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Inserto.Instruccion = ko.observable();

        GreinerAssistec.App.ViewModel().Inserto.GuardaInserto = GuardaInserto;
        /********************************************/
        GreinerAssistec.App.ViewModel().Location.Controller("Inserto");
        GreinerAssistec.App.ViewModel().Location.View("Listado de Insertos");
        ko.applyBindings(GreinerAssistec.App.ViewModel());
        ObtenerTodosInsertos();
        setTimeout(function () {
            var url_string = window.location.href;
            var url = new URL(url_string);
            var c = url.searchParams.get("redirect");
            console.log(c);
            if (c == 'new') {
                AltaInserto();
            }
        }, 500);
    };
    /**
    * Public Functions
    **/

    return {
        Initialize: Initialize,
        ObtenerTodosInsertos: ObtenerTodosInsertos,
        AltaInserto: AltaInserto,
        RegresarListadoInsertos: RegresarListadoInsertos,
        ConsultarInserto: ConsultarInserto,
        EditarInserto: EditarInserto,
        EliminarInserto: EliminarInserto,
        DescargaArchivo: DescargaArchivo
        /*
        SuspenderUsuario: SuspenderUsuario,
    LevantaArchivo: LevantaArchivo,
        */
    };
}(jQuery, window, document, navigator, undefined));