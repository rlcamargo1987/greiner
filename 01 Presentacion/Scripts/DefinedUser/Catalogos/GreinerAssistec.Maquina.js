﻿/// <reference path="Scripts/DefinedUser/GreinerAssistec.js" />
// Inicialización de namespace GreinerAssistec
var GreinerAssistec = window.GreinerAssistec || {};

// Módulo de inicialización y funciones genéricas que se utilizaran en todos los módulos
GreinerAssistec.App.Maquina = (function ($, window, document, undefined) {

    var AltaMaquina = function () {
        GreinerAssistec.App.ViewModel().Maquina.IdMaquina('00000000-0000-0000-0000-000000000000');
        GreinerAssistec.App.ViewModel().Maquina.NumeroActivo('');
        GreinerAssistec.App.ViewModel().Maquina.Nombre('');
        GreinerAssistec.App.ViewModel().Maquina.Tonelaje(0);
        GreinerAssistec.App.ViewModel().Maquina.DiametroHusillo(0);
        GreinerAssistec.App.ViewModel().Maquina.Comentarios('');
        GreinerAssistec.App.ViewModel().Maquina.Tipo(0);
        GreinerAssistec.App.ViewModel().Maquina.Descripcion('');

        $('#ListadoMaquinasPanel').animateCSS('fadeOutLeft', 500, function () {
            $('#ListadoMaquinasPanel').css("display", "none");
            $('#EditarMaquinaPanel').css("display", "block");
            $('#EditarMaquinaPanel').animateCSS('fadeInRight', 500, function () {
            });
        });
    }

    var ConsultarMaquina = function (IdMaquina) {
        ObtieneInfoMaquina(IdMaquina, function () {
            $('#ListadoMaquinasPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoMaquinasPanel').css("display", "none");
                $('#PerfilMaquinaPanel').css("display", "block");
                $('#PerfilMaquinaPanel').animateCSS('fadeInRight', 500, function () {
                });
            });
        });

    }

    var EditarMaquina = function (IdMaquina) {
        ObtieneInfoMaquina(IdMaquina, function () {
            $('#ListadoMaquinasPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoMaquinasPanel').css("display", "none");
                $('#EditarMaquinaPanel').css("display", "block");
                $('#EditarMaquinaPanel').animateCSS('fadeInRight', 500, function () {
                });
            });
            $('#PerfilMaquinaPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#PerfilMaquinaPanel').css("display", "none");
                $('#EditarMaquinaPanel').css("display", "block");
                $('#EditarMaquinaPanel').animateCSS('fadeInRight', 500, function () {
                });
            });

        });
    }

    var ObtieneInfoMaquina = function (IdMaquina, fn) {
        var item = Enumerable.from(GreinerAssistec.App.ViewModel().DataSource()).where(function (x) { return x.IdMaquina == IdMaquina; }).firstOrDefault();
        GreinerAssistec.App.ViewModel().Maquina.IdMaquina(item.IdMaquina);
        GreinerAssistec.App.ViewModel().Maquina.NumeroActivo(item.NumeroActivo);
        GreinerAssistec.App.ViewModel().Maquina.Nombre(item.Nombre);
        GreinerAssistec.App.ViewModel().Maquina.Tonelaje(item.Tonelaje);
        GreinerAssistec.App.ViewModel().Maquina.DiametroHusillo(item.DiametroHusillo);
        GreinerAssistec.App.ViewModel().Maquina.Comentarios(item.Comentarios);
        GreinerAssistec.App.ViewModel().Maquina.Tipo(item.Tipo);
        GreinerAssistec.App.ViewModel().Maquina.Descripcion(item.Descripcion);

        fn();
    }

    var EliminarMaquina = function (IdMaquina) {
        GreinerAssistec.App.ShowConfirm("Atención!", "¿Desea eliminar el Maquina?", "warning", "btn-warning", "Si", true, true, "No", true, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/maquinas/eliminarMaquina?IdMaquina=' + IdMaquina,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        if (data.Result == "OK") {
                            swal("Aviso", "El Maquina ha sido eliminado.", "success");
                            ObtenerTodosMaquinas();
                        }
                        else {
                            GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                        }
                    },
                    complete: function (data) {

                    }
                }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });
            }
        });
    }

    var RegresarListadoMaquinas = function (isEdit, confirm) {
        if (isEdit && confirm)
            $('#EditarMaquinaPanel').animateCSS('fadeOutRight', 500, function () {
                $('#EditarMaquinaPanel').css("display", "none");
                $('#PerfilMaquinaPanel').css("display", "none");
                $('#ListadoMaquinasPanel').css("display", "block");
                $('#ListadoMaquinasPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
        else if (isEdit && (confirm == false || confirm == Window.undefined)) {
            // Se manda mensaje para confirmación
        }
        else
            $('#PerfilMaquinaPanel').animateCSS('fadeOutRight', 500, function () {
                $('#PerfilMaquinaPanel').css("display", "none");
                $('#EditarMaquinaPanel').css("display", "none");
                $('#ListadoMaquinasPanel').css("display", "block");
                $('#ListadoMaquinasPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
    }

    var ObtenerTodosMaquinas = function () {
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/maquinas/obtenertodosmaquinas',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Result == "OK") {
                    console.log(data.Records);
                    $('#MaquinaTbl').DataTable().destroy();
                    GreinerAssistec.App.ViewModel().DataSource(data.Records.Record.ListMaquinas);

                    $('#MaquinaTbl').DataTable({
                        data: GreinerAssistec.App.ViewModel().DataSource(),
                        columns: [
                                     { data: 'NumeroActivo', title: 'Numero Activo' },
                                     { data: 'Nombre' },
                                     { data: 'Tonelaje' },
                                     { data: 'DiametroHusillo', title: 'Diametro de Husillo' },
                                     { data: 'Comentarios' },
                                     { data: 'Tipo' },
                                     { data: 'Descripcion' },
                                     { data: null, class: 'all' }
                        ],
                        columnDefs: [{
                            "render": function (data, type, row) {//Acciones
                                var res =
                                '<a class="btn-group bcs" onclick="GreinerAssistec.App.Maquina.ConsultarMaquina(\'' + row.IdMaquina + '\')">' +
                                row.NumeroActivo +
                                '</a>';
                                return res;
                            },
                            "targets": 0
                        },
                                        {
                                            "render": function (data, type, row) {//Acciones
                                                var res =
                                                '<div class="btn-group bcs">' +
                                                '    <button class="btn dropdown-toggle dropdown-toggle caActions" data-toggle="dropdown" aria-expanded="false">acciones</button>' +
                                                '    <ul class="dropdown-menu pull-right">' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Maquina.ConsultarMaquina(\'' + row.IdMaquina + '\')">Consultar</a></li>' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Maquina.EditarMaquina(\'' + row.IdMaquina + '\')">Editar</a></li>' +
                                                '       <li><a role="button" onclick="GreinerAssistec.App.Maquina.EliminarMaquina (\'' + row.IdMaquina + '\')">Archivar</a></li>' +
                                                '    </ul>' +
                                                '</div>';
                                                return res;
                                            },
                                            "targets": 7
                                        }
                        ]

                    });
                    $('body .dropdown-toggle').dropdown();
                    $('.table-scrollable').on('show.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "auto");
                    });

                    $('.table-scrollable').on('hide.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "hidden");
                    })
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });

    }

    var GuardaMaquina = function () {
        var errores = ko.validation.group(GreinerAssistec.App.ViewModel().Maquina);
        if (errores().length > 0) {
            swal("Error!", errores()[0], "error");
            return;
        }

        var d = ko.mapping.toJSON(GreinerAssistec.App.ViewModel().Maquina);
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + "mod/api/Maquinas/guardamaquina",
            contentType: 'application/json; charset=utf-8',
            data: d,
            success: function (data) {
                if (data.Result == "OK") {
                    RegresarListadoMaquinas(true, true);
                    setTimeout(function () { ObtenerTodosMaquinas(); }, 1000);
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });

    }

    /**
    * Constructor
    **/
    var Initialize = function () {
        /***************************************************/
        GreinerAssistec.App.ViewModel().DataSource = ko.observableArray();
        GreinerAssistec.App.ViewModel().ListaTipos = ko.observableArray([{ Id: 1, Nombre: 'Ensamble' }, { Id: 2, Nombre: 'Inyección' }, { Id: 3, Nombre: 'Trial' }]);
        GreinerAssistec.App.ViewModel().Maquina = new Object();
        GreinerAssistec.App.ViewModel().Maquina.IdMaquina = ko.observable().extend({
            required: { message: 'IdMaquina es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Maquina.NumeroActivo = ko.observable().extend({
            required: { message: 'El Numero Activo es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Maquina.Nombre = ko.observable().extend({
            required: { message: 'El Nombre es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Maquina.Tonelaje = ko.observable();
        GreinerAssistec.App.ViewModel().Maquina.DiametroHusillo = ko.observable();
        GreinerAssistec.App.ViewModel().Maquina.Comentarios = ko.observable();
        GreinerAssistec.App.ViewModel().Maquina.Tipo = ko.observable().extend({
            required: { message: 'El Tipo es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Maquina.Descripcion = ko.observable();

        GreinerAssistec.App.ViewModel().Maquina.GuardaMaquina = GuardaMaquina;
        /********************************************/
        GreinerAssistec.App.ViewModel().Location.Controller("Maquina");
        GreinerAssistec.App.ViewModel().Location.View("Listado de Maquinas");
        ko.applyBindings(GreinerAssistec.App.ViewModel());
        ObtenerTodosMaquinas();
        setTimeout(function () {
            var url_string = window.location.href;
            var url = new URL(url_string);
            var c = url.searchParams.get("redirect");
            console.log(c);
            if (c == 'new') {
                AltaMaquina();
            }
        }, 500);
    };
    /**
    * Public Functions
    **/

    return {
        Initialize: Initialize,
        ObtenerTodosMaquinas: ObtenerTodosMaquinas,
        AltaMaquina: AltaMaquina,
        RegresarListadoMaquinas: RegresarListadoMaquinas,
        ConsultarMaquina: ConsultarMaquina,
        EditarMaquina: EditarMaquina,
        EliminarMaquina: EliminarMaquina
        /*
        SuspenderUsuario: SuspenderUsuario,
    LevantaArchivo: LevantaArchivo,
        */
    };
}(jQuery, window, document, navigator, undefined));