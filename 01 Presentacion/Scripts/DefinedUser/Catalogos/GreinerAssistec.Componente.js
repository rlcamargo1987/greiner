﻿/// <reference path="Scripts/DefinedUser/GreinerAssistec.js" />
// Inicialización de namespace GreinerAssistec
var GreinerAssistec = window.GreinerAssistec || {};

// Módulo de inicialización y funciones genéricas que se utilizaran en todos los módulos
GreinerAssistec.App.Componente = (function ($, window, document, undefined) {

    var AltaComponente = function () {
        GreinerAssistec.App.ViewModel().Componente.IdComponente('00000000-0000-0000-0000-000000000000');
        GreinerAssistec.App.ViewModel().Componente.NPInterno(0);
        GreinerAssistec.App.ViewModel().Componente.Tipo('');
        GreinerAssistec.App.ViewModel().Componente.Descripcion('');
        GreinerAssistec.App.ViewModel().Componente.Medida('');
        GreinerAssistec.App.ViewModel().Componente.Cliente('');

        $('#ListadoComponentesPanel').animateCSS('fadeOutLeft', 500, function () {
            $('#ListadoComponentesPanel').css("display", "none");
            $('#EditarComponentePanel').css("display", "block");
            $('#EditarComponentePanel').animateCSS('fadeInRight', 500, function () {
            });
        });
    }

    var ConsultarComponente = function (IdComponente) {
        ObtieneInfoComponente(IdComponente, function () {
            $('#ListadoComponentesPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoComponentesPanel').css("display", "none");
                $('#PerfilComponentePanel').css("display", "block");
                $('#PerfilComponentePanel').animateCSS('fadeInRight', 500, function () {
                });
            });
        });

    }

    var EditarComponente = function (IdComponente) {
        ObtieneInfoComponente(IdComponente, function () {
            $('#ListadoComponentesPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoComponentesPanel').css("display", "none");
                $('#EditarComponentePanel').css("display", "block");
                $('#EditarComponentePanel').animateCSS('fadeInRight', 500, function () {
                });
            });
            $('#PerfilComponentePanel').animateCSS('fadeOutLeft', 500, function () {
                $('#PerfilComponentePanel').css("display", "none");
                $('#EditarComponentePanel').css("display", "block");
                $('#EditarComponentePanel').animateCSS('fadeInRight', 500, function () {
                });
            });

        });
    }

    var ObtieneInfoComponente = function (IdComponente, fn) {
        var item = Enumerable.from(GreinerAssistec.App.ViewModel().DataSource()).where(function (x) { return x.IdComponente == IdComponente; }).firstOrDefault();
        GreinerAssistec.App.ViewModel().Componente.IdComponente(item.IdComponente);
        GreinerAssistec.App.ViewModel().Componente.NPInterno(item.NPInterno);
        GreinerAssistec.App.ViewModel().Componente.Tipo(item.Tipo);
        GreinerAssistec.App.ViewModel().Componente.Descripcion(item.Descripcion);
        GreinerAssistec.App.ViewModel().Componente.Medida(item.Medida);
        GreinerAssistec.App.ViewModel().Componente.Cliente(item.Cliente);

        fn();
    }

    var EliminarComponente = function (IdComponente) {
        GreinerAssistec.App.ShowConfirm("Atención!", "¿Desea eliminar el componente?", "warning", "btn-warning", "Si", true, true, "No", true, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/componentes/eliminarcomponente?IdComponente=' + IdComponente,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        if (data.Result == "OK") {
                            swal("Aviso", "El componente ha sido eliminado.", "success");
                            ObtenerTodosComponentes();
                        }
                        else {
                            GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                        }
                    },
                    complete: function (data) {

                    }
                }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });
            }
        });
    }

    var RegresarListadoComponentes = function (isEdit, confirm) {
        if (isEdit && confirm)
            $('#EditarComponentePanel').animateCSS('fadeOutRight', 500, function () {
                $('#EditarComponentePanel').css("display", "none");
                $('#PerfilComponentePanel').css("display", "none");
                $('#ListadoComponentesPanel').css("display", "block");
                $('#ListadoComponentesPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
        else if (isEdit && (confirm == false || confirm == Window.undefined)) {
            // Se manda mensaje para confirmación
        }
        else
            $('#PerfilComponentePanel').animateCSS('fadeOutRight', 500, function () {
                $('#PerfilComponentePanel').css("display", "none");
                $('#EditarComponentePanel').css("display", "none");
                $('#ListadoComponentesPanel').css("display", "block");
                $('#ListadoComponentesPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
    }

    var ObtenerTodosComponentes = function () {
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/componentes/obtenertodoscomponentes',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Result == "OK") {
                    console.log(data.Records);
                    $('#ComponenteTbl').DataTable().destroy();
                    GreinerAssistec.App.ViewModel().DataSource(data.Records.Record.ListComponentes);

                    $('#ComponenteTbl').DataTable({
                        data: GreinerAssistec.App.ViewModel().DataSource(),
                        columns: [
                                     { data: 'NPInterno', title: 'NP Interno' },
                                     { data: 'Tipo' },
                                     { data: 'Descripcion' },
                                     { data: 'Medida' },
                                     { data: 'Cliente' },
                                     { data: null, class: 'all' }
                        ],
                        columnDefs: [
                                        {
                                            "render": function (data, type, row) {//Acciones
                                                var res =
                                                '<a class="btn-group bcs" onclick="GreinerAssistec.App.Componente.ConsultarComponente(\'' + row.IdComponente + '\')">' +
                                                row.NPInterno +
                                                '</a>';
                                                return res;
                                            },
                                            "targets": 0
                                        },
                                        {
                                            "render": function (data, type, row) {//Acciones
                                                var res =
                                                '<div class="btn-group bcs">' +
                                                '    <button class="btn dropdown-toggle dropdown-toggle caActions" data-toggle="dropdown" aria-expanded="false">acciones</button>' +
                                                '    <ul class="dropdown-menu pull-right">' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Componente.ConsultarComponente(\'' + row.IdComponente + '\')">Consultar</a></li>' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Componente.EditarComponente(\'' + row.IdComponente + '\')">Editar</a></li>' +
                                                '       <li><a role="button" onclick="GreinerAssistec.App.Componente.EliminarComponente (\'' + row.IdComponente + '\')">Archivar</a></li>' +
                                                '    </ul>' +
                                                '</div>';
                                                return res;
                                            },
                                            "targets": 5
                                        }
                        ]

                    });
                    $('body .dropdown-toggle').dropdown();
                    $('.table-scrollable').on('show.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "auto");
                    });

                    $('.table-scrollable').on('hide.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "hidden");
                    })
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });
        //Llena el listado de clientes
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/moldes/obtenertodosmoldes',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Result == "OK") {
                    GreinerAssistec.App.ViewModel().ListaClientes(Enumerable.from(data.Records.Record.ListMoldes).select(function (x) { return x.Cliente; }).distinct().toArray());
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });

    }

    var GuardaComponente = function () {
        var errores = ko.validation.group(GreinerAssistec.App.ViewModel().Componente);
        if (errores().length > 0) {
            swal("Error!", errores()[0], "error");
            return;
        }

        var d = ko.mapping.toJSON(GreinerAssistec.App.ViewModel().Componente);
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + "mod/api/componentes/guardacomponente",
            contentType: 'application/json; charset=utf-8',
            data: d,
            success: function (data) {
                if (data.Result == "OK") {
                    RegresarListadoComponentes(true, true);
                    setTimeout(function () { ObtenerTodosComponentes(); }, 1000);
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });

    }

    /**
    * Constructor
    **/
    var Initialize = function () {
        /***************************************************/
        GreinerAssistec.App.ViewModel().DataSource = ko.observableArray();
        GreinerAssistec.App.ViewModel().ListaClientes = ko.observableArray();
        GreinerAssistec.App.ViewModel().Componente = new Object();
        GreinerAssistec.App.ViewModel().Componente.IdComponente = ko.observable().extend({
            required: { message: 'El IdComponente es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Componente.NPInterno = ko.observable().extend({
            required: { message: 'El NP Interno es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Componente.Tipo = ko.observable().extend({
            required: { message: 'El Tipo es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Componente.Descripcion = ko.observable().extend({
            required: { message: 'La descripcion es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Componente.Medida = ko.observable().extend({
            required: { message: 'La medida es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Componente.Cliente = ko.observable().extend({
            required: { message: 'El Cliente es requerido.' }
        });

        GreinerAssistec.App.ViewModel().Componente.GuardaComponente = GuardaComponente;
        /********************************************/
        GreinerAssistec.App.ViewModel().Location.Controller("Componente");
        GreinerAssistec.App.ViewModel().Location.View("Listado de Componentes");
        ko.applyBindings(GreinerAssistec.App.ViewModel());
        ObtenerTodosComponentes();
        setTimeout(function () {
            var url_string = window.location.href;
            var url = new URL(url_string);
            var c = url.searchParams.get("redirect");
            console.log(c);
            if (c == 'new') {
                AltaComponente();
            }
        }, 500);
    };
    /**
    * Public Functions
    **/

    return {
        Initialize: Initialize,
        ObtenerTodosComponentes: ObtenerTodosComponentes,
        AltaComponente: AltaComponente,
        RegresarListadoComponentes: RegresarListadoComponentes,
        ConsultarComponente: ConsultarComponente,
        EditarComponente: EditarComponente,
        EliminarComponente: EliminarComponente
        /*
        SuspenderUsuario: SuspenderUsuario,
    LevantaArchivo: LevantaArchivo,
        */
    };
}(jQuery, window, document, navigator, undefined));