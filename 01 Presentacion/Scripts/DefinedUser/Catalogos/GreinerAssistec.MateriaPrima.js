﻿/// <reference path="Scripts/DefinedUser/GreinerAssistec.js" />
// Inicialización de namespace GreinerAssistec
var GreinerAssistec = window.GreinerAssistec || {};

// Módulo de inicialización y funciones genéricas que se utilizaran en todos los módulos
GreinerAssistec.App.MateriaPrima = (function ($, window, document, undefined) {

    var AltaMateriaPrima = function () {
        GreinerAssistec.App.ViewModel().MateriaPrima.IdMateriaPrima('00000000-0000-0000-0000-000000000000');
        GreinerAssistec.App.ViewModel().MateriaPrima.NumeroParteInterno(0);
        GreinerAssistec.App.ViewModel().MateriaPrima.Tipo(0);
        GreinerAssistec.App.ViewModel().MateriaPrima.Descripcion('');
        GreinerAssistec.App.ViewModel().MateriaPrima.Secado(false);
        GreinerAssistec.App.ViewModel().MateriaPrima.Temperatura(0);
        GreinerAssistec.App.ViewModel().MateriaPrima.Tiempo('');
        GreinerAssistec.App.ViewModel().MateriaPrima.Familia('');
        GreinerAssistec.App.ViewModel().MateriaPrima.PorcentajeHumedad(0);
        GreinerAssistec.App.ViewModel().MateriaPrima.AmorfoSemi(false);
        GreinerAssistec.App.ViewModel().MateriaPrima.Polimero('');

        $('#ListadoMateriaPrimasPanel').animateCSS('fadeOutLeft', 500, function () {
            $('#ListadoMateriaPrimasPanel').css("display", "none");
            $('#EditarMateriaPrimaPanel').css("display", "block");
            $('#EditarMateriaPrimaPanel').animateCSS('fadeInRight', 500, function () {
            });
        });
    }

    var ConsultarMateriaPrima = function (IdMateriaPrima) {
        ObtieneInfoMateriaPrima(IdMateriaPrima, function () {
            $('#ListadoMateriaPrimasPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoMateriaPrimasPanel').css("display", "none");
                $('#PerfilMateriaPrimaPanel').css("display", "block");
                $('#PerfilMateriaPrimaPanel').animateCSS('fadeInRight', 500, function () {
                });
            });
        });

    }

    var EditarMateriaPrima = function (IdMateriaPrima) {
        ObtieneInfoMateriaPrima(IdMateriaPrima, function () {
            $('#ListadoMateriaPrimasPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoMateriaPrimasPanel').css("display", "none");
                $('#EditarMateriaPrimaPanel').css("display", "block");
                $('#EditarMateriaPrimaPanel').animateCSS('fadeInRight', 500, function () {
                });
            });
            $('#PerfilMateriaPrimaPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#PerfilMateriaPrimaPanel').css("display", "none");
                $('#EditarMateriaPrimaPanel').css("display", "block");
                $('#EditarMateriaPrimaPanel').animateCSS('fadeInRight', 500, function () {
                });
            });

        });
    }

    var ObtieneInfoMateriaPrima = function (IdMateriaPrima, fn) {
        var item = Enumerable.from(GreinerAssistec.App.ViewModel().DataSource()).where(function (x) { return x.IdMateriaPrima == IdMateriaPrima; }).firstOrDefault();
        GreinerAssistec.App.ViewModel().MateriaPrima.IdMateriaPrima(item.IdMateriaPrima);
        GreinerAssistec.App.ViewModel().MateriaPrima.NumeroParteInterno(item.NumeroParteInterno);
        GreinerAssistec.App.ViewModel().MateriaPrima.Tipo(item.Tipo);
        GreinerAssistec.App.ViewModel().MateriaPrima.Descripcion(item.Descripcion);
        GreinerAssistec.App.ViewModel().MateriaPrima.Secado(item.Secado.toString());
        GreinerAssistec.App.ViewModel().MateriaPrima.Temperatura(item.Temperatura);
        GreinerAssistec.App.ViewModel().MateriaPrima.Tiempo(item.Tiempo);
        GreinerAssistec.App.ViewModel().MateriaPrima.Familia(item.Familia);
        GreinerAssistec.App.ViewModel().MateriaPrima.PorcentajeHumedad(item.PorcentajeHumedad);
        GreinerAssistec.App.ViewModel().MateriaPrima.AmorfoSemi(item.AmorfoSemi);
        GreinerAssistec.App.ViewModel().MateriaPrima.Polimero(item.Polimero);
        fn();
    }

    var EliminarMateriaPrima = function (IdMateriaPrima) {
        GreinerAssistec.App.ShowConfirm("Atención!", "¿Desea eliminar el materiaprima?", "warning", "btn-warning", "Si", true, true, "No", true, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/materiaprima/eliminarmateriaprima?IdMateriaPrima=' + IdMateriaPrima,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        if (data.Result == "OK") {
                            swal("Aviso", "El materiaprima ha sido eliminado.", "success");
                            ObtenerTodosMateriaPrimas();
                        }
                        else {
                            GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                        }
                    },
                    complete: function (data) {

                    }
                }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });
            }
        });
    }

    var RegresarListadoMateriaPrimas = function (isEdit, confirm) {
        if (isEdit && confirm)
            $('#EditarMateriaPrimaPanel').animateCSS('fadeOutRight', 500, function () {
                $('#EditarMateriaPrimaPanel').css("display", "none");
                $('#PerfilMateriaPrimaPanel').css("display", "none");
                $('#ListadoMateriaPrimasPanel').css("display", "block");
                $('#ListadoMateriaPrimasPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
        else if (isEdit && (confirm == false || confirm == Window.undefined)) {
            // Se manda mensaje para confirmación
        }
        else
            $('#PerfilMateriaPrimaPanel').animateCSS('fadeOutRight', 500, function () {
                $('#PerfilMateriaPrimaPanel').css("display", "none");
                $('#EditarMateriaPrimaPanel').css("display", "none");
                $('#ListadoMateriaPrimasPanel').css("display", "block");
                $('#ListadoMateriaPrimasPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
    }

    var ObtenerTodosMateriaPrimas = function () {
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/materiaprima/obtenertodosmateriaprima',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Result == "OK") {
                    console.log(data.Records);
                    $('#MateriaPrimaTbl').DataTable().destroy();
                    GreinerAssistec.App.ViewModel().DataSource(data.Records.Record.ListMateriaPrimas);

                    $('#MateriaPrimaTbl').DataTable({
                        data: GreinerAssistec.App.ViewModel().DataSource(),
                        columns: [
                            { data: 'NumeroParteInterno', title: 'Numero de Parte Interno' },
                            { data: 'Tipo' },
                            { data: 'Descripcion' },
                            { data: 'Secado' },
                            { data: 'Temperatura' },
                            { data: 'Tiempo' },
                            { data: 'Familia' },
                            { data: 'PorcentajeHumedad', title: 'Porcentaje de Humedad' },
                            { data: 'AmorfoSemi' },
                            { data: 'Polimero' },
                            { data: null, class: 'all' }
                        ],
                        columnDefs: [
                                        {
                                            "render": function (data, type, row) {//Acciones
                                                var res =
                                                '<a class="btn-group bcs" onclick="GreinerAssistec.App.MateriaPrima.ConsultarMateriaPrima(\'' + row.IdMateriaPrima + '\')">' +
                                                row.NumeroParteInterno +
                                                '</a>';
                                                return res;
                                            },
                                            "targets": 0
                                        },
                                         {
                                             "render": function (data, type, row) {//Acciones
                                                 var item = Enumerable.from(GreinerAssistec.App.ViewModel().ListaTipos()).where(function (x) { return x.Id == row.Tipo; }).firstOrDefault();
                                                 if (item)
                                                     return item.Nombre;
                                                 else
                                                     return '';
                                             },
                                             "targets": 1
                                         },
                                         {
                                             "render": function (data, type, row) {//Acciones
                                                 return row.Secado ? 'Sí' : 'No';
                                             },
                                             "targets": 3
                                         },
                                         {
                                             "render": function (data, type, row) {//Acciones
                                                 return row.Temperatura + '° C';
                                             },
                                             "targets": 4
                                         },
                                         {
                                             "render": function (data, type, row) {//Acciones
                                                 return row.Tiempo + ' Hrs.';
                                             },
                                             "targets": 5
                                         },
                                         {
                                             "render": function (data, type, row) {//Acciones
                                                 return row.PorcentajeHumedad + '%';
                                             },
                                             "targets": 7
                                         },
                                         {
                                             "render": function (data, type, row) {//Acciones
                                                 return row.AmorfoSemi ? 'Semicristalino' : 'Amorfo';
                                             },
                                             "targets": 8
                                         },
                                        {
                                            "render": function (data, type, row) {//Acciones
                                                var res =
                                                '<div class="btn-group bcs">' +
                                                '    <button class="btn dropdown-toggle dropdown-toggle caActions" data-toggle="dropdown" aria-expanded="false">acciones</button>' +
                                                '    <ul class="dropdown-menu pull-right">' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.MateriaPrima.ConsultarMateriaPrima(\'' + row.IdMateriaPrima + '\')">Consultar</a></li>' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.MateriaPrima.EditarMateriaPrima(\'' + row.IdMateriaPrima + '\')">Editar</a></li>' +
                                                '       <li><a role="button" onclick="GreinerAssistec.App.MateriaPrima.EliminarMateriaPrima (\'' + row.IdMateriaPrima + '\')">Archivar</a></li>' +
                                                '    </ul>' +
                                                '</div>';
                                                return res;
                                            },
                                            "targets": 10
                                        }
                        ]

                    });
                    $('body .dropdown-toggle').dropdown();
                    $('.table-scrollable').on('show.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "auto");
                    });

                    $('.table-scrollable').on('hide.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "hidden");
                    })
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });

    }

    var GuardaMateriaPrima = function () {
        var errores = ko.validation.group(GreinerAssistec.App.ViewModel().MateriaPrima);
        if (errores().length > 0) {
            swal("Error!", errores()[0], "error");
            return;
        }

        var d = ko.mapping.toJSON(GreinerAssistec.App.ViewModel().MateriaPrima);
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + "mod/api/materiaprima/guardamateriaprima",
            contentType: 'application/json; charset=utf-8',
            data: d,
            success: function (data) {
                if (data.Result == "OK") {
                    RegresarListadoMateriaPrimas(true, true);
                    setTimeout(function () { ObtenerTodosMateriaPrimas(); }, 1000);
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });

    }

    /**
    * Constructor
    **/
    var Initialize = function () {
        /***************************************************/
        GreinerAssistec.App.ViewModel().DataSource = ko.observableArray();
        GreinerAssistec.App.ViewModel().ListaTipos = ko.observableArray([{ Id: 1, Nombre: 'Pigmento' }, { Id: 2, Nombre: 'Resina' }, { Id: 3, Nombre: 'Aditivo' }, { Id: 4, Nombre: 'Inserto' }, { Id: 5, Nombre: 'Etiquetas' }]);
        GreinerAssistec.App.ViewModel().ListaAS = ko.observableArray([{ Id: false, Nombre: 'Amorfo' }, { Id: true, Nombre: 'Semicristalino' }]);
        GreinerAssistec.App.ViewModel().MateriaPrima = new Object();
        GreinerAssistec.App.ViewModel().MateriaPrima.IdMateriaPrima = ko.observable().extend({
            required: { message: 'El IdMateriaPrima es requerido.' }
        });
        GreinerAssistec.App.ViewModel().MateriaPrima.NumeroParteInterno = ko.observable().extend({
            required: { message: 'El NumeroParteInterno es requerido.' }
        });
        GreinerAssistec.App.ViewModel().MateriaPrima.Tipo = ko.observable().extend({
            required: { message: 'El Tipo es requerido.' }
        });
        GreinerAssistec.App.ViewModel().MateriaPrima.Descripcion = ko.observable().extend({
            required: { message: 'El Descripcion es requerido.' }
        });
        GreinerAssistec.App.ViewModel().MateriaPrima.Secado = ko.observable().extend({
            required: { message: 'El Secado es requerido.' }
        });
        GreinerAssistec.App.ViewModel().MateriaPrima.Temperatura = ko.observable();
        GreinerAssistec.App.ViewModel().MateriaPrima.Tiempo = ko.observable();
        GreinerAssistec.App.ViewModel().MateriaPrima.Familia = ko.observable();
        GreinerAssistec.App.ViewModel().MateriaPrima.PorcentajeHumedad = ko.observable();
        GreinerAssistec.App.ViewModel().MateriaPrima.AmorfoSemi = ko.observable().extend({
            required: { message: 'El AmorfoSemi es requerido.' }
        });
        GreinerAssistec.App.ViewModel().MateriaPrima.Polimero = ko.observable();

        GreinerAssistec.App.ViewModel().MateriaPrima.GuardaMateriaPrima = GuardaMateriaPrima;
        /********************************************/
        GreinerAssistec.App.ViewModel().Location.Controller("MateriaPrima");
        GreinerAssistec.App.ViewModel().Location.View("Listado de MateriaPrimas");
        ko.applyBindings(GreinerAssistec.App.ViewModel());
        ObtenerTodosMateriaPrimas();
        setTimeout(function () {
            var url_string = window.location.href;
            var url = new URL(url_string);
            var c = url.searchParams.get("redirect");
            console.log(c);
            if (c == 'new') {
                AltaMateriaPrima();
            }
        }, 500);
    };
    /**
    * Public Functions
    **/

    return {
        Initialize: Initialize,
        ObtenerTodosMateriaPrimas: ObtenerTodosMateriaPrimas,
        AltaMateriaPrima: AltaMateriaPrima,
        RegresarListadoMateriaPrimas: RegresarListadoMateriaPrimas,
        ConsultarMateriaPrima: ConsultarMateriaPrima,
        EditarMateriaPrima: EditarMateriaPrima,
        EliminarMateriaPrima: EliminarMateriaPrima
        /*
        SuspenderUsuario: SuspenderUsuario,
    LevantaArchivo: LevantaArchivo,
        */
    };
}(jQuery, window, document, navigator, undefined));