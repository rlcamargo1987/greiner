﻿using System.Web;
using System.Web.Optimization;

namespace GreinerAssistec
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-2.1.4.min.js",
                        //"~/Scripts/jquery-1.12.4.js",
                        "~/Scripts/moment.js",
                        "~/Scripts/moment-with-locales.min.js",
                        "~/Scripts/knockout-{version}.js",
                        "~/Scripts/knockout.validation.min.js",
                        "~/Scripts/knockout.mapping-latest.js",
                        "~/Scripts/bootstrap-datetimepicker.js",
                        "~/Scripts/knockout-select2.js",
                        "~/Scripts/LinqJS/linq.js",
                        "~/Scripts/jquery-typeahead-2.10.3/src/jquery.typeahead.js"
                          ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/bootstrap-sweetalert/sweetalert.js",
                      "~/Scripts/DefinedUser/GreinerAssistec.js",
                      "~/Scripts/DefinedUser/GreinerAssistec.Class.js",
                      "~/Scripts/DefinedUser/Login/GreinerAssistec.Login.js",
                      "~/Scripts/DefinedUser/Home/GreinerAssistec.Home.js",
                      "~/Scripts/DefinedUser/Seguridad/GreinerAssistec.Usuario.js",
                      "~/Scripts/DefinedUser/Catalogos/GreinerAssistec.Molde.js",
                      "~/Scripts/DefinedUser/Catalogos/GreinerAssistec.Componente.js",
                      "~/Scripts/DefinedUser/Catalogos/GreinerAssistec.Inserto.js",
                      "~/Scripts/DefinedUser/Catalogos/GreinerAssistec.Empaque.js",
                      "~/Scripts/DefinedUser/Catalogos/GreinerAssistec.Maquina.js",
                      "~/Scripts/DefinedUser/Catalogos/GreinerAssistec.MateriaPrima.js"
                                         ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      //"~/Content/site.css",
                      "~/assets/global/css/animate.min.css",
                      "~/Scripts/bootstrap-sweetalert/sweetalert.css",
                      "~/Scripts/jquery-typeahead-2.10.3/src/jquery.typeahead.css"));
        }
    }
}
