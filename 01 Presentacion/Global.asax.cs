﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace GreinerAssistec
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimsIdentity.DefaultNameClaimType;
        }

        private const int MAXMESSAGELENGTH = 255;

        void Application_PostAuthenticateRequest()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                string[] userRoles = null;
                string[] userCulture = null;
                int userLanguage = 1;
                //string userMenu = string.Empty;



                ////Add the culture of the user for the multi-language
                string usuario = HttpContext.Current.User.Identity.Name.Substring(HttpContext.Current.User.Identity.Name.IndexOf(@"\") + 1);

                if (HttpRuntime.Cache["User"] == null || (HttpRuntime.Cache["User"] != null && Convert.ToString(HttpRuntime.Cache["User"]) == usuario))
                {
                }
                else
                {
                    string KardexId = HttpContext.Current.User.Identity.Name.Substring(HttpContext.Current.User.Identity.Name.IndexOf(@"\") + 1);
                    //ResourcesHelper.Inicia(KardexId);
                }
                GenericPrincipal principal = new GenericPrincipal(HttpContext.Current.User.Identity, userRoles);
                Thread.CurrentPrincipal = HttpContext.Current.User = principal;
                // }
            }
        }


        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Response.Clear();
            string action;
            HttpException httpException = exception as HttpException;
            if (httpException != null)
            {
                switch (httpException.GetHttpCode())
                {
                    case 404:
                        // page not found
                        action = "HttpError404";
                        break;
                    case 500:
                        // server error
                        action = "HttpError500";
                        break;
                    default:
                        action = "GeneralError";
                        break;
                }
            }
            else if (exception != null)
            {
                if (exception.Message == "not authorized")
                    action = "AccessDenied";
                else if (exception.Message == "app not authorized")
                    action = "AppAccessDenied";
                else
                    action = "GeneralError";
            }
            else
            {
                exception = new Exception("not authorized");
                action = "AccessDenied";
            }
            // clear error on server
            string message = string.Empty;
            if (exception != null && exception.Message != null)
            {
                message = System.Net.WebUtility.HtmlEncode(exception.Message.ToString().Replace("\r\n", ""));
            }
            if (message != null && message.Length > MAXMESSAGELENGTH)
            {
                message = message.Substring(0, MAXMESSAGELENGTH);
            }
            ///Response.Redirect(String.Format("~/Home/Error/?message={0}", message));
            Server.ClearError();
        }
        protected void Application_EndRequest()
        {
            var context = new HttpContextWrapper(this.Context);


            // If we're an ajax request and forms authentication caused a 302,  
            // then we actually need to do a 401 
            //
            //
            if (context.Response.StatusCode == 302 && context.Request.IsAjaxRequest())
            {
                context.Response.Clear();
                context.Response.StatusCode = 401;
            }
        }
    }
}
