﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
//using AspNetIdentityDemo.Security;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
/*
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;*/
//using Domain.Entities.Security;
using System.Security.Principal;
using System.Threading;
using GreinerAssistec.Aplicacion.Negocio;
//using Application.Security;
//using Infrastructure.CrossCutting.IoC;
//using Application.Security.DTOs;
namespace GreinerAssistec.UI.Web.CustomActions
{
    /// <summary>
    /// Accion para realizar tareas comunes
    /// </summary>
    public static class Helper
    {
        private static void InicializaContainer()
        {
        }

        /// <summary>
        /// Método extensor para serializar un string a JSON
        /// </summary>
        public static string SerializaToJson(this object objeto)
        {
            string jsonResult = string.Empty;
            try
            {
                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(objeto.GetType());
                MemoryStream ms = new MemoryStream();
                jsonSerializer.WriteObject(ms, objeto);
                jsonResult = Encoding.Default.GetString(ms.ToArray());
            }
            catch { throw; }
            return jsonResult;
        }
       
        /// <summary>;
        /// Método extensor para deserializar JSON cualquier objeto
        /// </summary>;
        public static T DeserializarJsonTo<T>(this string jsonSerializado)
        {
            try
            {
                T obj = Activator.CreateInstance<T>();
                MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(jsonSerializado));
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
                obj = (T)serializer.ReadObject(ms);
                ms.Close();
                ms.Dispose();
                return obj;
            }
            catch { return default(T); }
        }
    }


    public static class ResourcesHelper
    {

        public static string ObtenerUsuarioActivo()
        {
            string usuario = HttpContext.Current.User.Identity.Name.Substring(HttpContext.Current.User.Identity.Name.IndexOf(@"\") + 1);
            ////if (HttpRuntime.Cache["User"] == null || (HttpRuntime.Cache["User"] != null && Convert.ToString(HttpRuntime.Cache["User"]) != usuario))
            ////    ResourcesHelper.Inicia(usuario);
            /*
            var salida = HttpRuntime.Cache["UserDTO"] as UsuarioDTO;
            return String.Format("{0}{1}{2}{3}",
                string.IsNullOrEmpty(salida.Nombre) ? "" : salida.Nombre + " ",
                string.IsNullOrEmpty(salida.Nombre2) ? "" : salida.Nombre2 + " ",
                string.IsNullOrEmpty(salida.ApellidoPaterno) ? "" : salida.ApellidoPaterno + " ",
                string.IsNullOrEmpty(salida.ApellidoMaterno) ? "" : salida.ApellidoMaterno
                    );
            */
            return "";
        }

        public static string ObtenerRecurso(string recurso)
        {
            string usuario = HttpContext.Current.User.Identity.Name.Substring(HttpContext.Current.User.Identity.Name.IndexOf(@"\") + 1);
            ////if (HttpRuntime.Cache["User"] == null || (HttpRuntime.Cache["User"] != null && Convert.ToString(HttpRuntime.Cache["User"]) != usuario))
            ////    ResourcesHelper.Inicia(usuario);

            //var salida = HttpRuntime.Cache[String.Format("UserCulture_{0}", usuario)] as List<RecursoDTO>;
            //return salida.Where(x => x.TipoRecurso == recurso).Single().Valor;
            return "";
        }
    }
}