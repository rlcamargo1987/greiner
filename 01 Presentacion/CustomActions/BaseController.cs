﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace GreinerAssistec.UI.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseController : Controller
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserService"></param>
        public BaseController()//IUserService UserService
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public string _Usuario 
        {
            get
            {
                string usuario = null;
                if (Session["USUARIO"] != null)
                {
                    usuario = (string)Session["USUARIO"];
                }
                return usuario;
            }
        }
    }
}