﻿using GreinerAssistec.UI.Web;
using GreinerAssistec.UI.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
namespace GreinerAssistec.Areas.Catalogos.Controllers
{
    public class CatalogosController : BaseController
    {
        public ActionResult Moldes()
        {
            return View();
        }

        public ActionResult Componentes()
        {
            return View();
        }

        public ActionResult Insertos()
        {
            return View();
        }

        public ActionResult Empaques()
        {
            return View();
        }

        public ActionResult Maquinas()
        {
            return View();
        }

        public ActionResult MateriaPrima()
        {
            return View();
        }
    }
}