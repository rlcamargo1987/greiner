﻿using System.Web.Mvc;
namespace GreinerAssistec.Areas.Catalogos
{

    public class RegistroAreaCatalogos : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Catalogos";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AreaCatalogos_default",
                "Catalogos/{controller}/{action}/{id}",
                new { action = "Acceso", id = UrlParameter.Optional },//action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "GreinerAssistec.Areas.Catalogos.Controllers" }
            );//.DataTokens.Add("area", "Catalogos");
        }
    }

}