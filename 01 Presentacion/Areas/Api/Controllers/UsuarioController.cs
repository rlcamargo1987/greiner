﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http.Results;
using Newtonsoft.Json;
using System.Net;
using System.Linq;
using System.IO;
using System.Data.Entity.Core.Objects;
using System.Net.Mail;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Aplicacion.Negocio;
using GreinerAssistec.CustomAttributes;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Modelo.Entidades;

namespace BRaices.Areas.Api.Controllers
{
    //[CommonApiAuthorize]
    [RoutePrefix("mod/api/usuarios")]
    //[CommonApiAuthorize(Option = "UsuarioController")]
    public class UsuarioController : ApiBaseController//AccountController
    {
        #region Variables

        #endregion

        #region Constructors
        public UsuarioController()
        {

        }
        #endregion

        #region Properties

        #endregion

        #region Methods Action
        [HttpPost]
        [Route("validacredenciales")]
        public JsonResult<Response> ValidaCredenciales(CredentialsDTO credenciales)
        {
            try
            {
                var r = SecurityManagement.Instance().VerificaCredenciales(credenciales.Email, credenciales.Password);
                if (r != null)
                {

                    var ctx = HttpContext.Current.GetOwinContext();
                    AuthorizeHelper.ValidaSesion(credenciales, ctx);
                    return Json(new Response() { Result = ResultType.OK, Record = r });
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = "El usuario y/o contraseña son incorrectos" });
                }
            }
            catch (Exception ex)
            {

                return Json(new Response() { Result = ResultType.Error, Message = string.Format("{0}, Detalle:{1} ", "ValidaCredenciales", General.GetDetailedError(ex)) });
            }
        }

        [HttpPost]
        [Route("creausuario")]
        public async Task<JsonResult<Response>> CreaUsuario(UserDto usuario)
        {
            try
            {
                var r = await SecurityManagement.Instance().CreaUsuario(usuario);
                if (r)
                {

                    return Json(new Response() { Result = ResultType.OK });
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = "Hubo un problema al crear al usuario" });
                }
            }
            catch (Exception ex)
            {

                return Json(new Response() { Result = ResultType.Error, Message = string.Format("{0}, Detalle:{1} ", "ValidaCredenciales", General.GetDetailedError(ex)) });
            }
        }

        [HttpPost]
        [Route("recuperacontrasena")]
        public JsonResult<Response> RecuperaContrasena(string correo)
        {
            try
            {
                var r = SecurityManagement.Instance().RecuperaContrasena(correo);
                if (r)
                {

                    return Json(new Response() { Result = ResultType.OK });
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema al enviar el correo.") });
                }
            }
            catch (Exception ex)
            {

                return Json(new Response() { Result = ResultType.Error, Message = string.Format("RecuperaContrasena, Hubo un problema al enviar el correo. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [HttpPost]
        [Route("info")]
        public JsonResult<Response> info(Guid IdUsuario)
        {
            var r = UserRepository.Instance().ObtenInfoUsuario(IdUsuario);
            if (r != null)
                return Json(new Response() { Result = ResultType.OK, Record = r });
            else
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema al enviar el correo.") });
        }

        [HttpPost]
        [Route("obtenertodosusuarios")]
        public JsonResult<Response> ObtenerTodosUsuarios()
        {
            try
            {
                var r = UserManagement.Instance().ObtenerTodosUsuarios();
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con ObtenerTodosUsuarios.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("ObtenerTodosUsuarios, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [HttpPost]
        [Route("obtenerestatususuarios")]
        public JsonResult<Response> ObtenerEstatusUsuarios()
        {
            try
            {
                var r = UserManagement.Instance().ObtenerEstatusUsuarios();
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con ObtenerEstatusUsuarios.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("ObtenerEstatusUsuarios: Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [Route("updatetmpimg")]
        [HttpPost]
        public string updateImgUser()
        {
            // Get the uploaded image from the Files collection
            var httpPostedFile = HttpContext.Current.Request.Files["CargaFotoUpload"];
            string relPath = GuardaImagen(httpPostedFile, "/Uploads/ImgTmpUsuarios/");
            return relPath;
        }

        private string GuardaImagen(HttpPostedFile httpPostedFile, string DirectorioRelativo)
        {
            string name = "";
            string relPath = "";
            if (httpPostedFile != null)
            {
                name = Guid.NewGuid().ToString() + Path.GetExtension(httpPostedFile.FileName);
                // Validate the uploaded image(optional)
                var pathBase = DirectorioRelativo;
                relPath = pathBase + name;
                // Get the complete file path
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + pathBase), name);

                // Save the uploaded file to "UploadedFiles" folder
                if (File.Exists(HttpContext.Current.Server.MapPath("~") + relPath))
                {
                    File.Delete(HttpContext.Current.Server.MapPath("~") + relPath);
                }
                if (File.Exists(fileSavePath))
                {
                    File.Delete(fileSavePath);
                }
                if (!Directory.Exists(HttpContext.Current.Server.MapPath("~" + pathBase)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~" + pathBase));
                }
                httpPostedFile.SaveAs(fileSavePath);
            }
            return relPath;
        }

        [Route("guardausuario")]
        [HttpPost]
        public async Task GuardaUsuario()
        {
            try
            {
                //Limpia Directorio Temporal
                if (Directory.Exists(HttpContext.Current.Server.MapPath("~" + "/Uploads/ImgTmpUsuarios/")))
                {
                    var files = Directory.GetFiles(HttpContext.Current.Server.MapPath("~" + "/Uploads/ImgTmpUsuarios/"));
                    foreach (var file in files)
                    {
                        File.Delete(file);
                    }
                }
            }
            catch (Exception)
            {

            }
            // Get the uploaded image from the Files collection
            var httpPostedFile = HttpContext.Current.Request.Files["CargaFotoUpload"];
            var userData = new CompleteUserDto();

            //Verifica si la imagen existe
            if (!string.IsNullOrEmpty(userData.Foto))
            {
                if (!File.Exists(HttpContext.Current.Server.MapPath("~" + userData.Foto)))
                {
                    userData.Foto = null;
                }
            }

            try
            {
                var aditionalData = HttpContext.Current.Request.Form["AditionalData"];
                userData = Newtonsoft.Json.JsonConvert.DeserializeObject<CompleteUserDto>(aditionalData);
            }
            catch (Exception ex)
            {
            }
            string relPath = GuardaImagen(httpPostedFile, "/Uploads/FotosUsuarios/");
            if (!string.IsNullOrEmpty(relPath))
            {
                userData.Foto = relPath;
            }
            var r = await UserManagement.Instance().CreaUsuario(userData);

        }

        [HttpPost]
        [Route("obtenerusuariosporid")]
        public JsonResult<Response> obtenerusuariosPorId(Guid IdUsuario)
        {
            try
            {
                var r = UserManagement.Instance().ObtenerUsuariosPorId(IdUsuario);
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con obtenerusuariosPorId.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("obtenerusuariosPorId, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [HttpPost]
        [Route("cambiaestatususuarios")]
        public async Task<JsonResult<Response>> CambiaEstatusUsuarios(Guid IdUsuario, Guid Estatus)
        {
            try
            {
                var r = await UserManagement.Instance().CambiaEstatusUsuarios(IdUsuario, Estatus);
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con obtenerusuariosPorId.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("obtenerusuariosPorId, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }


        [Route("logout")]
        [HttpPost]
        [AllowAnonymous]
        //        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        public JsonResult<Response> Logout()
        {
            Request.GetOwinContext().Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            Request.GetOwinContext().Authentication.SignOut();
            Request.GetOwinContext().Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);
            HttpContext.Current.GetOwinContext().Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);
            return Json(new Response() { Result = ResultType.OK });
        }
        #endregion
    }
}
