﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security.Cookies;
using System.Web.Http.Results;
using System.IO;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Aplicacion.Negocio;
using GreinerAssistec.CustomAttributes;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Modelo.Entidades;

namespace BRaices.Areas.Api.Controllers
{
    //[CommonApiAuthorize]
    [RoutePrefix("mod/api/componentes")]
    //[CommonApiAuthorize(Option = "UsuarioController")]
    public class ComponentesController : ApiBaseController//AccountController
    {
        #region Variables

        #endregion

        #region Constructors
        public ComponentesController()
        {

        }
        #endregion

        #region Properties

        #endregion

        #region Methods Action

        [HttpPost]
        [Route("obtenertodoscomponentes")]
        public JsonResult<Response> ObtenerTodosComponentes()
        {
            try
            {
                var r = ComponenteManagement.Instance().ObtenerTodosComponentes();
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con ObtenerTodosUsuarios.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("ObtenerTodosUsuarios, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [Route("guardacomponente")]
        [HttpPost]
        public async Task<JsonResult<Response>> GuardaComponente(Componente componente)
        {
            try
            {
                var r = await ComponenteManagement.Instance().CreaComponente(componente);
                return Json(r);
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("obtenerusuariosPorId, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [HttpPost]
        [Route("eliminarcomponente")]
        public async Task<JsonResult<Response>> EliminarComponente(Guid IdComponente)
        {
            try
            {
                var r = await ComponenteManagement.Instance().EliminarComponente(IdComponente);
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con CambiaEstatusComponente.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("CambiaEstatusComponente, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }


        #endregion
    }
}
