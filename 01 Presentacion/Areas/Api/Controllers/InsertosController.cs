﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security.Cookies;
using System.Web.Http.Results;
using System.IO;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Aplicacion.Negocio;
using GreinerAssistec.CustomAttributes;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Modelo.Entidades;

namespace BRaices.Areas.Api.Controllers
{
    //[CommonApiAuthorize]
    [RoutePrefix("mod/api/insertos")]
    //[CommonApiAuthorize(Option = "UsuarioController")]
    public class InsertosController : ApiBaseController//AccountController
    {
        #region Variables

        #endregion

        #region Constructors
        public InsertosController()
        {

        }
        #endregion

        #region Properties

        #endregion

        #region Methods Action

        [HttpPost]
        [Route("obtenertodosinsertos")]
        public JsonResult<Response> ObtenerTodosInsertos()
        {
            try
            {
                var r = InsertoManagement.Instance().ObtenerTodosInsertos();
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con ObtenerTodosUsuarios.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("ObtenerTodosUsuarios, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [Route("guardainserto")]
        [HttpPost]
        public async Task<JsonResult<Response>> GuardaInserto()
        {
            // Get the uploaded image from the Files collection
            var httpPostedFile = HttpContext.Current.Request.Files["UploadedFile"];
            var inserto = new Inserto();
            try
            {
                var aditionalData = HttpContext.Current.Request.Form["AditionalData"];
                inserto = Newtonsoft.Json.JsonConvert.DeserializeObject<Inserto>(aditionalData);
            }
            catch (Exception ex)
            {
            }

            try
            {
                var r = await InsertoManagement.Instance().CreaInserto(inserto, httpPostedFile);
                return Json(r);
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("obtenerusuariosPorId, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [HttpPost]
        [Route("eliminarinserto")]
        public async Task<JsonResult<Response>> EliminarInserto(Guid IdInserto)
        {
            try
            {
                var r = await InsertoManagement.Instance().EliminarInserto(IdInserto);
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con CambiaEstatusInserto.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("CambiaEstatusInserto, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }


        #endregion
    }
}
