﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http.Results;
using Newtonsoft.Json;
using System.Net;
using System.Linq;
using System.IO;
using System.Data.Entity.Core.Objects;
using System.Net.Mail;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Aplicacion.Negocio;
using GreinerAssistec.CustomAttributes;

namespace BRaices.Areas.Api.Controllers
{
    //[CommonApiAuthorize]
    [RoutePrefix("mod/api/seguridad")]
    //[CommonApiAuthorize(Option = "AuthenticationController")]
    public class AuthenticationController : ApiBaseController//AccountController
    {
        #region Variables

        #endregion

        #region Constructors
        public AuthenticationController()
        {

        }
        #endregion

        #region Properties

        #endregion

        #region Methods Action
        [HttpPost]
        [Route("validacredenciales")]
        public JsonResult<Response> ValidaCredenciales(CredentialsDTO credenciales)
        {
            try
            {
                var r = SecurityManagement.Instance().VerificaCredenciales(credenciales.Email, credenciales.Password);
                if (r != null)
                {

                    var ctx = HttpContext.Current.GetOwinContext();
                    AuthorizeHelper.ValidaSesion(credenciales, ctx);
                    return Json(new Response() { Result = ResultType.OK, Record = r });
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = "El usuario y/o contraseña son incorrectos" });
                }
            }
            catch (Exception ex)
            {

                return Json(new Response() { Result = ResultType.Error, Message = string.Format("{0}, Detalle:{1} ", "ValidaCredenciales", General.GetDetailedError(ex)) });
            }
        }

        [HttpPost]
        [Route("creausuario")]
        public async Task<JsonResult<Response>> CreaUsuario(UserDto usuario)
        {
            try
            {
                var r = await SecurityManagement.Instance().CreaUsuario(usuario);
                if (r)
                {

                    return Json(new Response() { Result = ResultType.OK });
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = "Hubo un problema al crear al usuario" });
                }
            }
            catch (Exception ex)
            {

                return Json(new Response() { Result = ResultType.Error, Message = string.Format("{0}, Detalle:{1} ", "ValidaCredenciales", General.GetDetailedError(ex)) });
            }
        }

        [HttpPost]
        [Route("recuperacontrasena")]
        public JsonResult<Response> RecuperaContrasena(string correo)
        {
            try
            {
                var r = SecurityManagement.Instance().RecuperaContrasena(correo);
                if (r)
                {

                    return Json(new Response() { Result = ResultType.OK });
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema al enviar el correo.") });
                }
            }
            catch (Exception ex)
            {

                return Json(new Response() { Result = ResultType.Error, Message = string.Format("RecuperaContrasena, Hubo un problema al enviar el correo. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [HttpPost]
        [Route("cambiacontrasena")]
        public async Task<JsonResult<Response>> CambiaContrasena(ChangePasswordModel model)
        {
            try
            {
                var r = await SecurityManagement.Instance().CambiaContrasena(model);
                if (r)
                {

                    return Json(new Response() { Result = ResultType.OK });
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema al cambiar la contraseña.") });
                }
            }
            catch (Exception ex)
            {

                return Json(new Response() { Result = ResultType.Error, Message = string.Format("CambiaContrasena, Hubo un problema al cambiar la contraseña. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [Route("logout")]
        [HttpPost]
        [AllowAnonymous]
        //        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        public JsonResult<Response> Logout()
        {
            Request.GetOwinContext().Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            Request.GetOwinContext().Authentication.SignOut();
            Request.GetOwinContext().Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);
            HttpContext.Current.GetOwinContext().Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);
            return Json(new Response() { Result = ResultType.OK });
        }
        #endregion
    }
}
