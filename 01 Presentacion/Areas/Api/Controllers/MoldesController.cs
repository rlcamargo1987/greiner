﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security.Cookies;
using System.Web.Http.Results;
using System.IO;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Aplicacion.Negocio;
using GreinerAssistec.CustomAttributes;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Modelo.Entidades;

namespace BRaices.Areas.Api.Controllers
{
    //[CommonApiAuthorize]
    [RoutePrefix("mod/api/moldes")]
    //[CommonApiAuthorize(Option = "UsuarioController")]
    public class MoldesController : ApiBaseController//AccountController
    {
        #region Variables

        #endregion

        #region Constructors
        public MoldesController()
        {

        }
        #endregion

        #region Properties

        #endregion

        #region Methods Action

        [HttpPost]
        [Route("obtenertodosmoldes")]
        public JsonResult<Response> ObtenerTodosMoldes()
        {
            try
            {
                var r = MoldeManagement.Instance().ObtenerTodosMoldes();
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con ObtenerTodosUsuarios.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("ObtenerTodosUsuarios, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [Route("guardamolde")]
        [HttpPost]
        public async Task<JsonResult<Response>> GuardaMolde(Molde molde)
        {
            try
            {
                var r = await MoldeManagement.Instance().CreaMolde(molde);
                return Json(r);
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("obtenerusuariosPorId, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [HttpPost]
        [Route("cambiaestatusmolde")]
        public async Task<JsonResult<Response>> CambiaEstatusMolde(Guid IdMolde, bool Activo)
        {
            try
            {
                var r = await MoldeManagement.Instance().CambiaEstatusMolde(IdMolde, Activo);
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con CambiaEstatusMolde.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("CambiaEstatusMolde, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }


        #endregion
    }
}
