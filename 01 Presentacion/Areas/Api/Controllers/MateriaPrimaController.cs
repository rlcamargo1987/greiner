﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security.Cookies;
using System.Web.Http.Results;
using System.IO;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Aplicacion.Negocio;
using GreinerAssistec.CustomAttributes;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Modelo.Entidades;

namespace BRaices.Areas.Api.Controllers
{
    //[CommonApiAuthorize]
    [RoutePrefix("mod/api/materiaprima")]
    //[CommonApiAuthorize(Option = "UsuarioController")]
    public class MateriaPrimaController : ApiBaseController//AccountController
    {
        #region Variables

        #endregion

        #region Constructors
        public MateriaPrimaController()
        {

        }
        #endregion

        #region Properties

        #endregion

        #region Methods Action

        [HttpPost]
        [Route("obtenertodosmateriaprima")]
        public JsonResult<Response> ObtenerTodosMateriaPrima()
        {
            try
            {
                var r = MateriaPrimaManagement.Instance().ObtenerTodosMateriaPrimas();
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con ObtenerTodosUsuarios.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("ObtenerTodosUsuarios, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [Route("guardamateriaprima")]
        [HttpPost]
        public async Task<JsonResult<Response>> GuardaMateriaPrima(MateriaPrima materiaprima)
        {
            try
            {
                var r = await MateriaPrimaManagement.Instance().CreaMateriaPrima(materiaprima);
                return Json(r);
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("obtenerusuariosPorId, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [HttpPost]
        [Route("eliminarmateriaprima")]
        public async Task<JsonResult<Response>> EliminarMateriaPrima(Guid IdMateriaPrima)
        {
            try
            {
                var r = await MateriaPrimaManagement.Instance().EliminarMateriaPrima(IdMateriaPrima);
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con CambiaEstatusMateriaPrima.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("CambiaEstatusMateriaPrima, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }


        #endregion
    }
}
