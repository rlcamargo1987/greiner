﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security.Cookies;
using System.Web.Http.Results;
using System.IO;
using GreinerAssistec.Dominio.Modelo.Dtos;
using GreinerAssistec.Aplicacion.Negocio;
using GreinerAssistec.CustomAttributes;
using GreinerAssistec.Dominio.Repositorio;
using GreinerAssistec.Dominio.Modelo.Entidades;

namespace BRaices.Areas.Api.Controllers
{
    //[CommonApiAuthorize]
    [RoutePrefix("mod/api/empaques")]
    //[CommonApiAuthorize(Option = "UsuarioController")]
    public class EmpaquesController : ApiBaseController//AccountController
    {
        #region Variables

        #endregion

        #region Constructors
        public EmpaquesController()
        {

        }
        #endregion

        #region Properties

        #endregion

        #region Methods Action

        [HttpPost]
        [Route("obtenertodosempaques")]
        public JsonResult<Response> ObtenerTodosEmpaques()
        {
            try
            {
                var r = EmpaqueManagement.Instance().ObtenerTodosEmpaques();
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con ObtenerTodosUsuarios.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("ObtenerTodosUsuarios, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [Route("guardaempaque")]
        [HttpPost]
        public async Task<JsonResult<Response>> GuardaEmpaque(Empaque empaque)
        {
            try
            {
                var r = await EmpaqueManagement.Instance().CreaEmpaque(empaque);
                return Json(r);
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("obtenerusuariosPorId, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }

        [HttpPost]
        [Route("eliminarempaque")]
        public async Task<JsonResult<Response>> EliminarEmpaque(Guid IdEmpaque)
        {
            try
            {
                var r = await EmpaqueManagement.Instance().EliminarEmpaque(IdEmpaque);
                if (r != null)
                {
                    return Json(r);
                }
                else
                {
                    return Json(new Response() { Result = ResultType.Error, Message = string.Format("Hubo un problema con CambiaEstatusEmpaque.") });
                }
            }
            catch (Exception ex)
            {
                return Json(new Response() { Result = ResultType.Error, Message = string.Format("CambiaEstatusEmpaque, Hubo un problema. Error: {0}", General.GetDetailedError(ex)) });
            }
        }


        #endregion
    }
}
