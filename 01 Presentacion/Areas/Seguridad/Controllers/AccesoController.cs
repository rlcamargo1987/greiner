﻿using GreinerAssistec.UI.Web;
using GreinerAssistec.UI.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
namespace GreinerAssistec.Areas.Seguridad.Controllers
{
    public class AccesoController : BaseController
    {
        public ActionResult Inicio()
        {
            return View();
        }

        public ActionResult NotAuthorized()
        {
            return View();
        }
         
        public ActionResult Activar()
        {
            return View();
        }
    }
}