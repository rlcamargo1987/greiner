﻿using System.Web.Mvc;
namespace GreinerAssistec.Areas.Seguridad
{

    public class RegistroAreaSeguridad : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Seguridad";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AreaSeguridad_default",
                "Seguridad/{controller}/{action}/{id}",
                new { action = "Acceso", id = UrlParameter.Optional },//action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "GreinerAssistec.Areas.Seguridad.Controllers" }
            );//.DataTokens.Add("area", "Seguridad");
        }
    }

}