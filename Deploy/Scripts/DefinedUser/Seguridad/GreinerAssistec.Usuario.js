﻿/// <reference path="Scripts/DefinedUser/GreinerAssistec.js" />
// Inicialización de namespace GreinerAssistec
var GreinerAssistec = window.GreinerAssistec || {};

// Módulo de inicialización y funciones genéricas que se utilizaran en todos los módulos
GreinerAssistec.App.Usuario = (function ($, window, document, undefined) {

    var AltaUsuario = function () {
        GreinerAssistec.App.ViewModel().Usuario.IdUsuario('');
        GreinerAssistec.App.ViewModel().Usuario.Foto('');
        GreinerAssistec.App.ViewModel().Usuario.FotoOriginal(GreinerAssistec.App.ViewModel().General.RelativePath()+'/assets/pages/img/user-icon.png');
        GreinerAssistec.App.ViewModel().Usuario.FotoTemporal(GreinerAssistec.App.ViewModel().General.RelativePath()+'/assets/pages/img/user-icon.png');
        GreinerAssistec.App.ViewModel().Usuario.Correo('');
        GreinerAssistec.App.ViewModel().Usuario.Contrasena('');
        GreinerAssistec.App.ViewModel().Usuario.Nombre('');
        GreinerAssistec.App.ViewModel().Usuario.ApellidoPaterno('');
        GreinerAssistec.App.ViewModel().Usuario.ApellidoMaterno('');
        GreinerAssistec.App.ViewModel().Usuario.Genero(false);
        GreinerAssistec.App.ViewModel().Usuario.TelefonoMovil('');
        GreinerAssistec.App.ViewModel().Usuario.FechaNacimiento(GreinerAssistec.App.OnlyDate(new Date()));
        GreinerAssistec.App.ViewModel().Usuario.Area('');
        GreinerAssistec.App.ViewModel().Usuario.Departamento('');
        GreinerAssistec.App.ViewModel().Usuario.Puesto('');
        GreinerAssistec.App.ViewModel().Usuario.PerfilUsuario('');
        GreinerAssistec.App.ViewModel().Usuario.FechaIngreso(GreinerAssistec.App.OnlyDate(new Date()));
        GreinerAssistec.App.ViewModel().Usuario.Estatus(Enumerable.from(GreinerAssistec.App.ViewModel().ListaEstatus()).firstOrDefault().IdEstatusUsuario);


        $('#ListadoUsuariosPanel').animateCSS('fadeOutLeft', 500, function () {
            $('#ListadoUsuariosPanel').css("display", "none");
            $('#EditarUsuarioPanel').css("display", "block");
            $('#EditarUsuarioPanel').animateCSS('fadeInRight', 500, function () {
            });
        });
    }

    var ConsultarUsuario = function (IdUsuario) {
        ObtieneInfoUsuario(IdUsuario, function () {
            $('#ListadoUsuariosPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoUsuariosPanel').css("display", "none");
                $('#PerfilUsuarioPanel').css("display", "block");
                $('#PerfilUsuarioPanel').animateCSS('fadeInRight', 500, function () {
                });
            });
        });

    }

    var EditarUsuario = function (IdUsuario) {
        ObtieneInfoUsuario(IdUsuario, function () {
            $('#ListadoUsuariosPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoUsuariosPanel').css("display", "none");
                $('#EditarUsuarioPanel').css("display", "block");
                $('#EditarUsuarioPanel').animateCSS('fadeInRight', 500, function () {
                });
            });
        });
    }

    var ObtieneInfoUsuario = function (IdUsuario, fn) {
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/usuarios/obtenerusuariosporid?IdUsuario=' + IdUsuario,
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Result == "OK") {
                    if (data.Record.Foto == Window.undefined || data.Record.Foto == '') {
                        data.Record.Foto = GreinerAssistec.App.ViewModel().General.RelativePath()+'/assets/pages/img/user-icon.png';
                    }
                    GreinerAssistec.App.ViewModel().Usuario.IdUsuario(data.Record.IdUsuario);
                    GreinerAssistec.App.ViewModel().Usuario.Foto(data.Record.Foto);
                    GreinerAssistec.App.ViewModel().Usuario.FotoOriginal(data.Record.Foto);
                    GreinerAssistec.App.ViewModel().Usuario.FotoTemporal(data.Record.Foto);
                    GreinerAssistec.App.ViewModel().Usuario.Correo(data.Record.Correo);
                    GreinerAssistec.App.ViewModel().Usuario.Contrasena(data.Record.Contrasena);
                    GreinerAssistec.App.ViewModel().Usuario.Nombre(data.Record.Nombre);
                    GreinerAssistec.App.ViewModel().Usuario.ApellidoPaterno(data.Record.ApellidoPaterno);
                    GreinerAssistec.App.ViewModel().Usuario.ApellidoMaterno(data.Record.ApellidoMaterno);
                    GreinerAssistec.App.ViewModel().Usuario.Genero(data.Record.Genero);
                    GreinerAssistec.App.ViewModel().Usuario.TelefonoMovil(data.Record.TelefonoMovil);
                    GreinerAssistec.App.ViewModel().Usuario.FechaNacimiento(GreinerAssistec.App.OnlyDate(new Date(data.Record.FechaNacimiento)));
                    GreinerAssistec.App.ViewModel().Usuario.Area(data.Record.Area);
                    GreinerAssistec.App.ViewModel().Usuario.Departamento(data.Record.Departamento);
                    GreinerAssistec.App.ViewModel().Usuario.Puesto(data.Record.Puesto);
                    GreinerAssistec.App.ViewModel().Usuario.PerfilUsuario(data.Record.PerfilUsuario);
                    GreinerAssistec.App.ViewModel().Usuario.FechaIngreso(GreinerAssistec.App.OnlyDate(new Date(data.Record.FechaIngreso)));
                    GreinerAssistec.App.ViewModel().Usuario.Estatus(data.Record.Estatus);

                    fn();
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });
    }

    var SuspenderUsuario = function (IdUsuario) {
        CambiaEstatus(IdUsuario, 'B9038DE4-17DA-4E7D-B4B0-52A8FEB2BDE8');
    }

    var ArchivarUsuario = function (IdUsuario) {
        CambiaEstatus(IdUsuario, 'BCE7938F-D1B5-419E-96E3-81F24909BA19');
    }

    var CambiaEstatus = function (IdUsuario, Estatus) {
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/usuarios/cambiaestatususuarios?IdUsuario=' + IdUsuario + "&Estatus=" + Estatus,
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Result == "OK") {
                    swal("Aviso", "El estatus ha sido cambiado.", "success");
                    ObtenerTodosUsuarios();
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });

    }

    var RegresarListadoUsuarios = function (isEdit, confirm) {
        if (isEdit && confirm)
            $('#EditarUsuarioPanel').animateCSS('fadeOutRight', 500, function () {
                $('#EditarUsuarioPanel').css("display", "none");
                $('#ListadoUsuariosPanel').css("display", "block");
                $('#ListadoUsuariosPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
        else if (isEdit && (confirm == false || confirm == Window.undefined)) {
            // Se manda mensaje para confirmación
        }
        else
            $('#PerfilUsuarioPanel').animateCSS('fadeOutRight', 500, function () {
                $('#PerfilUsuarioPanel').css("display", "none");
                $('#ListadoUsuariosPanel').css("display", "block");
                $('#ListadoUsuariosPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
    }

    var LevantaArchivo = function () {

        var data = new FormData();
        var files = $("#FileUpload1").get(0).files;
        // Add the uploaded image content to the form data collection
        if (files.length > 0) { data.append("UploadedImage", files[0]); }

        console.log(data);
        // Make Ajax request with the contentType = false, and procesDate = false
        var ajaxRequest = $.ajax({
            type: "POST",
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + "/mod/api/usuarios/levantaarchivo",
            method: "POST",
            dataType: 'json',
            contentType: false,
            processData: false,
            cache: false,
            //headers: header,
            data: data
        }).error(function (e) { alert("Error" + e); console.log(e); });

        ajaxRequest.done(function (xhr, textStatus) {
            // Do other operation
            console.log(xhr);
            if (xhr.Result == "OK") {
                GreinerAssistec.App.ShowAlert("alertUser", "Se han guardado los cambios", 'success', 5000);
                $("#result").html(JSON.stringify(xhr.Record));
                //setTimeout(function () { location.reload(); }, 2000);
            }

            /*setTimeout(function () {
                GreinerAssistec.App.ViewModel().DashboardUser.ImgProfile(xhr);
            }, 2000);*/
        });
    }

    var ObtenerTodosUsuarios = function () {
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/usuarios/obtenertodosusuarios',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Result == "OK") {
                    console.log(data.Records);
                    $('#UserTbl').DataTable().destroy();
                    GreinerAssistec.App.ViewModel().DataSource(data.Records);
                    $('#UserTbl').DataTable({
                        data: GreinerAssistec.App.ViewModel().DataSource(),
                        columns: [
                                     //{ data: 'IdUsuario' },
                                     { data: 'Correo' },
                                     //{ data: 'Contrasena' },
                                     { data: 'Nombre' },
                                     { data: 'ApellidoPaterno' },
                                     { data: 'ApellidoMaterno' },
                                     { data: 'Genero' },
                                     { data: 'TelefonoMovil' },
                                     { data: 'FechaNacimiento' },
                                     { data: 'Area' },
                                     { data: 'Departamento' },
                                     { data: 'Puesto' },
                                     { data: 'PerfilUsuario' },
                                     { data: 'FechaIngreso' },
                                     { data: 'Estatus', class: 'all' },
                                     { data: null, class: 'all' }
                        ],
                        columnDefs: [
                                        {
                                            "render": function (data, type, row) {//Genero
                                                if (data == false) { return 'Hombre'; } else { return 'Mujer'; }
                                            },
                                            "targets": 4
                                        },
                                        {
                                            "render": function (data, type, row) {//Fecha Nacimiento
                                                return GreinerAssistec.App.OnlyDate(GreinerAssistec.App.SqlToJsDate(data));
                                            },
                                            "targets": 6
                                        },
                                        {
                                            "render": function (data, type, row) {//FechaIngreso
                                                return GreinerAssistec.App.OnlyDate(GreinerAssistec.App.SqlToJsDate(data));
                                            },
                                            "targets": 11
                                        },
                                        {
                                            "render": function (data, type, row) {//FechaIngreso
                                                var r = Enumerable.from(GreinerAssistec.App.ViewModel().ListaEstatus()).where(function (x) { return x.IdEstatusUsuario == data; }).firstOrDefault();
                                                if (r != Window.undefined) {
                                                    return r.Nombre;
                                                } else { return 'No reconocido'; }
                                            },
                                            "targets": 12
                                        },
                                        {
                                            "render": function (data, type, row) {//Acciones
                                                var res =
                                                '<div class="btn-group bcs">' +
                                                '    <button class="btn dropdown-toggle dropdown-toggle caActions" data-toggle="dropdown" aria-expanded="false">acciones</button>' +
                                                '    <ul class="dropdown-menu pull-right">' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Usuario.ConsultarUsuario(\'' + row.IdUsuario + '\')">Consultar</a></li>' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Usuario.EditarUsuario(\'' + row.IdUsuario + '\')">Editar</a></li>' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Usuario.SuspenderUsuario(\'' + row.IdUsuario + '\')">Suspender</a></li>' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Usuario.ArchivarUsuario (\'' + row.IdUsuario + '\')">Archivar</a></li>' +
                                                '    </ul>' +
                                                '</div>';
                                                return res;
                                            },
                                            "targets": 13
                                        }
                        ]
                    });
                    $('body .dropdown-toggle').dropdown();
                    $('.table-scrollable').on('show.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "auto");
                    });

                    $('.table-scrollable').on('hide.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "hidden");
                    })
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });



    }

    var ObtenerEstatusUsuarios = function () {
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/usuarios/obtenerestatususuarios',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Result == "OK") {
                    GreinerAssistec.App.ViewModel().ListaEstatus(data.Records);
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });



    }

    var actualizaImgTmp = function () {
        var data = new FormData();

        var files = $("#CargaFotoUpload").get(0).files;
        // Add the uploaded image content to the form data collection
        if (files.length > 0) {
            data.append("CargaFotoUpload", files[0]);
        }
        var t = '';
        // Make Ajax request with the contentType = false, and procesDate = false
        var ajaxRequest = $.ajax({
            type: "POST",
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + "mod/api/usuarios/updatetmpimg",
            method: "POST",
            dataType: 'json',
            contentType: false,
            processData: false,
            cache: false,
            data: data
        }).error(function (e) { alert("Error" + e); console.log(e); });

        ajaxRequest.done(function (xhr, textStatus) {
            console.log(xhr);
            GreinerAssistec.App.ViewModel().Usuario.FotoTemporal(xhr);
        });
    }

    var GuardaUsuario = function () {
        var errores = ko.validation.group(GreinerAssistec.App.ViewModel().Usuario.Correo);
        if (errores().length > 0) {
            swal("Error!", errores()[0], "error");
            return;
        }
        errores = ko.validation.group(GreinerAssistec.App.ViewModel().Usuario.Contrasena);
        if (errores().length > 0) {
            swal("Error!", errores()[0], "error");
            return;
        }

        var data = new FormData();

        var files = $("#CargaFotoUpload").get(0).files;
        // Add the uploaded image content to the form data collection
        if (files.length > 0) {
            data.append("CargaFotoUpload", files[0]);
            GreinerAssistec.App.ViewModel().Usuario.Foto(GreinerAssistec.App.ViewModel().Usuario.FotoTemporal());
        } else {
            GreinerAssistec.App.ViewModel().Usuario.Foto(GreinerAssistec.App.ViewModel().Usuario.FotoOriginal());
            if (GreinerAssistec.App.ViewModel().Usuario.Foto() == GreinerAssistec.App.ViewModel().General.RelativePath()+'/assets/pages/img/user-icon.png') {
                GreinerAssistec.App.ViewModel().Usuario.Foto('');
            }
        }

        var d = {
            //region Aditional Properties
            IdUsuario: GreinerAssistec.App.ViewModel().Usuario.IdUsuario(),
            Foto: GreinerAssistec.App.ViewModel().Usuario.Foto(),
            Correo: GreinerAssistec.App.ViewModel().Usuario.Correo(),
            Contrasena: GreinerAssistec.App.ViewModel().Usuario.Contrasena(),
            Nombre: GreinerAssistec.App.ViewModel().Usuario.Nombre(),
            ApellidoPaterno: GreinerAssistec.App.ViewModel().Usuario.ApellidoPaterno(),
            ApellidoMaterno: GreinerAssistec.App.ViewModel().Usuario.ApellidoMaterno(),
            Genero: GreinerAssistec.App.ViewModel().Usuario.Genero(),
            TelefonoMovil: GreinerAssistec.App.ViewModel().Usuario.TelefonoMovil(),
            FechaNacimiento: new Date(parseInt($("#ctrlDate1").val().split("/")[2]), parseInt($("#ctrlDate1").val().split("/")[1]) - 1, parseInt($("#ctrlDate1").val().split("/")[0])),
            Area: GreinerAssistec.App.ViewModel().Usuario.Area(),
            Departamento: GreinerAssistec.App.ViewModel().Usuario.Departamento(),
            Puesto: GreinerAssistec.App.ViewModel().Usuario.Puesto(),
            PerfilUsuario: GreinerAssistec.App.ViewModel().Usuario.PerfilUsuario(),
            FechaIngreso: new Date(parseInt($("#ctrlDate2").val().split("/")[2]), parseInt($("#ctrlDate2").val().split("/")[1]) - 1, parseInt($("#ctrlDate2").val().split("/")[0])),
            Estatus: GreinerAssistec.App.ViewModel().Usuario.Estatus()
        };
        data.append("AditionalData", JSON.stringify(d));

        // Make Ajax request with the contentType = false, and procesDate = false
        var ajaxRequest = $.ajax({
            type: "POST",
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + "mod/api/usuarios/guardausuario",
            method: "POST",
            dataType: 'json',
            contentType: false,
            processData: false,
            cache: false,
            data: data
        }).error(function (e) { alert("Error" + e); console.log(e); });

        ajaxRequest.done(function (xhr, textStatus) {
            RegresarListadoUsuarios(true, true);
            setTimeout(function () { ObtenerTodosUsuarios(); }, 1000);
        });
    }

    /**
    * Constructor
    **/
    var Initialize = function () {
        $('#datetimepicker1').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
        $('#datetimepicker2').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });

        $('#changeBtn').click(function () { $('#CargaFotoUpload').click(); });
        $('#resetBtn').click(function () {
            GreinerAssistec.App.ViewModel().Usuario.FotoTemporal(GreinerAssistec.App.ViewModel().Usuario.FotoOriginal());
            $("#CargaFotoUpload").val(Window.undefined);
        });
        $("#CargaFotoUpload").on("change", function () { /*updateImgUser(); */
            if ($("#CargaFotoUpload").get(0).files.length) {
                actualizaImgTmp();
            } else {
                GreinerAssistec.App.ViewModel().Usuario.FotoTemporal(GreinerAssistec.App.ViewModel().Usuario.FotoOriginal());
            }
        });

        /***************************************************/
        GreinerAssistec.App.ViewModel().ListaEstatus = ko.observableArray([]);
        GreinerAssistec.App.ViewModel().ListaGeneros = ko.observableArray([{ Value: false, DisplayText: 'Hombre' }, { Value: true, DisplayText: 'Mujer' }]);
        ObtenerEstatusUsuarios();
        GreinerAssistec.App.ViewModel().DataSource = ko.observableArray();
        GreinerAssistec.App.ViewModel().Usuario = new Object();
        GreinerAssistec.App.ViewModel().Usuario.IdUsuario = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.Foto = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.FotoOriginal = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.FotoTemporal = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.Correo = ko.observable()
        .extend({
            required: { message: 'El Correo es requerido.' }
        })
        .extend({
            pattern: { message: 'El correo no tiene el formato correcto.', params: '^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$' }
        });
        GreinerAssistec.App.ViewModel().Usuario.Contrasena = ko.observable()
        .extend({
            required: { message: 'Contraseña: La contraseña es requerida.' }
        });
        GreinerAssistec.App.ViewModel().Usuario.Nombre = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.ApellidoPaterno = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.ApellidoMaterno = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.Genero = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.TelefonoMovil = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.FechaNacimiento = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.Area = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.Departamento = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.Puesto = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.PerfilUsuario = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.FechaIngreso = ko.observable();
        GreinerAssistec.App.ViewModel().Usuario.Estatus = ko.observable();

        GreinerAssistec.App.ViewModel().Usuario.GuardaUsuario = GuardaUsuario;
        /********************************************/
        GreinerAssistec.App.ViewModel().Location.Controller("Inicio");
        GreinerAssistec.App.ViewModel().Location.View("Pantalla de Bienvenida");
        ko.applyBindings(GreinerAssistec.App.ViewModel());
        ObtenerTodosUsuarios();
    };
    /**
    * Public Functions
    **/

    return {
        Initialize: Initialize,
        ObtenerTodosUsuarios: ObtenerTodosUsuarios,
        LevantaArchivo: LevantaArchivo,
        AltaUsuario: AltaUsuario,
        ConsultarUsuario: ConsultarUsuario,
        EditarUsuario: EditarUsuario,
        SuspenderUsuario: SuspenderUsuario,
        ArchivarUsuario: ArchivarUsuario,
        RegresarListadoUsuarios: RegresarListadoUsuarios
    };
}(jQuery, window, document, navigator, undefined));