﻿/// <reference path="Scripts/DefinedUser/GreinerAssistec.js" />
// Inicialización de namespace GreinerAssistec
var GreinerAssistec = window.GreinerAssistec || {};

// Módulo de inicialización y funciones genéricas que se utilizaran en todos los módulos
GreinerAssistec.App.Home = (function ($, window, document, undefined) {

    /**
    * Constructor
    **/
    var Initialize = function () {
        ko.applyBindings(GreinerAssistec.App.ViewModel());
    };
    /**
    * Public Functions
    **/

    return {
        Initialize: Initialize
    };
}(jQuery, window, document, navigator, undefined));