﻿/// <reference path="Scripts/DefinedUser/GreinerAssistec.js" />
// Inicialización de namespace GreinerAssistec
var GreinerAssistec = window.GreinerAssistec || {};

// Módulo de inicialización y funciones genéricas que se utilizaran en todos los módulos
GreinerAssistec.App.Molde = (function ($, window, document, undefined) {

    var AltaMolde = function () {
        GreinerAssistec.App.ViewModel().Molde.IdMolde('00000000-0000-0000-0000-000000000000');
        GreinerAssistec.App.ViewModel().Molde.Codigo('');
        GreinerAssistec.App.ViewModel().Molde.Nombre('');
        GreinerAssistec.App.ViewModel().Molde.TonelajeCotizado(0);
        GreinerAssistec.App.ViewModel().Molde.TonelajeReal(0);
        GreinerAssistec.App.ViewModel().Molde.Cliente('');
        GreinerAssistec.App.ViewModel().Molde.Descripcion('');
        GreinerAssistec.App.ViewModel().Molde.Activo(1);

        $('#ListadoMoldesPanel').animateCSS('fadeOutLeft', 500, function () {
            $('#ListadoMoldesPanel').css("display", "none");
            $('#EditarMoldePanel').css("display", "block");
            $('#EditarMoldePanel').animateCSS('fadeInRight', 500, function () {
            });
        });
        setTimeout(function () { initTypeAhead() }, 1000);

    }

    var ConsultarMolde = function (IdMolde) {
        ObtieneInfoMolde(IdMolde, function () {
            $('#ListadoMoldesPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoMoldesPanel').css("display", "none");
                $('#PerfilMoldePanel').css("display", "block");
                $('#PerfilMoldePanel').animateCSS('fadeInRight', 500, function () {
                });
            });
        });

    }

    var EditarMolde = function (IdMolde) {
        ObtieneInfoMolde(IdMolde, function () {
            $('#ListadoMoldesPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoMoldesPanel').css("display", "none");
                $('#EditarMoldePanel').css("display", "block");
                $('#EditarMoldePanel').animateCSS('fadeInRight', 500, function () {
                });
            });
            $('#PerfilMoldePanel').animateCSS('fadeOutLeft', 500, function () {
                $('#PerfilMoldePanel').css("display", "none");
                $('#EditarMoldePanel').css("display", "block");
                $('#EditarMoldePanel').animateCSS('fadeInRight', 500, function () {
                });
            });

        });
        setTimeout(function () { initTypeAhead() }, 1000);
    }

    var ObtieneInfoMolde = function (IdMolde, fn) {
        var item = Enumerable.from(GreinerAssistec.App.ViewModel().DataSource()).where(function (x) { return x.IdMolde == IdMolde; }).firstOrDefault();
        GreinerAssistec.App.ViewModel().Molde.IdMolde(item.IdMolde);
        GreinerAssistec.App.ViewModel().Molde.Codigo(item.Codigo);
        GreinerAssistec.App.ViewModel().Molde.Nombre(item.Nombre);
        GreinerAssistec.App.ViewModel().Molde.TonelajeCotizado(item.TonelajeCotizado);
        GreinerAssistec.App.ViewModel().Molde.TonelajeReal(item.TonelajeReal);
        GreinerAssistec.App.ViewModel().Molde.Cliente(item.Cliente);
        GreinerAssistec.App.ViewModel().Molde.Descripcion(item.Descripcion);
        GreinerAssistec.App.ViewModel().Molde.Activo(item.Activo);


        fn();

    }

    var CambiaEstatus = function (IdMolde, activo) {
        GreinerAssistec.App.ShowConfirm("Atención!", "¿Desea archivar el molde?", "warning", "btn-warning", "Si", true, true, "No", true, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/moldes/cambiaestatusmolde?IdMolde=' + IdMolde + "&Activo=" + activo,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        if (data.Result == "OK") {
                            swal("Aviso", "El estatus ha sido cambiado.", "success");
                            ObtenerTodosMoldes();
                        }
                        else {
                            GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                        }
                    },
                    complete: function (data) {

                    }
                }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });
            }
        });
    }

    var RegresarListadoMoldes = function (isEdit, confirm) {
        if (isEdit && confirm)
            $('#EditarMoldePanel').animateCSS('fadeOutRight', 500, function () {
                $('#EditarMoldePanel').css("display", "none");
                $('#PerfilMoldePanel').css("display", "none");
                $('#ListadoMoldesPanel').css("display", "block");
                $('#ListadoMoldesPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
        else if (isEdit && (confirm == false || confirm == Window.undefined)) {
            // Se manda mensaje para confirmación
        }
        else
            $('#PerfilMoldePanel').animateCSS('fadeOutRight', 500, function () {
                $('#PerfilMoldePanel').css("display", "none");
                $('#EditarMoldePanel').css("display", "none");
                $('#ListadoMoldesPanel').css("display", "block");
                $('#ListadoMoldesPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
    }

    var ObtenerTodosMoldes = function () {
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/moldes/obtenertodosmoldes',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Result == "OK") {
                    console.log(data.Records);
                    $('#MoldeTbl').DataTable().destroy();
                    GreinerAssistec.App.ViewModel().DataSource(Enumerable.from(data.Records.Record.ListMoldes).where(function (x) { return x.Activo }).toArray());
                    console.log(GreinerAssistec.App.ViewModel().DataSource());
                    $('#MoldeTbl').DataTable({
                        data: GreinerAssistec.App.ViewModel().DataSource(),
                        columns: [
                                     { data: 'Codigo', title: 'Molde' },
                                     { data: 'Nombre' },
                                     { data: 'TonelajeCotizado', title: 'Tonelaje Cotizado' },
                                     { data: 'TonelajeReal', title: 'Tonelaje Real' },
                                     { data: 'Cliente' },
                                     { data: 'Descripcion', title: 'Descripción' },
                                     { data: 'Activo' },
                                     { data: null, class: 'all' }
                        ],
                        columnDefs: [
                                        {
                                            "render": function (data, type, row) {//Acciones
                                                var res =
                                                '<a class="btn-group bcs"  onclick="GreinerAssistec.App.Molde.ConsultarMolde(\'' + row.IdMolde + '\')">' +
                                                row.Nombre +
                                                '</a>';
                                                return res;
                                            },
                                            "targets": 1
                                        },
                                        {
                                            "render": function (data, type, row) {//Fecha Nacimiento
                                                return row.Activo == true || row.Activo == 1 ? '<span class="active">Activo</span>' : '<span class="inactive">Desactivado</span>';
                                            },
                                            "targets": 6
                                        },
                                        {
                                            "render": function (data, type, row) {//Acciones
                                                var res =
                                                '<div class="btn-group bcs">' +
                                                '    <button class="btn dropdown-toggle dropdown-toggle caActions" data-toggle="dropdown" aria-expanded="false">acciones</button>' +
                                                '    <ul class="dropdown-menu pull-right">' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Molde.ConsultarMolde(\'' + row.IdMolde + '\')">Consultar</a></li>' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Molde.EditarMolde(\'' + row.IdMolde + '\')">Editar</a></li>' +
                                                '       <li><a role="button" onclick="GreinerAssistec.App.Molde.ArchivarMolde (\'' + row.IdMolde + '\',false)">Archivar</a></li>' +
                                                '    </ul>' +
                                                '</div>';
                                                return res;
                                            },
                                            "targets": 7
                                        }
                        ]

                    });
                    $('body .dropdown-toggle').dropdown();
                    $('.table-scrollable').on('show.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "auto");
                    });

                    $('.table-scrollable').on('hide.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "hidden");
                    })
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });
    }

    var GuardaMolde = function () {
        var errores = ko.validation.group(GreinerAssistec.App.ViewModel().Molde);
        if (errores().length > 0) {
            swal("Error!", errores()[0], "error");
            return;
        }

        var d = ko.mapping.toJSON(GreinerAssistec.App.ViewModel().Molde);
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + "mod/api/moldes/guardamolde",
            contentType: 'application/json; charset=utf-8',
            data: d,
            success: function (data) {
                if (data.Result == "OK") {
                    RegresarListadoMoldes(true, true);
                    setTimeout(function () { ObtenerTodosMoldes(); }, 1000);
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });

    }

    var initTypeAhead = function () {

        var states = Enumerable.from(GreinerAssistec.App.ViewModel().DataSource()).select(function (x) { return x.Cliente; }).distinct(function (x) { return x; }).toArray();

        $.typeahead({
            input: '.typeahead',
            //order: "desc",
            matcher: function (item, obj) {
                item = $("#ClienteInput").val();
                if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                    return true;
                }
                if (item.toUpperCase().indexOf(this.query.trim().toUpperCase()) != -1) {
                    return true;
                }
                var query = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&');
                query = query.replace(/a/ig, '[a\341\301\340\300\342\302\344\304]');
                query = query.replace(/e/ig, '[e\351\311\350\310\352\312\353\313]');
                query = query.replace(/i/ig, '[i\355\315\354\314\356\316\357\317]');
                query = query.replace(/o/ig, '[o\363\323\362\322\364\324\366\326]');
                query = query.replace(/u/ig, '[u\372\332\371\331\373\333\374\334]');
                query = query.replace(/c/ig, '[c\347\307]');
                console.log(query);
                if (item.toLowerCase().match(query.toLowerCase())) {
                    return true;
                }
            },
            updater: function (item) {
                // do what you want with the item here
                GreinerAssistec.App.ViewModel().Molde.Cliente(item);
                return item;
            },
            source: {
                data: states
            },
            callback: {
                onInit: function (node) {
                    console.log('Typeahead Initiated on ' + node.selector);
                },
                onCancel: function (node) {
                    GreinerAssistec.App.ViewModel().Molde.Cliente('');
                },
                onClickAfter: function (a, b, c) {
                    GreinerAssistec.App.ViewModel().Molde.Cliente(c.display);
                }
            }
        });


    }
    /**
    * Constructor
    **/
    var Initialize = function () {

        /***************************************************/
        GreinerAssistec.App.ViewModel().DataSource = ko.observableArray();

        GreinerAssistec.App.ViewModel().Molde = new Object();
        GreinerAssistec.App.ViewModel().Molde.IdMolde = ko.observable().extend({
            required: { message: 'El IdMolde es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Molde.Codigo = ko.observable().extend({
            required: { message: 'El Codigo es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Molde.Nombre = ko.observable().extend({
            required: { message: 'El Nombre es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Molde.TonelajeCotizado = ko.observable().extend({
            required: { message: 'El Tonelaje cotizado es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Molde.TonelajeReal = ko.observable().extend({
            required: { message: 'El Tonelaje real es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Molde.Cliente = ko.observable().extend({
            required: { message: 'El Cliente es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Molde.Descripcion = ko.observable().extend({
            required: { message: 'La descripcion es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Molde.Activo = ko.observable().extend({
            required: { message: 'Activo es requerido.' }
        });

        GreinerAssistec.App.ViewModel().Molde.GuardaMolde = GuardaMolde;
        /********************************************/
        GreinerAssistec.App.ViewModel().Location.Controller("Molde");
        GreinerAssistec.App.ViewModel().Location.View("Listado de Moldes");
        ko.applyBindings(GreinerAssistec.App.ViewModel());
        ObtenerTodosMoldes();

        setTimeout(function () {
            var url_string = window.location.href;
            var url = new URL(url_string);
            var c = url.searchParams.get("redirect");
            console.log(c);
            if (c == 'new') {
                AltaMolde();
            }
        }, 500);
    };
    /**
    * Public Functions
    **/

    return {
        Initialize: Initialize,
        ObtenerTodosMoldes: ObtenerTodosMoldes,
        AltaMolde: AltaMolde,
        RegresarListadoMoldes: RegresarListadoMoldes,
        ConsultarMolde: ConsultarMolde,
        EditarMolde: EditarMolde,
        ArchivarMolde: CambiaEstatus
        /*
        SuspenderUsuario: SuspenderUsuario,
    LevantaArchivo: LevantaArchivo,
        */
    };
}(jQuery, window, document, navigator, undefined));