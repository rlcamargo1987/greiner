﻿/// <reference path="Scripts/DefinedUser/GreinerAssistec.js" />
// Inicialización de namespace GreinerAssistec
var GreinerAssistec = window.GreinerAssistec || {};

// Módulo de inicialización y funciones genéricas que se utilizaran en todos los módulos
GreinerAssistec.App.Empaque = (function ($, window, document, undefined) {

    var AltaEmpaque = function () {
        GreinerAssistec.App.ViewModel().Empaque.IdEmpaque('00000000-0000-0000-0000-000000000000');
        GreinerAssistec.App.ViewModel().Empaque.NPInterno(0);
        GreinerAssistec.App.ViewModel().Empaque.Tipo('');
        GreinerAssistec.App.ViewModel().Empaque.Descripcion('');
        GreinerAssistec.App.ViewModel().Empaque.CajasPallets(0);
        GreinerAssistec.App.ViewModel().Empaque.KilosSoporta(0);
        GreinerAssistec.App.ViewModel().Empaque.NivelesCajaTarima(0);

        $('#ListadoEmpaquesPanel').animateCSS('fadeOutLeft', 500, function () {
            $('#ListadoEmpaquesPanel').css("display", "none");
            $('#EditarEmpaquePanel').css("display", "block");
            $('#EditarEmpaquePanel').animateCSS('fadeInRight', 500, function () {
            });
        });
    }

    var ConsultarEmpaque = function (IdEmpaque) {
        ObtieneInfoEmpaque(IdEmpaque, function () {
            $('#ListadoEmpaquesPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoEmpaquesPanel').css("display", "none");
                $('#PerfilEmpaquePanel').css("display", "block");
                $('#PerfilEmpaquePanel').animateCSS('fadeInRight', 500, function () {
                });
            });
        });

    }

    var EditarEmpaque = function (IdEmpaque) {
        ObtieneInfoEmpaque(IdEmpaque, function () {
            $('#ListadoEmpaquesPanel').animateCSS('fadeOutLeft', 500, function () {
                $('#ListadoEmpaquesPanel').css("display", "none");
                $('#EditarEmpaquePanel').css("display", "block");
                $('#EditarEmpaquePanel').animateCSS('fadeInRight', 500, function () {
                });
            });
            $('#PerfilEmpaquePanel').animateCSS('fadeOutLeft', 500, function () {
                $('#PerfilEmpaquePanel').css("display", "none");
                $('#EditarEmpaquePanel').css("display", "block");
                $('#EditarEmpaquePanel').animateCSS('fadeInRight', 500, function () {
                });
            });

        });
    }

    var ObtieneInfoEmpaque = function (IdEmpaque, fn) {
        var item = Enumerable.from(GreinerAssistec.App.ViewModel().DataSource()).where(function (x) { return x.IdEmpaque == IdEmpaque; }).firstOrDefault();
        GreinerAssistec.App.ViewModel().Empaque.IdEmpaque(item.IdEmpaque);
        GreinerAssistec.App.ViewModel().Empaque.NPInterno(item.NPInterno);
        GreinerAssistec.App.ViewModel().Empaque.Tipo(item.Tipo);
        GreinerAssistec.App.ViewModel().Empaque.Descripcion(item.Descripcion);
        GreinerAssistec.App.ViewModel().Empaque.CajasPallets(item.CajasPallets);
        GreinerAssistec.App.ViewModel().Empaque.KilosSoporta(item.KilosSoporta);
        GreinerAssistec.App.ViewModel().Empaque.NivelesCajaTarima(item.NivelesCajaTarima);

        fn();
    }

    var EliminarEmpaque = function (IdEmpaque) {
        GreinerAssistec.App.ShowConfirm("Atención!", "¿Desea eliminar el Empaque?", "warning", "btn-warning", "Si", true, true, "No", true, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/empaques/eliminarEmpaque?IdEmpaque=' + IdEmpaque,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        if (data.Result == "OK") {
                            swal("Aviso", "El Empaque ha sido eliminado.", "success");
                            ObtenerTodosEmpaques();
                        }
                        else {
                            GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                        }
                    },
                    complete: function (data) {

                    }
                }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });
            }
        });
    }

    var RegresarListadoEmpaques = function (isEdit, confirm) {
        if (isEdit && confirm)
            $('#EditarEmpaquePanel').animateCSS('fadeOutRight', 500, function () {
                $('#EditarEmpaquePanel').css("display", "none");
                $('#PerfilEmpaquePanel').css("display", "none");
                $('#ListadoEmpaquesPanel').css("display", "block");
                $('#ListadoEmpaquesPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
        else if (isEdit && (confirm == false || confirm == Window.undefined)) {
            // Se manda mensaje para confirmación
        }
        else
            $('#PerfilEmpaquePanel').animateCSS('fadeOutRight', 500, function () {
                $('#PerfilEmpaquePanel').css("display", "none");
                $('#EditarEmpaquePanel').css("display", "none");
                $('#ListadoEmpaquesPanel').css("display", "block");
                $('#ListadoEmpaquesPanel').animateCSS('fadeInLeft', 500, function () {
                });
            });
    }

    var ObtenerTodosEmpaques = function () {
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + 'mod/api/empaques/obtenertodosempaques',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Result == "OK") {
                    console.log(data.Records);
                    $('#EmpaqueTbl').DataTable().destroy();
                    GreinerAssistec.App.ViewModel().DataSource(data.Records.Record.ListEmpaques);

                    $('#EmpaqueTbl').DataTable({
                        data: GreinerAssistec.App.ViewModel().DataSource(),
                        columns: [
                                     { data: 'NPInterno', title: 'NP Interno' },
                                     { data: 'Tipo' },
                                     { data: 'Descripcion' },
                                     { data: 'CajasPallets' },
                                     { data: 'KilosSoporta' },
                                     { data: 'NivelesCajaTarima' },
                                     { data: null, class: 'all' }
                        ],
                        columnDefs: [{
                            "render": function (data, type, row) {//Acciones
                                var res =
                                '<a class="btn-group bcs" onclick="GreinerAssistec.App.Empaque.ConsultarEmpaque(\'' + row.IdEmpaque + '\')">' +
                                row.NPInterno +
                                '</a>';
                                return res;
                            },
                            "targets": 0
                                    },
                                        {
                                            "render": function (data, type, row) {//Acciones
                                                var res =
                                                '<div class="btn-group bcs">' +
                                                '    <button class="btn dropdown-toggle dropdown-toggle caActions" data-toggle="dropdown" aria-expanded="false">acciones</button>' +
                                                '    <ul class="dropdown-menu pull-right">' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Empaque.ConsultarEmpaque(\'' + row.IdEmpaque + '\')">Consultar</a></li>' +
                                                '        <li><a role="button" onclick="GreinerAssistec.App.Empaque.EditarEmpaque(\'' + row.IdEmpaque + '\')">Editar</a></li>' +
                                                '       <li><a role="button" onclick="GreinerAssistec.App.Empaque.EliminarEmpaque (\'' + row.IdEmpaque + '\')">Archivar</a></li>' +
                                                '    </ul>' +
                                                '</div>';
                                                return res;
                                            },
                                            "targets": 6
                                        }
                        ]

                    });
                    $('body .dropdown-toggle').dropdown();
                    $('.table-scrollable').on('show.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "auto");
                    });

                    $('.table-scrollable').on('hide.bs.dropdown', function () {
                        $('.table-scrollable').css("overflow-y", "hidden");
                    })
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });

    }

    var GuardaEmpaque = function () {
        var errores = ko.validation.group(GreinerAssistec.App.ViewModel().Empaque);
        if (errores().length > 0) {
            swal("Error!", errores()[0], "error");
            return;
        }

        var d = ko.mapping.toJSON(GreinerAssistec.App.ViewModel().Empaque);
        $.ajax({
            type: 'POST',
            url: GreinerAssistec.App.ViewModel().General.RelativePath() + "mod/api/Empaques/guardaempaque",
            contentType: 'application/json; charset=utf-8',
            data: d,
            success: function (data) {
                if (data.Result == "OK") {
                    RegresarListadoEmpaques(true, true);
                    setTimeout(function () { ObtenerTodosEmpaques(); }, 1000);
                }
                else {
                    GreinerAssistec.App.ShowAlert("alertHome", data.Message, 'danger', 5000);
                }
            },
            complete: function (data) {

            }
        }).fail(function (e) { GreinerAssistec.App.ShowAlert("Error"); +console.log(JSON.stringify(e)) });

    }

    /**
    * Constructor
    **/
    var Initialize = function () {
        /***************************************************/
        GreinerAssistec.App.ViewModel().DataSource = ko.observableArray();
        GreinerAssistec.App.ViewModel().ListaClientes = ko.observableArray();
        GreinerAssistec.App.ViewModel().Empaque = new Object();
        GreinerAssistec.App.ViewModel().Empaque.IdEmpaque = ko.observable().extend({
            required: { message: 'El IdEmpaque es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Empaque.NPInterno = ko.observable().extend({
            required: { message: 'El NP Interno es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Empaque.Tipo = ko.observable().extend({
            required: { message: 'El Tipo es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Empaque.Descripcion = ko.observable().extend({
            required: { message: 'La descripcion es requerido.' }
        });
        GreinerAssistec.App.ViewModel().Empaque.CajasPallets = ko.observable();
        GreinerAssistec.App.ViewModel().Empaque.KilosSoporta = ko.observable();
        GreinerAssistec.App.ViewModel().Empaque.NivelesCajaTarima = ko.observable();

        GreinerAssistec.App.ViewModel().Empaque.GuardaEmpaque = GuardaEmpaque;
        /********************************************/
        GreinerAssistec.App.ViewModel().Location.Controller("Empaque");
        GreinerAssistec.App.ViewModel().Location.View("Listado de Empaques");
        ko.applyBindings(GreinerAssistec.App.ViewModel());
        ObtenerTodosEmpaques();
        setTimeout(function () {
            var url_string = window.location.href;
            var url = new URL(url_string);
            var c = url.searchParams.get("redirect");
            console.log(c);
            if (c == 'new') {
                AltaEmpaque();
            }
        }, 500);
    };
    /**
    * Public Functions
    **/

    return {
        Initialize: Initialize,
        ObtenerTodosEmpaques: ObtenerTodosEmpaques,
        AltaEmpaque: AltaEmpaque,
        RegresarListadoEmpaques: RegresarListadoEmpaques,
        ConsultarEmpaque: ConsultarEmpaque,
        EditarEmpaque: EditarEmpaque,
        EliminarEmpaque: EliminarEmpaque
        /*
        SuspenderUsuario: SuspenderUsuario,
    LevantaArchivo: LevantaArchivo,
        */
    };
}(jQuery, window, document, navigator, undefined));