﻿/// <reference path="Scripts/DefinedUser/GreinerAssistec.js" />
// Inicialización de namespace GreinerAssistec
var GreinerAssistec = window.GreinerAssistec || {};

// Módulo de inicialización de clases en general
GreinerAssistec.Class = (function ($, window, document, undefined) {
    var self = this;

    var UsuarioModel = function (IdUsuario, NombreUsuario, Nombre, Nombre2, ApellidoPaterno, ApellidoMaterno, Email,
        IdLenguaje, IdRol, Activo, FechaCreacion) {
        var self = this;
        self.IdUsuario = IdUsuario;
        self.NombreUsuario = NombreUsuario;
        self.Nombre = Nombre;
        self.Nombre2 = Nombre2;
        self.ApellidoPaterno = ApellidoPaterno;
        self.ApellidoMaterno = ApellidoMaterno;
        self.Email = Email;
        self.IdLenguaje = IdLenguaje;
        self.IdRol = IdRol;
        self.Activo = Activo;
        self.FechaCreacion = FechaCreacion;
    };

    var UsuarioADModel = function (IdUsuario, NombreUsuario, Correo) {
        var self = this;
        self.IdUsuario = IdUsuario;
        self.NombreUsuario = NombreUsuario;
        self.Correo = Correo;
    };

    var Usuario = function (nombre, contrasena) {

    }

    var RoleModel = function (src, obj) {
        var role = new Object();
        if (src.IdRole === Window.undefined) {
            role.IdRole = ko.observable(obj.IdRole);
            role.Name = ko.observable(obj.Name);
        } else {
            src.IdRole(obj.IdRole);
            src.Name(objeto.Name);
        }
    }

    var ProfileModel = function (src, objeto) {

        if (src.Id === Window.undefined) {
            src.Id = ko.observable(objeto.Id);
            src.UserName = ko.observable(objeto.UserName);
            src.Email = ko.observable(objeto.Email);
            src.PasswordHash = ko.observable(objeto.PasswordHash);
            src.SecurityStamp = ko.observable(objeto.SecurityStamp);
            src.TwoFactorEnabled = ko.observable(objeto.TwoFactorEnabled);
            src.ImgProfile = ko.observable(objeto.ImgProfile);
            src.Name = ko.observable(objeto.Name);
            src.LastName = ko.observable(objeto.LastName);
            src.AboutMe = ko.observable(objeto.AboutMe);
            src.Phone = ko.observable(objeto.Phone);
            src.Address = ko.observable(objeto.Address);
            src.DateBirth = ko.observable(objeto.DateBirth);
            src.Website = ko.observable(objeto.Website);
            //src.Role = new Object();
            // new RoleModel(src.Role, objeto.Role);
        } else {
            src.Id(objeto.Id);
            src.UserName(objeto.UserName);
            src.Email(objeto.Email);
            src.PasswordHash(objeto.PasswordHash);
            src.SecurityStamp(objeto.SecurityStamp);
            src.TwoFactorEnabled(objeto.TwoFactorEnabled);
            src.ImgProfile(objeto.ImgProfile);
            src.Name(objeto.Name);
            src.LastName(objeto.LastName);
            src.AboutMe(objeto.AboutMe);
            src.Phone(objeto.Phone);
            src.Address(objeto.Address);
            src.DateBirth(objeto.DateBirth);
            src.Website(objeto.Website);
            // new RoleModel(src.Role, objeto.Role);
        }

    };

    var HousePublishedModel = function (src, objeto) {

        if (src.Id === Window.undefined) {
            src.Id = ko.observable(objeto.Id);
            src.UserName = ko.observable(objeto.UserName);
            src.Email = ko.observable(objeto.Email);
            src.PasswordHash = ko.observable(objeto.PasswordHash);
            src.SecurityStamp = ko.observable(objeto.SecurityStamp);
            src.TwoFactorEnabled = ko.observable(objeto.TwoFactorEnabled);
            src.ImgProfile = ko.observable(objeto.ImgProfile);
            src.Name = ko.observable(objeto.Name);
            src.LastName = ko.observable(objeto.LastName);
            src.AboutMe = ko.observable(objeto.AboutMe);
            src.Phone = ko.observable(objeto.Phone);
            src.Address = ko.observable(objeto.Address);
            src.DateBirth = ko.observable(objeto.DateBirth);
            src.Website = ko.observable(objeto.Website);
            //src.Role = new Object();
            // new RoleModel(src.Role, objeto.Role);
        } else {
            src.Id(objeto.Id);
            src.UserName(objeto.UserName);
            src.Email(objeto.Email);
            src.PasswordHash(objeto.PasswordHash);
            src.SecurityStamp(objeto.SecurityStamp);
            src.TwoFactorEnabled(objeto.TwoFactorEnabled);
            src.ImgProfile(objeto.ImgProfile);
            src.Name(objeto.Name);
            src.LastName(objeto.LastName);
            src.AboutMe(objeto.AboutMe);
            src.Phone(objeto.Phone);
            src.Address(objeto.Address);
            src.DateBirth(objeto.DateBirth);
            src.Website(objeto.Website);
            // new RoleModel(src.Role, objeto.Role);
        }

    };


    //Inicializacion del objeto
    var objProfile = function () {
        var self = this;
        var objeto = new Object();
        objeto.Id = '';
        objeto.UserName = '';
        objeto.Email = '';
        objeto.PasswordHash = '';
        objeto.SecurityStamp = '';
        objeto.TwoFactorEnabled = '';
        objeto.ImgProfile = '';
        objeto.Name = '';
        objeto.LastName = '';
        objeto.AboutMe = '';
        objeto.Phone = '';
        objeto.Address = '';
        objeto.DateBirth = '';
        objeto.Website = '';
        return objeto;
    };

    return {
        UsuarioModel: UsuarioModel,
        ProfileModel: ProfileModel,
        objProfile: objProfile
    };
}(jQuery, window, document, navigator, undefined));